# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/) and this
project adheres to [Semantic Versioning](https://semver.org/).

## [0.3.0] - 2020-12-05
### Added
- All style variables
- Many menus and screens
- Many CGs and special sprites, including most of Emi and Rin's routes

## [0.2.0] - 2020-10-12
### Added
- All non-invis sprites now upscale
- Text size upscaling for game dialogue
- File tree
- Upscaled many menus and screens
### Changed
- Helper functions were renamed for clarity
- The HD folder and file extensions with it can now be CAPS

## [0.1.0] - 2020-08-29
### Added
- Support for the "hd" folder
- NOP1 vfx (snow, heartbeat, etc.)
- Initial load screen work
### Changed
- Image rescaling no longer requires passing the original file size
- Code is now copied from KS and can be uncommented and modified more easily

## [0.0.1] - 2020-07-26
### Added
- README.md
- This file, CHANGELOG.md
- Initial version of heartburn.rpy
  - Automatically upscales main menu screen
  - Automatically upscales backgrounds
