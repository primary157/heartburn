# Heartburn
Heartburn version 0.3.0

## About
This is an fan-made patch to add support for HD graphics to Katawa Shoujo.

## Installation
1. Install Katawa Shoujo normally
2. Open the folder where the game was installed
3. [Download Heartburn](../raw/release/heartburn.rpy?inline=false)
4. Move or copy the `heartburn.rpy` file into Katawa Shoujo's `game` folder (which should already exist)
5. Run Katawa Shoujo like normal

## Adding HD assets
HD assets are not included. Heartburn just makes it possible to add them if they are available.
1. If you have any HD assets, they should be in a folder called `bgs`, `event`, `sprites`, `ui`, or `video`
2. Create an `hd` folder inside of Katawa Shoujo's `game` folder
3. Copy the entire HD asset folder into the newly created `hd` folder

A full file tree can be found in [file-tree.txt](file-tree.txt) that shows where each file must be placed. Names and paths must match exactly except for the file extension which can be anything reasonable (e.g. .jpeg, .PNG, .bmp, etc.).

## Uninstalling
Uninstalling Heartburn is as simple as deleting `heartburn.rpy` and `heartburn.rpyc` (if present) from Katawa Shoujo's `game` directory.

Optionally, you can also delete the `hd` folder if you have created one, but this is not required.

## Known issues
This is a development version of Heartburn. Until it reaches version 1.0.0, things not working right is completely expected. You will also need to manually edit the `config.screen_width` and `config.screen_height` variables in `heartburn.rpy` to the desired 4:3 resolution. You should not set them to any other aspect ratio for the time being. The goal in the future is to allow an arbitrary resolution and aspect ratio to be chosen through the in-game options menu.
