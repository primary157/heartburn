#========================
# Heartburn version 0.3.0
#========================

init 250 python:

    import os

    config.screen_width = 1200
    config.screen_height = 900

    hb_version = '0.3.0'
    config.developer = True
    old_screen_height = 600
    old_screen_width = 800
    game_version += '\nHeartburn v' + hb_version

    for label in ['confirm_mm', 'load_screen', 'cg_gallery', 'extra_menu', 'gm_bare', 'text_history_gm', 'confirm_quit', 'prefs_screen', 'joystick_screen', 'language_screen', 'save_screen', 'video_menu', 'music_menu', 'music_menu_loop', 'scene_select', 'scene_select_loop', 'scene_deleted', 'credits', 'after_credits', 'localized_tc']:
        config.label_overrides[label] = 'hb_' + label

    # Calculates a new height or y-value from an old height
    def hb_rescale_y(height):
        return config.screen_height * height / old_screen_height

    # Calculates a new width or x-value from an old width
    def hb_rescale_x(width):
        return config.screen_width * width / old_screen_width

    # Calculates a new width and height and returns a tuple
    def hb_rescale_xy(width, height):
        return (hb_rescale_x(width), hb_rescale_y(height))

    # Looks for an HD version of an image
    def hb_get_img_path(img):
        if os.path.isdir('hd'):
            hd_img = os.path.splitext('hd/' + img)[0]
        else:
            hd_img = os.path.splitext('HD/' + img)[0]
        extensions = ['.png', '.jpg', '.jpeg', '.webp', '.gif', '.bmp']
        for e in extensions:
            if renpy.loadable(hd_img + e):
                img = hd_img + e
                break
            elif renpy.loadable(hd_img + e.upper()):
                img = hd_img + e.upper()
                break
        return img

    # Reads the size of an image file
    def hb_get_img_size(img):
        tmp1 = renpy.loader.load(img)
        tmp2 = renpy.display.scale.image_load_unscaled(tmp1, img)
        return tmp2.get_size()

    # Rescales an image based on its old size and the old screen resolution
    def hb_rescale_image(img):
        old_w, old_h = hb_get_img_size(img)
        img = hb_get_img_path(img)
        img_w, img_h = hb_get_img_size(img)
        h_fraction = old_h / float(old_screen_height)
        new_h = h_fraction * config.screen_height
        return im.FactorScale(img, new_h / img_h)

# ==============================================================================
# ui_code.rpy
# ==============================================================================

init 499 python:

    def is_glrenpy():
        version = renpy.version().split()[1]
        if int(version.split(".")[0]) > 6:
            return True
        elif int(version.split(".")[0]) == 6:
            if int(version.split(".")[1]) > 10:
                return True
        return False
#
#     def is_lion():
#         import os, sys
#         if sys.platform == "darwin" and int(os.uname()[2].split(".")[0]) > 10:
#             return True
#         return False
#
#     disallow_fullscreen = False
#
#     if is_lion() and not is_glrenpy():
#         print "JESUS CHRIST IT'S A LION, DISABLE FULLSCREEN"
#         disallow_fullscreen = True
#         _preferences.fullscreen = False
#         config.keymap['toggle_fullscreen'] = None
#
#     def mm_context():
#         if is_glrenpy():
#             return renpy.context()._main_menu
#         else:
#             return renpy.context().main_menu
#
#     if not persistent.oldvol:
#         persistent.oldvol = {}
#
#     def mute_toggle():
#         mixers = ["music", "sfx"]
#         if not persistent.is_muted:
#             for mixer in mixers:
#                 persistent.oldvol[mixer] = _preferences.get_volume(mixer)
#                 _preferences.set_volume(mixer, config.minimumvolume)
#             persistent.is_muted=True
#         else:
#             for mixer in mixers:
#                 _preferences.set_volume(mixer, persistent.oldvol[mixer])
#             persistent.is_muted= False
#         if renpy.current_interact_type() in ("menu", "say"):
#             renpy.restart_interaction()
#
#     config.locked = False
#     config.keymap['mute_toggle'] = ['m']
#     config.keymap['abort_video'] = ['K_ESCAPE']
#     mymap = renpy.Keymap(mute_toggle = mute_toggle)
#     config.underlay.append(mymap)
#     config.keymap['dismiss'].remove('K_RETURN')
#     config.keymap['dismiss'].remove('K_KP_ENTER')
#     config.keymap['hide_windows'] = []
#     config.locked = True
#
#     if persistent.is_muted:
#         mute_toggle()
#
#     def turn_afm_on():
#         if may_afm:
#             _preferences.afm_time = persistent.afm_time
#             renpy.restart_interaction()
#
#     def go_history():
#         if may_afm:
#             renpy.game_menu('text_history')
#
#     def go_load():
#         if may_afm:
#             renpy.game_menu('load_key')
#
#     def go_save():
#         if may_afm:
#             renpy.game_menu('save_key')
#
#     def go_prefs():
#         if may_afm:
#             renpy.game_menu('prefs_key')
#
#     def go_image():
#         if may_afm:
#             renpy.game_menu('image_key')
#
#     def additional_keys():
#
#         ui.keymap(a=turn_afm_on)
#         ui.keymap(t=go_history)
#         ui.keymap(K_F2=go_load)
#         ui.keymap(K_F3=go_save)
#         ui.keymap(K_F4=go_prefs)
#         ui.keymap(h=go_image)
#         ui.keymap(mouseup_2=go_image)
#         ui.keymap(joy_hide=go_image)
#     config.overlay_functions.append(additional_keys)
#
#     def gm_page_return_to_game():
#         store.entered_from_game = False
#         ui.jumps("custom_return")()
#
#     def gm_image_return_to_game():
#         store.entered_from_game = False
#         ui.jumps("custom_return_dissolve")()
#
#     def generate_string(length):
#         rv = ""
#         for i in xrange(length):
#             rv += "."
#         return rv
#
#     def unique_id():
#         import random
#         return str(random.getrandbits(32))
#
#     class SoundTransitionClassCompat(renpy.display.transition.Transition):
#         """
#         A transition that does nothing but play a sound.
#         To be used in MultipleTransitions, obviously.
#         """
#
#         def __init__(self, sound, time=0.0001, channel="sound", old_widget=None, new_widget=None, **properties):
#             super(SoundTransitionClassCompat, self).__init__(time, **properties)
#
#             self.time = time
#             self.old_widget = old_widget
#             self.new_widget = new_widget
#             self.events = False
#             self.sound = sound
#             self.channel = channel
#
#         def render(self, width, height, st, at):
#
#             if st >= self.time:
#                 self.events = True
#                 return render(self.new_widget, width, height, st, at)
#
#             if st < self.time:
#                 renpy.display.render.redraw(self, 0)
#
#             renpy.sound.play(self.sound, channel=self.channel)
#             return renpy.display.render.Render(width, height, opaque=True)
#
#     def disp_size(disp):
#         if isinstance(disp, str):
#             disp = im.Image(disp)
#         return disp.load().get_size()
#
#     def quasiblur(disp_in, factor, bilinear_out=True, bilinear_in=True):
#
#         disp_down = im.FactorScale(disp_in, width=1.0/factor, bilinear=bilinear_in)
#         if bilinear_out:
#             in_w, in_h = disp_size(disp_in)
#             disp_up = im.Scale(disp_down, width=int(in_w+factor), height=int(in_h+factor), bilinear=True)
#             disp_up = im.Crop(disp_up, 0, 0, in_w, in_h)
#         else:
#             disp_up = im.FactorScale(disp_down, width=factor, bilinear=False)
#         return disp_up
#
#     class SoundTransitionClassGL(renpy.display.transition.Transition):
#         """
#         A transition that does nothing but play a sound.
#         To be used in MultipleTransitions, obviously.
#         """
#         def __init__(self, sound, delay=0.0001, channel="sound", old_widget=None, new_widget=None, **properties):
#             super(SoundTransitionClassGL, self).__init__(delay, **properties)
#
#             self.old_widget = old_widget
#             self.new_widget = new_widget
#             self.events = True
#
#             self.sound = sound
#             self.channel = channel
#             self.played = False
#
#         def render(self, width, height, st, at):
#
#             if not self.played:
#                 renpy.sound.play(self.sound, channel=self.channel)
#                 self.played = True
#
#             return renpy.display.transition.null_render(self, width, height, st, at)
#
#     if is_glrenpy():
#         SoundTransition = renpy.curry(SoundTransitionClassGL)
#     else:
#         SoundTransition = renpy.curry(SoundTransitionClassCompat)
#
#     def transition_attach_sound(tr_in, sound):
#         return MultipleTransition([False,SoundTransition(sound),False,tr_in,True])
#
#
#
#     class ReadbackADVCharacter(ADVCharacter):
#         def do_done(self, who, what):
#             if self.show_function == quotefixer:
#                 what = change_quotes(what)
#             store_say(who, what)
#             return
#
#     class ReadbackNVLCharacter(NVLCharacter):
#         def do_done(self, who, what):
#             store_say(who, what)
#             return
#
#     def menu_init():
#         store.playthroughflag = False
#         store.ask_to_quit = False
#         switch_language(persistent.current_language)
#         store.last_visited_label = None
#         renpy.music.play(music_menus, fadein=5.0, fadeout=0.5, if_changed=True)
#         config.skipping = None
#         _preferences.afm_time = 0
#
#     def test_sound():
#         sounds = [sfx_slide, sfx_slide2, sfx_draw]
#         this_sound = renpy.random.choice(sounds)
#         renpy.music.play(this_sound, channel="sound")
#
#     def fallthrough_catcher(label, not_ft):
#         global available_languages
#         is_script = False
#         for rawprefix in available_languages:
#             prefix = rawprefix + "_"
#             if label.startswith(prefix):
#                 is_script = True
#         if is_script and not not_ft:
#             renpy.jump("simple_return")
#
#
#     config.label_callback = fallthrough_catcher
#
#
#
#
#
#
#     def draw_graph():
#         n = 300.0
#         for i in xrange(n):
#             x = (i / n)
#             xd = x * 0.75 + 0.125
#
#
#
#
#             y = 1.0 - acdc_warp(x)
#             renpy.show("dot", tag="test"+str(i), at_list=[Position(xpos=xd, ypos=y)])
#
#
#     renpy.music.register_channel("ambient", "sfx", True, tight=True)
#     renpy.music.register_channel("ambient2", "sfx", True, tight=True)

init 502 python:

#     if not persistent.emi:
#         persistent.emi = 0
#     if not persistent.hanako:
#         persistent.hanako = 0
#     if not persistent.lilly:
#         persistent.lilly = 0
#     if not persistent.shizune:
#         persistent.shizune = 0
#     if not persistent.rin:
#         persistent.rin = 0
#     if not persistent.bad:
#         persistent.bad = 0
#
#     automode = False
#     notextmode = False
#     config.auto_choice_delay = None
#     nongamemenus = 0
#
#     def autosave_metabuilder():
#         if save_name:
#             full_save_name = save_name + "#" + str(renpy.get_game_runtime())
#         else:
#             full_save_name = "#" + str(renpy.get_game_runtime())
#         return full_save_name
#
#     config.auto_save_extra_info = autosave_metabuilder
#
#
#     max_readback_buffer_length = 256
#     readback_buffer = []
#     def store_say(who, what):
#         global readback_buffer
#         if not who or who == NARRATOR_NAME or who ==  preparse_say_for_store(NARRATOR_NAME):
#             who = ""
#         if not what:
#             what = ""
#         if not who.strip() and not (what.strip() and preparse_say_for_store(what)):
#             return
#         newline = (preparse_say_for_store(who),preparse_say_for_store(what))
#         readback_buffer.append(newline)
#         readback_prune()
#
#     current_line = None
#     def store_current_line(who, what):
#         global current_line
#         current_line = (preparse_say_for_store(who), preparse_say_for_store(what))
#
#     import re
#     remove_tags_expr = re.compile(r'{nw}|{image=.*?}|{/image}|{color=.*?}|{/color}|{a=.*?}|{/a}|{font=.*?}|{/font}|{size=.*?}|{/size}')
#     def preparse_say_for_store(input):
#         global remove_tags_expr
#         if input:
#             cleaned = re.sub(remove_tags_expr, "", input)
#             return cleaned.replace("\n","")
#
#
#     def readback_prune():
#         global readback_buffer
#         while len(readback_buffer) > max_readback_buffer_length:
#             del readback_buffer[0]
#
#     def readback_catcher():
#         ui.add(renpy.Keymap(rollback=ui.callsinnewcontext("text_history")))
#         ui.add(renpy.Keymap(rollforward=ui.returns(None)))
#
#     if not config.rollback_enabled:
#         config.overlay_functions.append(readback_catcher)
#

    def increase_rb_yoffset():

        store_rb_yoffset(yadj.get_value() + hb_rescale_y(50))

    def decrease_rb_yoffset():

        store_rb_yoffset(yadj.get_value() - hb_rescale_y(50))

#     def store_rb_yoffset(input):
#
#         yadj.change(input)
#
#     def wdt_on(do_with=True, trans=None):
#         global _window
#         _window = True
#         if do_with:
#             renpy.with_statement(trans)
#
#     def wdt_off(do_with=True, trans=None):
#         global _window
#         _window = False
#         if do_with:
#             renpy.with_statement(trans)
#
#
#     tl_note=None
#     tl_note_id=None
#
#     def add_note(id, text, author=""):
#         store.tl_notes[id] = (author, text)
#
#     def display_tl_note(new_tl_note_id=None):
#
#         global tl_note, tl_note_id, tl_notes
#         if new_tl_note_id != tl_note_id:
#             tl_note_id = new_tl_note_id
#             if tl_note_id==None:
#                 tl_note=None
#             elif tl_note_id in tl_notes:
#                 tl_note=tl_notes[tl_note_id]
#             renpy.restart_interaction()
#
#     def tl_note_overlay():
#
#         global tl_note
#         if tl_note and not config.skipping and _preferences.afm_time <= 0:
#             ui.frame(style="comment_frame")
#             ui.vbox()
#             if tl_note[0]:
#                 ui.text(tl_note[0], style="comment_label")
#             ui.text(tl_note[1], style="comment_text")
#             ui.close()
#         else:
#             tl_note=None
#
#     config.overlay_functions.append(tl_note_overlay)
#
#
#
#
#
#
#
#
#
#     def tl_dismiss(a):
#
#         display_tl_note()
#         return True
#
#     import re
#     rh_expr = re.compile(r'{a=.*?}|{/a}')
#     def remove_hyperlinks(input):
#
#         global rh_expr
#         if not persistent.commentary_on:
#             return re.sub(rh_expr, "", input)
#         else:
#             return input
#
#
#     config.hyperlink_callback = tl_dismiss
#     config.hyperlink_focus = display_tl_note
#     config.say_menu_text_filter = remove_hyperlinks
#
#
#
#     def viewportkeys(upfunc, downfunc):
#
#         ui.add(renpy.Keymap(mousedown_4=upfunc))
#         ui.add(renpy.Keymap(K_PAGEUP=upfunc))
#         ui.add(renpy.Keymap(mousedown_5=downfunc))
#         ui.add(renpy.Keymap(K_PAGEDOWN=downfunc))
#
#     def make_percentage(part, total, precision=0, clamp=True):
#         import math
#         if part >= total and clamp:
#             if precision == 0:
#                 return 100
#             else:
#                 return 100.0
#         elif precision == 0:
#             if total > 0:
#                 return int(math.floor((float(part) / float(total)) * 100.0))
#             else:
#                 return 0
#         else:
#             if total > 0:
#                 return round((float(part) / float(total)) * 100.0, precision)
#             else:
#                 return 0.0
#
#     def get_music_name():
#         for (name, file) in ex_m_tracks:
#             if file == renpy.music.get_playing():
#                 return name
#         return None
#
#     def get_completion_percentage():
#         seen = 0
#         if get_available_scenes():
#             seen = len(get_available_scenes())
#         total = len(get_available_scenes(include_locked=True, exclude_hidden=True))
#         return make_percentage(seen, total, 0)
#
#     def get_available_scenes(filter=False, include_locked=False, exclude_hidden=False, include_acts=False):
#
#         available_scenes = []
#         for (name, label, display, path) in s_scenes:
#             if (renpy.has_label(label) and display) or (include_acts and label == rp_actmark):
#                 if not filter or filter == path or (isinstance(path, tuple) and filter in path):
#                     unlocked = False
#                     if (persistent_seen(label) or has_devlvl()) and not exclude_hidden:
#                         unlocked = True
#                     if (include_locked and display) or unlocked:
#                         available_scenes.append((name, label, display, path, unlocked))
#         if len(available_scenes) > 0:
#             return available_scenes
#         else:
#             return False
#
#     def get_available_images():
#
#         global ex_g_images
#         available_images = []
#         for thumb in ex_g_images:
#             if isinstance(thumb, tuple):
#                 for image in thumb:
#                     if tuple(image.split()) in persistent._seen_images or has_devlvl():
#                         available_images.append(image)
#             else:
#                 if tuple(thumb.split()) in persistent._seen_images or has_devlvl():
#                     available_images.append(thumb)
#         if len(available_images) > 0:
#             return available_images
#         else:
#             return False
#
#     def get_available_music():
#
#         global ex_m_tracks
#         available_music = []
#         for title, filename in ex_m_tracks:
#             if renpy.seen_audio(filename) or has_devlvl():
#                 available_music.append(filename)
#         if len(available_music) > 1:
#             return available_music
#         else:
#             return False
#
#
#     def has_devlvl():
#         global devlvl
#         if devlvl > 4:
#             return True
#         return False
#     def devlvl_I():
#         global devlvl
#         if devlvl==0:
#             devlvl=1
#         else:
#             devlvl=0
#     def devlvl_D():
#         global devlvl
#         if devlvl in (1,2):
#             devlvl+=1
#         else:
#             devlvl=0
#     def devlvl_Q():
#         global devlvl
#         if devlvl==3:
#             devlvl=4
#         else:
#             devlvl=0
#     def devlvl_N():
#         global devlvl
#         if devlvl==4:
#             renpy.sound.play(sfx_heartfast)
#             devlvl=5
#         else:
#             devlvl=0

    def widget_button(text,displayable,clicked=None,style='prefs_label',xsize=220,ysize=30,widgetyoffset=3,textxoffset=30,state="button", xpos=0, ypos=0):

        textbase = Text(text, style=style)
        texthover = Text(text, style=style, color="#000")
        textdisabled = Text(text, style=style, color="#00000019")
        thisbutton_hover = LiveComposite(hb_rescale_xy(xsize, ysize),
                                         hb_rescale_xy(0, widgetyoffset), hb_rescale_image(displayable),
                                         hb_rescale_xy(textxoffset, 0), texthover)
        if xpos or ypos:
            ui.fixed(xpos=hb_rescale_x(xpos), ypos=hb_rescale_y(ypos))
        if state == "disabled":
            thisbutton_disabled = LiveComposite(hb_rescale_xy(xsize, ysize),
                                                hb_rescale_xy(0, widgetyoffset), ib_disabled(hb_rescale_image(displayable)),
                                                hb_rescale_xy(textxoffset, 0), textdisabled)
            ui.imagebutton(thisbutton_disabled,thisbutton_disabled,clicked=None)
        elif state == "active":
            ui.imagebutton(thisbutton_hover,thisbutton_hover,clicked=None)
        else:
            thisbutton = LiveComposite(hb_rescale_xy(xsize, ysize),
                                       hb_rescale_xy(0, widgetyoffset), ib_base(hb_rescale_image(displayable)),
                                       hb_rescale_xy(textxoffset, 0), textbase)
            ui.imagebutton(thisbutton, thisbutton_hover, clicked=clicked)
        if xpos or ypos:
            ui.close()

    def return_button(wherefunc, text=None, xpos=540, ypos=450):
        if text == None:
            text = displayStrings.return_button_text

        ui.add(renpy.Keymap(game_menu=wherefunc))
        widget_button(text, "ui/bt-return.png", wherefunc, xsize=100, xpos=xpos, ypos=ypos)

#     def time_from_seconds(seconds):
#
#         mins, secs = divmod(int(float(seconds)), 60)
#         hours, minutes = divmod(mins, 60)
#         playhours = str(hours)
#         if len(str(minutes)) == 1:
#             playminutes = "0" + str(minutes)
#         else:
#             playminutes = str(minutes)
#         return playhours+":"+playminutes

    def custom_render_savefile(index, name, filename, extra_info, screenshot, mtime, newest, clickable, has_delete, **positions):

        if clickable:
            clicked = ui.returns((filename, True))
        else:
            clicked = None

        info_split = extra_info.split("#")
        scene_name = name_from_label(info_split[0])
        if len(info_split) > 1:
            playtime = time_from_seconds(info_split[1])
        else:
            playtime = "x:xx"

        if is_autosave(name):

            name = name[5:]

        ui.vbox()
        ui.hbox()
        ui.button(style=style.file_picker_entry[index],
                  clicked=clicked,
                  **positions)
        ui.hbox(style=style.file_picker_entry_box[index])
        ui.add(screenshot)

        ui.null(width=hb_rescale_x(10))


        headstyle_o = ""
        headstyle_c = ""
        if newest:
            headstyle_o = "{b}"
            headstyle_c = "{/b}"


        s = config.file_entry_format % dict(
            time=headstyle_o+renpy.time.strftime(config.time_format,
                                     renpy.time.localtime(mtime))+headstyle_c,
            save_name=scene_name, playtime=playtime)
        ui.text(s, style=style.file_picker_extra_info[index])
        ui.close()
        ui.vbox()
        ui.null(height=hb_rescale_y(6))
        if has_delete:
            ui.imagebutton(ib_base(hb_rescale_image("ui/bt-del.png")), hb_rescale_image("ui/bt-del.png"), clicked=ui.returns(("delete", filename)))
        ui.close()
        ui.close()
        ui.null(height=hb_rescale_y(3))
        ui.close()

#     def is_autosave(fn):
#         if fn.startswith("auto-"):
#             return True
#         return False

    def custom_file_picker(selected, save, mode="manual", background = None):

        global auto_offset, displayed_slots
        saved_games = renpy.list_saved_games(regexp=r'(auto-|quick-)?[0-9]+')
        newest = None
        newest_mtime = None
        newest_auto = None
        newest_automtime = None
        save_info = { }
        positions = { }
        for fn, extra_info, screenshot, mtime in saved_games:
            save_info[fn] = (extra_info, screenshot, mtime)
            if mtime > newest_mtime:
                newest = fn
                newest_mtime = mtime
            elif is_autosave(fn) and mtime > newest_automtime:
                newest_auto = fn
                newest_automtime = mtime

        def save_slot_sort(in_1, in_2):
            if len(in_1[0]) > len(in_2[0]):
                return 1
            elif len(in_1[0]) < len(in_2[0]):
                return -1
            if in_1[0] > in_2[0]:
                return 1
            elif in_1[0] < in_2[0]:
                return -1
            else:
                return 0


        while True:

            displayed_manual = []
            displayed_auto = []

            for fn, extra_info, screenshot, mtime in saved_games:
                if not is_autosave(fn):
                    displayed_manual.append((fn, fn, False))
                else:
                    displayed_auto.append((fn, fn, True))

            if len(displayed_manual) < 1 and len(displayed_auto) > 0:
                mode = "auto"

            if mode == "auto" and not selected == "save":
                displayed_slots = displayed_auto
                newest = newest_auto
                has_delete = False
            else:
                displayed_slots = displayed_manual
                has_delete = True

            displayed_slots.sort(cmp=save_slot_sort)

            scrollable = False
            if len(displayed_slots) > 5:
                scrollable = True

            if scrollable:
                auto_offset = 1.0 / (len(displayed_slots) - 5)
                # TODO
                auto_offset = 59
            else:
                auto_offset = 0

            if background:
                ui.image(background)
            layout.navigation(None)

            # TODO
            ui.window(style='file_picker_frame', xminimum=470)

            ui.vbox()

            if selected == "save":
                ui.text(displayStrings.save_page_caption, style="page_caption")
            else:
                ui.text(displayStrings.load_page_caption, style="page_caption")

            def entry(name, filename, offset, ro):
                place = positions.get("entry_" + str(offset + 1), { })

                if filename not in save_info:
                    clickable = save and not ro
                    _render_new_slot(offset, name, filename, clickable, **place)
                else:
                    clickable = not save or not ro
                    extra_info, screenshot, mtime = save_info[filename]
                    custom_render_savefile(offset, name, filename, extra_info, screenshot, mtime, newest == filename, clickable, has_delete, **place)

            def store_yoffset(input=persistent.fpicker_yoffset):
                global auto_offset, displayed_slots
                maxheight = auto_offset * (len(displayed_slots) - 5)
                if input > maxheight:
                    input = maxheight
                elif input < 0:
                    input = 0
                persistent.fpicker_yoffset = input
                yadj.change(input)

            def increase_yoffset():
                global auto_offset
                store_yoffset(persistent.fpicker_yoffset + auto_offset)

            def decrease_yoffset():
                global auto_offset
                store_yoffset(persistent.fpicker_yoffset - auto_offset)

            if not persistent.fpicker_yoffset:
                persistent.fpicker_yoffset = 0.0
            elif persistent.fpicker_yoffset > auto_offset * (len(displayed_slots) - 5) or persistent.fpicker_yoffset == -1:
                persistent.fpicker_yoffset = auto_offset * (len(displayed_slots) - 5)

            yadj = ui.adjustment(range=auto_offset * (len(displayed_slots) - 5), value=persistent.fpicker_yoffset, changed=store_yoffset)
            vp = ui.viewport(yadjustment=yadj, mousewheel=False, draggable=False, xmaximum=hb_rescale_x(400), ymaximum = hb_rescale_y(296), xpos=0, ypos = hb_rescale_y(2))
            ui.vbox(xfill=True)

            maxname = 0

            for i, (filename, name, ro) in enumerate(displayed_slots):
                entry(name, filename, i, ro)
                maxname = name

            ui.close()
            ui.close()

            ui.vbox(xpos = hb_rescale_x(600), yalign=0.493, box_spacing=hb_rescale_y(2))
            if scrollable:
                ui.imagebutton(ib_base(hb_rescale_image("ui/bt-vscrollup.png")), hb_rescale_image("ui/bt-vscrollup.png"), clicked=decrease_yoffset)
                ui.bar(style='vscrollbar', changed=store_yoffset, adjustment=yadj)
                ui.imagebutton(ib_base(hb_rescale_image("ui/bt-vscrolldown.png")), hb_rescale_image("ui/bt-vscrolldown.png"), clicked=increase_yoffset)
            else:
                ui.imagebutton(ib_disabled(hb_rescale_image("ui/bt-vscrollup.png")), hb_rescale_image("ui/bt-vscrollup.png"), clicked=None)
                ui.bar(style='vscrollbar_disabled')
                ui.imagebutton(ib_disabled(hb_rescale_image("ui/bt-vscrolldown.png")), hb_rescale_image("ui/bt-vscrolldown.png"), clicked=None)
            ui.close()
            if selected == "save":
                nextsaveslot=1
                nextsaveslot = int(maxname) + 1
                widget_button(displayStrings.new_save_button, "ui/bt-star.png", ui.returns((str(nextsaveslot),False)), xsize=340, widgetyoffset=0, xpos=180, ypos=450)
            elif config.has_autosave:
                style_auto_b = "mm"
                clicked_auto_b = ui.returns(("_setmode", "auto"))
                if len(displayed_auto) < 1:
                    clicked_auto_b = None
                style_manual_b = "mm"
                clicked_manual_b = ui.returns(("_setmode", "manual"))
                if len(displayed_manual) < 1:
                    clicked_manual_b = None

                if mode == "auto":
                    style_auto_b = "rpa_active"
                    clicked_auto_b = None
                else:
                    style_manual_b = "rpa_active"
                    clicked_manual_b = None

                ui.hbox(xpos=hb_rescale_x(180), ypos=hb_rescale_y(450))
                layout.button(displayStrings.show_manual_saves, style_manual_b, clicked=clicked_manual_b)
                layout.button(displayStrings.show_auto_saves, style_auto_b, clicked=clicked_auto_b)
                ui.close()

            return_button(ui.returns(("return","return")))

            viewportkeys(decrease_yoffset,increase_yoffset)

            value = ui.interact()
            return value

#     def automode_f():
#
#
#         def clicked():
#             _preferences.afm_time = 0
#             config.skipping = None
#             renpy.restart_interaction()
#
#
#         if _preferences.afm_time == 0:
#             return
#         elif renpy.current_interact_type() != "menu":
#
#             ui.add(renpy.Keymap(dismiss=clicked))
#             ui.add(renpy.Keymap(rollforward=clicked))
#             ui.add(renpy.Keymap(rollback=clicked))
#
#     def indicator_icons():
#         res = []
#         if _preferences.afm_time != 0:
#             res.append("{image=ui/sd-auto.png}")
#         if config.skipping == "slow" and config.allow_skipping:
#             res.append("{image=ui/sd-skip.png}")
#         if persistent.is_muted:
#             res.append("{image=ui/sd-mute.png}")
#         ui.text(' '.join(res), xanchor=1.0, xpos=790, ypos=10 )
#
#
#     config.overlay_functions.append(automode_f)
#     config.overlay_functions.append(indicator_icons)
#
#     def bc_backup():
#         store.breadcrumbs = copy.deepcopy(persistent.breadcrumbs)
#
#
#
#
#     config.overlay_functions.append(bc_backup)
#
#
#     def _prompt(screen, message, image=None, isyesno = False, background=None, transition=config.intra_transition, interact = True):
#
#         renpy.transition(transition)
#         layout.navigation(screen)
#         if background:
#             ui.image(background)
#         ui.image(style.gm_root.background)
#         ui.window(style='yesno_frame')
#         ui.vbox(style='yesno_frame_vbox')
#         layout.prompt(message, "yesno")
#         ui.hbox(style='yesno_button_hbox')
#         if isyesno:
#             layout.button(displayStrings.yesno_yes, 'yesno', clicked=ui.returns(True))
#             layout.button(displayStrings.yesno_no, 'yesno', clicked=ui.returns(False))
#             ui.add(renpy.Keymap(game_menu=ui.returns(False)))
#         elif interact:
#             ui.saybehavior()
#             layout.button(displayStrings.yesno_okay, 'yesno', clicked=ui.returns(True))
#             ui.add(renpy.Keymap(game_menu=ui.returns(True)))
#         ui.close()
#         ui.close()
#         if image:
#             ui.image(image)
#         if interact:
#             rv =  ui.interact()
#         else:
#             rv = False
#         renpy.transition(config.intra_transition)
#         return rv
#
#     def _yesno_prompt(screen, message, image=None, background=None, transition=config.intra_transition):
#
#         return _prompt(screen, message, image=image, isyesno = True, background=background, transition=transition)
#
#     class TogglePreference(object):
#         """
#         This is a class that's used to represent a 2-choice preference.
#         We'll completely rewrite the presentation function (so this is not a general Ren'Py module, sorry tom) and reinit the classes.
#         For the original documentation, see the class _Preference in 00preferences.rpy
#         """
#         def __init__(self, name, field, boolmap=(False,True), base=_preferences):
#
#
#             self.name = name
#             self.field = field
#             self.values = []
#             self.boolmap = boolmap
#             self.base = base
#
#             config.all_preferences[name] = self
#
#         def render_preference(self, thisxpos=0, thisypos=0, disabled=False):
#             cur = getattr(self.base, self.field)
#
#             if cur == self.boolmap[0]:
#                 boolCur = False
#                 checkboximage = "ui/bt-cf-unchecked.png"
#             elif cur == self.boolmap[1]:
#                 boolCur = True
#                 checkboximage = "ui/bt-cf-checked.png"
#             else:
#                 boolCur = cur
#                 checkboximage = "ui/bt-cf-unchecked.png"
#
#             def clicked(value=boolCur):
#                 if value == True:
#                     writevalue = self.boolmap[0]
#                 elif value == False:
#                     writevalue = self.boolmap[1]
#                 else:
#                     writevalue = value
#
#                 setattr(self.base, self.field, writevalue)
#                 return True
#
#             if disabled:
#                 widget_button(self.name, checkboximage, clicked, xsize=325, widgetyoffset=0, state="disabled")
#             else:
#                 widget_button(self.name, checkboximage, clicked, xsize=325, widgetyoffset=0)

    class customSliderPreference(object):

        def __init__(self, name, range, get, set, enable='True'):

            self.name = name
            self.range = range
            self.get = get
            self.set = set
            self.enable = enable

            config.all_preferences[name] = self

        def render_preference(self, thisxpos=0, thisypos=0):

            if not eval(self.enable):
                return

            def changed(v):
                self.set(v)

            ui.hbox()
            ui.bar(self.range,
                   self.get(),
                   changed=changed,
                   style=style.prefs_slider[self.name])
            ui.null(width=hb_rescale_x(15))
            layout.label(self.name, "prefs")
            ui.close()

    class customVolumePreference(object):

        def __init__(self, name, mixer, enable='True', sound='None', channel=0):

            self.name = name
            self.mixer = mixer
            self.enable = enable
            self.sound = sound
            self.channel = channel
            self.steps = 1.0

            config.all_preferences[name] = self

        def render_preference(self, thisxpos=0, thisypos=0):

            if not eval(self.enable):
                return

            def changed(v):
                if persistent.is_muted:
                    mute_toggle()
                newvol = self.fader_characteristic(v)
                if newvol == 0.0:
                    newvol = config.minimumvolume
                _preferences.set_volume(self.mixer, newvol)

            ui.hbox()
            ui.bar(self.steps,
                   self.get_slider_position(),
                   changed=changed,
                   style=style.prefs_volume_slider[self.name])
            ui.null(width=hb_rescale_x(15))
            layout.label(self.name, "prefs")
            ui.close()

        def fader_characteristic(self, v):

            import math
            return 1.0 - math.cos(v * math.pi / 2.0)

        def fader_characteristic_inverse(self,v):
            import math
            return (2.0 / math.pi) * math.acos(1.0 - v)

        def get_slider_position(self):
            if persistent.is_muted:
                vol = persistent.oldvol[self.mixer]
            else:
                vol = _preferences.get_volume(self.mixer)
            if vol == config.minimumvolume:
                return 0.0
            rv = self.fader_characteristic_inverse(vol)
            return rv

#     def cps_get():
#
#         cps = _preferences.text_cps
#         if cps == 0:
#             cps = 150
#         else:
#             cps -= 1
#         return cps
#
#     def cps_set(cps):
#
#         cps += 1
#         if cps == 151:
#             cps = 0
#         _preferences.text_cps = cps
#
#     def afm_get():
#
#
#         afm = persistent.afm_time
#         afm -= 1
#         if afm < 1:
#             afm = 1
#         return afm
#
#     def afm_set(afm):
#
#         afm += 1
#         if afm > 40:
#             afm = 40
#         if _preferences.afm_time > 0:
#             _preferences.afm_time = afm
#         persistent.afm_time = afm
#
#
#
#     def initialize_prefs():
#         store.fullscreen_p = TogglePreference(displayStrings.config_fullscreen_label, 'fullscreen')
#         store.unreadskip_p = TogglePreference(displayStrings.config_skip_unseen_label, 'skip_unseen')
#         store.choiceskip_p = TogglePreference(displayStrings.config_skip_after_choice_label, 'skip_after_choices')
#         store.textspeed_p = customSliderPreference(displayStrings.config_textspeed_label, 150, cps_get, cps_set)
#         store.afm_p = customSliderPreference(displayStrings.config_afmspeed_label, 40, afm_get, afm_set)
#         store.musicvol_p = customVolumePreference(displayStrings.config_musicvol_label, 'music')
#         store.musicvol_p_jukebox = customVolumePreference(displayStrings.config_musicvol_jukebox_label, 'music')
#         store.sfxvol_p = customVolumePreference(displayStrings.config_sfxvol_label, 'sfx')
#
#
#
#
#
#
#
#
#
#
#     def ksgallery_unlock(name):
#         if not isinstance(name, tuple):
#             name = tuple(name.split())
#         persistent._seen_images[name] = True
#
#
#     class GalleryGridLayout(object):
#         def __init__(self, gridsize, upperleft, offsets):
#             self.gridsize = gridsize
#             self.upperleft = upperleft
#             self.offsets = offsets
#
#         def __call__(self, imagenum, image_count):
#
#             cols, rows = self.gridsize
#             ulx, uly = self.upperleft
#             ox, oy = self.offsets
#
#             return dict(
#                 xpos = ulx + (imagenum % cols) * ox,
#                 ypos = uly + (imagenum // cols) * oy,
#                 )
#
#     class GalleryAllPriorCondition(object):
#
#         def check(self, all_prior):
#             return all_prior
#
#     class GalleryArbitraryCondition(object):
#         def __init__(self, condition):
#             self.condition = condition
#
#         def check(self, all_prior):
#             return eval(self.condition)
#
#     class GalleryUnlockCondition(object):
#         def __init__(self, images):
#             self.images = images
#
#         def check(self, all_prior):
#             for i in self.images:
#                 if tuple(i.split()) not in persistent._seen_images:
#                     return False
#
#             return True
#
#
#     class GalleryImage(object):
#         def __init__(self, gallery, images, displayable):
#             self.gallery = gallery
#             self.images = images or [ ]
#             self.displayable = displayable
#             self.conditions = [ ]
#
#         def check_unlock(self, all_prior):
#             for i in self.conditions:
#                 if not i.check(all_prior):
#                     return False
#
#             return True
#
#         def show_locked(self, image_num, image_count):
#             renpy.transition(self.gallery.transition)
#             self.gallery.locked_image(image_num, image_count)
#             ui.saybehavior()
#             ui.interact()
#
#         def show(self, image_num, image_count):
#             renpy.transition(self.gallery.transition)
#
#             renpy.scene()
#             renpy.show("ksgallerybg")
#
#             for i in self.images:
#                 renpy.show(i)
#
#             if self.displayable:
#                 ui.add(self.displayable)
#
#             ui.saybehavior()
#             ui.interact()
#
#     class GalleryButton(object):
#         def __init__(self, gallery, idle, hover, insensitive, properties):
#             self.gallery = gallery
#             self.idle = idle
#             self.hover = hover
#             self.insensitive = insensitive
#             self.properties = properties
#             self.images  = [ ]
#             self.conditions = [ ]
#
#         def check_unlock(self):
#             for i in self.conditions:
#                 if not i.check(True):
#                     return False
#
#             for i in self.images:
#                 if i.check_unlock(False):
#                     return True
#
#             return False
#
#         def render(self, i, pos):
#             props = pos.copy()
#             props.update(self.properties)
#
#             if not self.check_unlock():
#                 insensitive = self.insensitive or self.gallery.locked_button
#                 if insensitive is not None:
#                     ui.fixed(**props)
#                     ui.image(insensitive)
#                     ui.close()
#                     return
#
#             if self.hover:
#                 ui.imagebutton(self.idle,
#                                self.hover,
#                                clicked=ui.returns(("button", i)),
#                                **props)
#
#             else:
#                 ui.fixed(**props)
#                 ui.image(self.idle)
#                 ui.imagebutton(self.gallery.idle_border,
#                                self.gallery.hover_border,
#                                clicked=ui.returns(("button", i)))
#                 ui.close()
#
#         def show(self):
#
#             all_prior = True
#
#
#             im_sort = []
#             im_locked = []
#             im_locktext = ""
#             for i, img in enumerate(self.images):
#                 if img.check_unlock(all_prior):
#                     im_sort.append((i, img))
#                 else:
#                     im_locked.append((i, img))
#             im_sort.extend(im_locked)
#
#
#             for i, img in im_sort:
#                 ui.add(renpy.Keymap(game_menu=ui.returns(True)))
#                 if img.check_unlock(all_prior):
#                     img.show(i, len(self.images))
#                 else:
#                     if len(im_locked) == 1:
#                         self.gallery.locked_images_screen(displayStrings.gallery_onelocked)
#                     else:
#                         self.gallery.locked_images_screen(_(displayStrings.gallery_manylocked) % (len(im_locked)))
#                     return
#
#
#
#
#     class GalleryPage(object):
#
#         def __init__(self, gallery, name, background):
#             self.gallery = gallery
#             self.name = name
#             self.background = background
#             self.buttons = [ ]

    class Gallery(object):

        transition = dissolve

        locked_button = None
        locked_background = "#000"

        hover_border = None
        idle_border = None

        background = None

        def __init__(self):
            self.pages = [ ]

            self.page_ = None
            self.button_ = None
            self.image_ = None
            self.unlockable = None

        def page(self, name, background=None):

            self.page_ = GalleryPage(self, name, background)
            self.pages.append(self.page_)

        def button(self, idle, hover=None, locked=None, **properties):
            self.button_ = GalleryButton(self, idle, hover, locked, properties)
            self.page_.buttons.append(self.button_)
            self.unlockable = self.button_

        def image(self, *images):
            self.image_ = GalleryImage(self, images, None)
            self.button_.images.append(self.image_)
            self.unlockable = self.image_

        def display(self, displayable):
            self.image_ = GalleryImage(self, [ ], displayable)
            self.button_.images.append(self.image_)
            self.unlockable = self.image_

        def unlock(self, *images):
            self.unlockable.conditions.append(GalleryUnlockCondition(images))

        def condition(self, condition):
            self.unlockable.conditions.append(GalleryArbitraryCondition(condition))

        def allprior(self):
            self.unlockable.conditions.append(GalleryAllPriorCondition())

        def unlock_image(self, *images):
            self.image(*images)
            self.unlock(*images)

        def navigation(self, page_name, page_num, pages, currentpage=0):

            ui.hbox(background=None,xpos=hb_rescale_x(180),ypos=hb_rescale_y(448))
            ui.text(displayStrings.gallery_num_page_prefix+": ", style="gallery_pager_desc")


            wrap = False

            if len(self.pages) > 1:
                myclick = ui.returns(("page", currentpage - 1))
                if currentpage <= 0:
                    myclick = None
                    if wrap:
                        myclick = ui.returns(("page", len(self.pages) - 1))
                layout.button("<",
                    "gallery_nav",
                    selected=False,
                    clicked=myclick)

            for i, p in enumerate(self.pages):
                mysel = False
                myclick = ui.returns(("page", i))
                if i == page_num:
                    mysel = True
                    myclick = False
                layout.button(p.name,
                                "gallery_nav",
                                selected=mysel,
                                clicked=myclick)

            if len(self.pages) > 1:
                myclick = ui.returns(("page", currentpage + 1))
                if currentpage >= (len(self.pages) - 1):
                    myclick = None
                    if wrap:
                        myclick = ui.returns(("page", 0))
                layout.button(">",
                    "gallery_nav",
                    selected=False,
                    clicked=myclick)

            ui.close()


        def grid_layout(self, gridsize, upperleft, offsets):
            self.layout = GalleryGridLayout(gridsize, upperleft, offsets)

        def layout(self, i, n):
            return { }

        def locked_image(self, num, total):
            ui.add(self.locked_background)
            ui.text(_(displayStrings.gallery_singlelocked) % (num + 1, total), xalign=0.5, yalign=0.7, style="prefs_label")

        def locked_images_screen(self, text):
            renpy.transition(self.transition)
            ui.add(self.locked_background)
            ui.text(text, xalign=0.5, ypos=hb_rescale_y(400), style="prefs_label")
            ui.saybehavior()
            ui.interact()

        def show(self, page=0):

            while True:
                renpy.transition(self.transition)

                gallery_predict()

                p = self.pages[page]

                bg = p.background or self.background
                if bg is not None:
                    renpy.scene()
                    ui.add(bg)

                ui.text(displayStrings.gallery_page_caption, style="page_caption", xpos=hb_rescale_x(180),ypos=hb_rescale_y(120))
                if len(self.pages) > 1:
                    self.navigation(p.name, page, len(self.pages),page)

                for i, b in enumerate(p.buttons):
                    pos = self.layout(i, len(p.buttons))
                    b.render(i, pos)

                return_button(_intra_jumps("extra_menu", "intra_transition"))

                cmd, arg = ui.interact(suppress_overlay=True, suppress_underlay=True)

                if cmd == "return":
                    return

                elif cmd == "page":
                    page = arg
                    continue

                elif cmd == "button":
                    p.buttons[arg].show()
                    continue


        def autobutton(self, in_images):

            thumbnail = False
            if in_images == "":
                return
            if isinstance(in_images, tuple):
                thumb_base = hb_rescale_image("ui/bg-ex-gallery-lockedimage.png")
                for image in in_images:
                    if image.startswith("thumb/"):
                        thumbnail = hb_rescale_image("event/" + image)
                        break
                    if tuple(image.split()) in persistent._seen_images or has_devlvl():
                        thumb_base = image
                        break
            else:
                thumb_base = hb_rescale_image(in_images)
                in_images = (in_images,)
            if not thumbnail:
                thumbnail = im.Scale(ImageReference(thumb_base), hb_rescale_x(100), hb_rescale_y(75))
            button_base = LiveComposite(hb_rescale_xy(110, 85),
                                        (0, 0), ib_base(hb_rescale_image("ui/bt-cg-locked.png")),
                                        hb_rescale_xy(5, 5), im.MatrixColor(thumbnail, im.matrix.desaturate()))
            button_hover = LiveComposite(hb_rescale_xy(110, 85),
                                         (0, 0), hb_rescale_image("ui/bt-cg-locked.png"),
                                         hb_rescale_xy(5, 5), thumbnail)
            self.button(button_base, button_hover)
            for in_image in in_images:
                if not in_image.startswith("thumb/"):
                    if has_devlvl():
                        self.image(in_image)
                    else:
                        self.unlock_image(in_image)

#     def gallery_predict():
#
#         return

    def doublespeak(char0, char1, msg0, msg1=False):

        global current_line
        speaker=dict()
        ctc=dict()
        for (n, char) in enumerate((char0,char1)):
            if hasattr(char,"name"):
                if hasattr(char,"dynamic") and char.dynamic == True:
                    myname = eval(char.name)
                else:
                    myname = char.name
                if hasattr(char,"who_args") and "color" in char.who_args:
                    speaker[n] = "{color="+char.who_args["color"]+"}"+myname+"{/color}"
                else:
                    speaker[n] = myname
            else:
                speaker[n] = str(char)
            if hasattr(char,"display_args") and "ctc" in char.display_args:
                ctc[n] = char.display_args["ctc"]
            else:
                ctc[n] = config.nvl_page_ctc

        msg0 = char0.what_prefix + msg0 + char0.what_suffix
        if not msg1:
            msg1 = msg0
        else:
            msg1 = char1.what_prefix + msg1 + char1.what_suffix

        current_line = None
        if msg0 == msg1:
            store_say(speaker[0] + " & " + speaker[1], msg0)
        else:
            store_say(speaker[0], msg0)
            store_say(speaker[1], msg1)

        renpy.shown_window()
        ui.frame(background=hb_rescale_image("ui/bg-doublespeak.png"), yalign=1.0, yminimum=hb_rescale_y(160), style="say_window")
        ui.grid(2,1,xfill=True)
        ui.vbox(style="say_vbox")
        ui.text(speaker[0], style="say_label", **displayStrings.styleoverrides)
        ui.text(msg0, slow=True, style="say_dialogue", xmaximum=hb_rescale_x(350), **displayStrings.styleoverrides)
        ui.close()
        ui.vbox(style="say_vbox", xpos=hb_rescale_x(20))
        ui.text(speaker[1], style="say_label", **displayStrings.styleoverrides)
        ui.text(msg1, slow=True, style="say_dialogue", xmaximum=hb_rescale_x(350), **displayStrings.styleoverrides)
        ui.close()
        ui.close()

        ui.fixed(xpos=hb_rescale_x(-400))
        ui.image(ctc[0])
        ui.close()
        ui.image(ctc[1])

        ui.saybehavior(afm=msg0+msg1)
        ui.interact(roll_forward=True, type="say")
        renpy.checkpoint()

#     def written_note(text, window_args={}, text_args={}, quiet=False):
#         global current_line, _window
#
#         def note_widget(text=text, window_args=window_args, text_args=text_args):
#
#             default_text_args = displayStrings.styleoverrides.copy()
#             default_text_args.update(text_args)
#
#             ui.tag("written_note")
#             ui.frame(style="note_window", **window_args)
#             ui.vbox()
#             ui.text("", style="note_text")
#             ui.text(text, style="note_text", **default_text_args)
#             ui.text("", style="note_text")
#             ui.close()
#             ui.image(centered.display_args["ctc"])
#             ui.saybehavior(afm=text)
#
#         renpy.shown_window()
#
#         current_line = None
#         store_say(displayStrings.text_history_note, text.replace("\n\n","\n"))
#
#         if not quiet:
#             renpy.music.play(sfx_paper, channel="sound")
#
#         ui.at(note_enter)
#         note_widget()
#
#         ui.interact(roll_forward=True, type="say")
#         renpy.checkpoint()
#
#         ui.at(note_exit)
#         note_widget()
#
#         renpy.shown_window()
#         renpy.pause(0.5)
#         renpy.hide("written_note")

    def extra_button(text,in_displayable,clicked=None,style='prefs_label',state="button"):

        image = hb_rescale_image(in_displayable[0:-4]+"-c.png")
        imagebase = VBox(hb_rescale_image(in_displayable), xalign=0.5)
        imagedisabled = VBox(ib_base(hb_rescale_image(in_displayable)), xalign=0.5)
        textbase = Text(text, style=style, xalign=0.5)
        texthover = Text(text, style=style, color="#000", xalign=0.5)
        textdisabled = Text(text, style=style, color="#00000019", xalign=0.5)

        if state == "return":
            textbase = LiveComposite(hb_rescale_xy(100, 30),
                                     hb_rescale_xy(0, 3), ib_base(hb_rescale_image("ui/bt-return.png")),
                                     hb_rescale_xy(30, 0), textbase)
            texthover = LiveComposite(hb_rescale_xy(100, 30),
                                     hb_rescale_xy(0, 3), hb_rescale_image("ui/bt-return.png"),
                                     hb_rescale_xy(30, 0), texthover)

        if state == "disabled":
            button_disabled = VBox(imagedisabled, textdisabled)
            ui.imagebutton(button_disabled,button_disabled,clicked=None, yalign=1.0)
            return
        button_base = VBox(imagebase, textbase)
        button_hover = VBox(image, texthover, xalign=0.5)
        ui.imagebutton(button_base,button_hover,clicked=clicked, yalign=1.0)

#     def toggle_commentary():
#
#         persistent.commentary_on = not persistent.commentary_on
#         return False
#
#     def toggle_h():
#
#         persistent.hdisabled = not persistent.hdisabled
#         return False
#
#
#     def increase_m_yoffset():
#
#         global auto_offset
#         store_m_yoffset(persistent.mpicker_yoffset + auto_offset)
#
#     def decrease_m_yoffset():
#
#         global auto_offset
#         store_m_yoffset(persistent.mpicker_yoffset - auto_offset)
#
#     def store_m_yoffset(input=persistent.mpicker_yoffset):
#
#         global auto_offset, ex_m_tracks
#         maxheight = auto_offset * (len(ex_m_tracks) - 8)
#         if input > maxheight:
#             input = maxheight
#         elif input < 0:
#             input = 0
#         persistent.mpicker_yoffset = input
#         yadj.change(input)
#
#
#     def joy_button(label,key):
#         myclicked = renpy.curry(joy_clicked)(label=label, key=key)
#         layout.button(_(label) + " - " + _(_preferences.joymap.get(key, displayStrings.gamepad_key_na)), "mm", clicked=myclicked, index=label)
#
#     def joy_clicked(label, key):
#         return renpy.invoke_in_new_context(set_binding, label, key)
#
#     def set_binding(label, key):
#         renpy.transition(config.intra_transition)
#         if mm_context():
#             bgimage = style.mm_static.background
#         else:
#             bgimage = None
#
#         message = displayStrings.gamepad_request_key % label
#
#         _prompt(None, message, background=bgimage, interact=False)
#
#         _joystick_get_binding()
#         ui.add(renpy.Keymap(game_menu=ui.returns(True)))
#         binding = ui.interact()
#         _joystick_take_binding(binding, key)
#
#         return True

    def refresh_label(a,b):

        global ss_desc
        disp = Text(ss_desc, xalign=0.5, yalign=0.98, size=hb_rescale_y(18), **displayStrings.styleoverrides)
        return (disp,0.05)

#     def ss_unhovered():
#
#         global ss_desc
#         ss_desc = ""
#
#     def ss_hovered(hover_text=None):
#
#         global ss_desc
#         if not hover_text or hover_text == True:
#             ss_desc = ""
#         else:
#             ss_desc = hover_text
#
#     def ss_button(text, hover_text=None, clicked=None, selected=False):
#
#         global ss_desc, ss_hovered, ss_unhovered
#         myhovered = renpy.curry(ss_hovered)(hover_text=hover_text)
#         layout.button(text, 'mm', clicked=clicked, hovered=myhovered, unhovered=ss_unhovered, selected=selected)
#
#     def store_s_yoffset(input=persistent.spicker_yoffset):
#
#         global auto_offset, available_scenes
#         maxheight = auto_offset * (len(available_scenes) - 8)
#         if input > maxheight:
#             input = maxheight
#         elif input < 0:
#             input = 0
#         persistent.spicker_yoffset = input
#         yadj.change(input)
#
#     def increase_s_yoffset():
#
#         global auto_offset
#         store_s_yoffset(persistent.spicker_yoffset + auto_offset)
#
#     def decrease_s_yoffset():
#
#         global auto_offset
#         store_s_yoffset(persistent.spicker_yoffset - auto_offset)
#
#     def toggle_playthrough():
#
#         global playthroughflag
#         playthroughflag = not playthroughflag
#         return ("_pt_toggled", playthroughflag)
#
#
#     def prefix_dict(indict, prefix=False, combine=False):
#         if not prefix:
#             return indict
#         outdict = {}
#         inkeys = indict.keys()
#         for key in inkeys:
#             outdict[prefix + key] = indict[key]
#
#         if combine:
#             outdict.update(indict)
#
#         return outdict

    class drugsDisp(renpy.Displayable):
        def __init__(self, width, height):
            super(drugsDisp, self).__init__(self)

            self.length = 22.0
            self.fadeintime = 1.0
            self.framelength = 0.04

            self.width = width
            self.height = height

            self.make_words()

        def make_words(self):

            from random import shuffle

            self.progress = 0

            self.singlewordlist = displayStrings.drugs_wordlist
            shuffle(self.singlewordlist)
            self.wordlist = self.singlewordlist * 5

            self.timeperword = self.length / len(self.wordlist)
            self.words = []
            self.disps = []
            for word in self.wordlist:
                thisword = object()
                thisword.payload = word
                thisword.position = self.randompos()
                thisword.size = self.randomsize()
                thisword.alpha = 0
                thisword.fulldisp = self.subdisp(thisword.payload,thisword.size,255)
                self.words.append(thisword)
                self.disps.append(thisword.fulldisp)

        def visit(self):
            return self.disps

        def subdisp(self, payload, size, alpha):
            font = srsfont
            if displayStrings.sayfont != mainfont:
                font = displayStrings.sayfont
            return Text(payload,size=size, color=(0,0,0,alpha), font=font)

        def randompos(self):
            from random import randrange
            return (randrange(self.width), randrange(self.height))

        def randomsize(self):
            from random import randrange
            return hb_rescale_y(randrange(30,100))

        def render(self, width, height, st, at):



            if st == 0:
                self.make_words()

            width = self.width
            height = self.height

            from math import floor

            rv = renpy.Render(width, height)
            rv.fill((255,255,255,255))


            for n, word in enumerate(self.words):

                if n > self.progress:
                    break

                if word.alpha < 255:
                    word.alpha = ((st - (n * self.timeperword)) / self.fadeintime) * 255
                    if word.alpha > 255:
                        word.alpha = 255
                    disp = self.subdisp(word.payload, word.size, word.alpha)
                else:
                    disp = word.fulldisp

                rend = renpy.render(disp,width,height,st,at)
                size = rend.get_size()
                rv.blit(rend, (word.position[0]-(size[0]/2), word.position[1]-(size[1]/2)))


            newprogress = int(floor(st / self.timeperword))
            self.progress = newprogress
            renpy.redraw(self, self.framelength)

            return rv

#     def datedisplay(date):
#         renpy.transition(fade)
#         ui.image(Solid("000"))
#         ui.text(date, xalign=0.5, yalign=0.5)
#         ui.pausebehavior(5.0)
#         ui.interact(suppress_overlay=True, suppress_underlay=True)
#         renpy.transition(fade)
#
#
#     def custom_movie_cutscene(filename, delay=None, loops=0, stop_music=True):
#         """
#         copypaste from renpy/exports.py, see docs there
#         """
#
#         if not filename in persistent.seen_videos:
#             config.skipping = None
#
#         if stop_music:
#             renpy.audio.audio.set_force_stop("music", True)
#
#         renpy.movie_start_fullscreen(filename, loops=loops)
#
#         if not filename in persistent.seen_videos:
#             renpy.ui.saybehavior(dismiss=['abort_video'])
#         else:
#             renpy.ui.saybehavior(dismiss=['abort_video','dismiss'])
#
#         if delay is None or delay < 0:
#             renpy.ui.soundstopbehavior("movie")
#         else:
#             renpy.ui.pausebehavior(delay, False)
#
#         if renpy.game.log.forward:
#             roll_forward = True
#         else:
#             roll_forward = None
#
#         rv = renpy.ui.interact(suppress_overlay=True,
#                                suppress_underlay=True,
#                                show_mouse=False,
#                                roll_forward=roll_forward)
#
#
#
#
#         if not filename in persistent.seen_videos:
#             persistent.seen_videos.append(filename)
#
#         renpy.movie_stop()
#
#         if stop_music:
#             renpy.audio.audio.set_force_stop("music", False)
#
#         return rv
#
#     renpy.movie_cutscene = custom_movie_cutscene

# ==============================================================================
# ui_ingamemenu.rpy
# ==============================================================================

init 500 python:
#     import sets
#
#     def menu(items, **add_input):
#
#         is_narrator = False
#         newitems = []
#         for label, val in items:
#             if val == None:
#                 narrator(label, interact=False)
#                 is_narrator=True
#             else:
#                 newitems.append((label, val))
#         rv = custom_menu(newitems, is_narrator, **add_input)
#         for label, val in items:
#             if rv == val:
#                 store_say(None, ">> " + label)
#         return rv

    def custom_menu(items, is_narrator, window_style='menu_window', interact=True, with_none=None, **kwargs):
        """
        Displays a menu containing the given items, returning the value of
        the item the user selects.

        @param items: A list of tuples that are the items to be added to
        this menu. The first element of a tuple is a string that is used
        for this menuitem. The second element is the value to be returned
        if this item is selected, or None if this item is a non-selectable
        caption.

        @param interact: If True, then an interaction occurs. If False, no suc
        interaction occurs, and the user should call ui.interact() manually.

        @param with_none: If True, performs a with None after the input. If None,
        takes the value from config.implicit_with_none.
        """

        renpy.choice_for_skipping()

        location = renpy.game.context().current


        choices = [ val for label, val in items ]
        while None in choices:
            choices.remove(None)


        roll_forward = renpy.exports.roll_forward_info()

        if roll_forward not in choices:
            roll_forward = None



        for choice in persistent.breadcrumbs:
            if choice[0] == location:
                rval = choice[1]
                renpy.ui.window(style=window_style, ypadding=40)
                show_menu(items, location=location, focus="choices", default=True, active=rval, **kwargs)
                ui.saybehavior(afm=generate_string(200))
                ui.interact(mouse='say', type="say", roll_forward=True)
                return rval

        if renpy.config.auto_choice_delay:
            renpy.ui.pausebehavior(renpy.config.auto_choice_delay,
                                   random.choice(choices))


        renpy.ui.window(style=window_style, ypadding=hb_rescale_y(40))
        show_menu(items, location=renpy.game.context().current, focus="choices", default=True, **kwargs)


        renpy.exports.shown_window()

        if interact:

            rv = renpy.ui.interact(mouse='menu', type="menu", roll_forward=roll_forward)

            renpy.checkpoint(rv)

            if with_none is None:
                with_none = renpy.config.implicit_with_none

            if with_none:
                renpy.game.interface.do_with(None, None)

            return rv

        return None

#     def show_menu(menuitems,
#              style = 'menu',
#              caption_style='menu_caption',
#              choice_style='menu_choice',
#              choice_chosen_style='menu_choice_chosen',
#              choice_button_style='menu_choice_button',
#              choice_chosen_button_style='menu_choice_chosen_button',
#              location=None,
#              focus=None,
#              default=False,
#              active=None,
#              **properties):
#
#
#
#         renpy.random.shuffle(menuitems)
#
#         ui.vbox(style=style, **properties)
#
#
#
#
#         for label, val in menuitems:
#             chosenbefore = False
#             if val is None:
#                 pass
#
#             else:
#
#                 text = choice_style
#                 button = choice_button_style
#
#                 if location:
#                     chosen = renpy.game.persistent._chosen.setdefault(location, sets.Set())
#                     if label in chosen:
#                         chosenbefore = True
#
#                     def clicked(chosen=chosen, label=label, val=val):
#                         chosen.add(label)
#                         persistent.breadcrumbs.append((location,val))
#                         return val
#                 else:
#                     clicked = renpy.ui.returns(val)
#
#                 if active is not None:
#                     ingamebutton_dead(label, val == active)
#                 else:
#                     ingamebutton(label, clicked, chosenbefore)
#
#
#         ui.close()
#

    def ingamebutton(text, clicked, previously=None):

        img = Fixed(Text(text, xalign=0.5, yalign=0.5, color="#000"), xmaximum=hb_rescale_x(608))
        if previously:
            bgim = im.Composite(None,
                                (0,0), hb_rescale_image("ui/bg-choice.png"),
                                hb_rescale_xy(577,2), im.Alpha(hb_rescale_image("ui/bt-cf-checked.png"),0.5))
        elif previously is not None:
            bgim = im.Composite(None,
                                (0,0), hb_rescale_image("ui/bg-choice.png"),
                                hb_rescale_xy(577,2), im.Alpha(hb_rescale_image("ui/bt-cf-unchecked.png"),0.5))
        else:
            bgim = hb_rescale_image("ui/bg-choice.png")

        bg = Fixed(bgim, xmaximum=hb_rescale_x(608))

        this_idle = LiveComposite(hb_rescale_xy(608,35),
                                  (0,0), ingm_bg(bg),
                                  (0,0), ingm_btn(img)
                                  )
        this_hover = LiveComposite(hb_rescale_xy(608,35),
                                  (0,0), ingm_bg(bg),
                                  (0,0), ingm_btn_hover(img)
                                  )

        ui.fixed(xpos=0.5, ymaximum=hb_rescale_y(35), xmaximum=hb_rescale_x(800))
        ui.imagebutton(this_idle, this_hover, clicked=clicked, style="default", xanchor=0.5, yanchor=0.5)
        ui.close()

    def ingamebutton_dead(text, is_active=False):

        clicked = None
        if is_active:
            img = Fixed(Text(text, xalign=0.5, yalign=0.5, color="#ccc"), xmaximum=hb_rescale_x(608))
            bg = Fixed(hb_rescale_image("ui/bg-choice_nochoice.png"), xmaximum=hb_rescale_x(608))
        else:
            img = Fixed(Text(text, xalign=0.5, yalign=0.5, color="#00000080"), xmaximum=hb_rescale_x(608))
            bg = Fixed(im.MatrixColor(hb_rescale_image("ui/bg-choice.png"),im.matrix.desaturate()*im.matrix.brightness(-0.2)*im.matrix.opacity(0.3)), xmaximum=hb_rescale_x(608))

        ui.fixed(xpos=0.5, ymaximum=hb_rescale_y(35), xmaximum=hb_rescale_x(800))
        ui.button(clicked=clicked, style="default", xanchor=0.5, yanchor=0.5)
        ui.image(ingm_bg(bg))
        ui.button(clicked=clicked, style="default", xanchor=0.5, yanchor=0.5)
        ui.image(ingm_bg(img))
        ui.close()

# init:
#
#     transform ingm_bg(disp):
#         disp
#         yalign 0.5 xalign 0.5 alpha 0.0 zoom 1.0 subpixel True
#         0.5
#         linear 0.5 alpha 1.0
#
#     transform ingm_btn(disp):
#         disp
#         yalign 0.5 xalign 0.5 alpha 0.0 zoom 1.0 subpixel True
#         0.5
#         linear 0.5 alpha 0.4
#
#     transform ingm_btn_hover(disp):
#         disp
#         yalign 0.5 xalign 0.5 alpha 0.0 zoom 1.0 subpixel True
#         0.5
#         linear 0.5 alpha 1.0

# ==============================================================================
# ui_labels.rpy
# ==============================================================================

# label splashscreen:
#
#
#     scene mmcache
#     show black
#     with None
#
#     $ renpy.pause(0.2)
#
#     $ renpy.movie_cutscene(vid_4ls)
#     $ renpy.pause(0.5)
#     $ from_splash = True
#
#     return
#
# label main_menu:
#
#
#     python:
#         if from_splash:
#             renpy.transition(Dissolve(1.0))
#             from_splash = False
#         menu_init()
#     jump main_menu_screen
#
#
#
# label _custom_game_menu(_game_menu_screen=_game_menu_screen):
#     if not _game_menu_screen:
#         return
#
#
#
#     call _enter_game_menu from _call__enter_game_menu_cgm
#
#     if renpy.has_label("game_menu"):
#         jump expression "game_menu"
#
#     jump expression _game_menu_screen

label hb_gm_bare:

    python:
        footerstring = ""
        if previous_language == None:
            previous_language = persistent.current_language
        gm_active = True
        config.skipping = None
        layout.navigation("gm_bare")
        if name_from_label(save_name):
            currentscenename = name_from_label(save_name)
            if not playthroughflag:
                currentscenename += " ("+ displayStrings.game_menu_replay_indicator +")"
            footerstring += displayStrings.play_time_label+": "+time_from_seconds(renpy.get_game_runtime())+"\n"+displayStrings.game_menu_current_scene+": "+currentscenename

        nowplaying = get_music_name()
        if nowplaying:
            footerstring += "\n"+displayStrings.game_menu_current_music+": "+nowplaying

        ui.text(footerstring, text_align=0.5, xalign=0.5, yalign=0.98, size=hb_rescale_y(18))
        ui.interact()

# label text_history:
#
#     $ renpy.transition(config.main_game_transition)
#     $ gm_active = True
#     $ entered_from_game = True

label hb_text_history_gm:
    python:

        layout.navigation(None)
        ui.image(style.gm_root.background)
        ui.image(hb_rescale_image("ui/bg-config.png"), xalign=0.5, yalign=0.5)

        yadj = ui.adjustment()

        if not current_line and len(readback_buffer) == 0:
            lines_to_show = []
        elif current_line and len(readback_buffer) == 0:
            lines_to_show = [current_line]
        elif current_line and current_line != readback_buffer[-1]:
            lines_to_show = readback_buffer + [current_line]
        else:
            lines_to_show = readback_buffer

        ui.vbox(xpos=hb_rescale_x(180), ypos=hb_rescale_y(120), background=None)
        ui.text(displayStrings.text_history_caption, style="page_caption")
        ui.hbox(xpos=0)
        vp = ui.viewport(yadjustment=yadj, offsets=(0.0,1.0), mousewheel=False, draggable=False, xmaximum=hb_rescale_x(415), ymaximum = hb_rescale_y(296), xalign=0.5, yalign=0.5)
        ui.vbox(xfill=True)
        for line in lines_to_show:
            if line[0] and line[0] != preparse_say_for_store(NARRATOR_NAME):
                ui.text(line[0], style="readback_label", **displayStrings.styleoverrides)
            ui.text(line[1], style="readback_text", **displayStrings.styleoverrides)
            ui.null(height=10)
        ui.close()
        ui.null(width=10)
        ui.vbox()
        ui.imagebutton(ib_base(hb_rescale_image("ui/bt-vscrollup.png")), hb_rescale_image("ui/bt-vscrollup.png"), clicked=decrease_rb_yoffset)
        ui.bar(adjustment=yadj, style='vscrollbar')
        ui.imagebutton(ib_base(hb_rescale_image("ui/bt-vscrolldown.png")), hb_rescale_image("ui/bt-vscrolldown.png"), clicked=increase_rb_yoffset)
        ui.close()
        ui.close()
        ui.close()
        ui.add(renpy.Keymap(t=ui.returns(True)))

        viewportkeys(decrease_rb_yoffset,increase_rb_yoffset)

        if not entered_from_game:
            return_func = _intra_jumps(_game_menu_screen, "intra_transition")
        else:
            entered_from_game = False
            return_func = ui.jumps("custom_return")

        ui.add(renpy.Keymap(t=return_func))
        return_button(return_func)

        ui.interact()

        renpy.jump("text_history_gm")

# label image_key:
#     $ renpy.transition(config.main_game_transition)
#     $ gm_active = True
#     $ entered_from_game = True
#
# label gm_image:
#
#     python:
#
#         layout.navigation(None)
#
#         renpy.transition(config.intra_transition)
#
#
#
#
#         if entered_from_game:
#             ui.add(renpy.Keymap(h=gm_image_return_to_game))
#             ui.add(renpy.Keymap(mouseup_2=gm_image_return_to_game))
#             ui.add(renpy.Keymap(joy_hide=gm_image_return_to_game))
#             ui.add(renpy.Keymap(game_menu=gm_image_return_to_game))
#             ui.add(renpy.Keymap(dismiss=gm_image_return_to_game))
#         else:
#             ui.add(renpy.Keymap(game_menu=ui.returns(True)))
#             ui.add(renpy.Keymap(dismiss=ui.returns(True)))
#         ui.interact()
#
#         renpy.transition(config.intra_transition)
#         renpy.jump(_game_menu_screen)
#
# label quit_from_os:
#
#     if is_glrenpy():
#         $ _enter_menu()
#     else:
#         call _enter_menu from quit_from_os_1
#     $ quit_from_os_flag = True

label hb_confirm_quit:
    python:
        if not ask_to_quit:
            renpy.jump("softquit")
        else:
            transition = config.intra_transition
            if not gm_active:
                transition = config.enter_transition
            # TODO
            gi_result = _yesno_prompt(None, displayStrings.yesno_quit, im.Image("ui/sd-hanako.png",xpos=515,ypos=305), transition=transition)
            if gi_result:
                renpy.jump("softquit")
            else:
                if quit_from_os_flag and gm_active:
                    quit_from_os_flag = False
                    renpy.jump("simple_return")
                elif gm_active:
                    quit_from_os_flag = False
                    renpy.jump(_game_menu_screen)
                else:
                    quit_from_os_flag = False
                    renpy.jump("_return")

label hb_confirm_mm:
    python:
        layout.navigation(None)
        if playthroughflag:
            # TODO
            gi_result = _yesno_prompt(None, displayStrings.yesno_return_to_main, im.Image("ui/sd-shizune.png",xpos=hb_rescale_x(195),ypos=hb_rescale_y(275)))
            if gi_result:
                renpy.music.stop()
                renpy.full_restart(transition=config.game_main_transition)
            else:
                renpy.jump(_game_menu_screen)
        else:
            renpy.music.stop()
            renpy.full_restart(transition=config.game_main_transition)

# label prefs_key:
#     $ renpy.transition(config.main_game_transition)
#     $ gm_active = True
#     $ entered_from_game = True

label hb_prefs_screen:

    python:
        animate_mm = False

        layout.navigation(None)
        if prefs_looped:
            renpy.transition(ImageDissolve(im.Tile(hb_rescale_image("ui/tr-checkwipe.png")), 0.5, 8))
        elif (mm_context() or entered_from_game) and not coming_from_prefs_sub:
            renpy.transition(config.main_game_transition)
        else:
            renpy.transition(config.intra_transition)
        if mm_context():
            ui.image(style.mm_static.background)
        coming_from_prefs_sub = False
        ui.image(style.gm_root.background)
        prefs_looped = False
        ui.image(hb_rescale_image("ui/bg-config.png"), xalign=0.5, yalign=0.5)

        group_spacing = 8

        ui.vbox(xpos = hb_rescale_x(180), ypos = hb_rescale_y(120))
        ui.text(displayStrings.config_page_caption, style="page_caption")
        ui.null(height=hb_rescale_y(8))
        ui.hbox()
        ui.null(width=hb_rescale_x(20))
        ui.vbox()
        if not persistent.hdisabled:
            checkboximage = "ui/bt-cf-unchecked.png"
        else:
            checkboximage = "ui/bt-cf-checked.png"
        widget_button(displayStrings.hdisabled_label, checkboximage, toggle_h, xsize=300, widgetyoffset=0)
        ui.null(height=group_spacing)

        fullscreen_p.render_preference(disabled=disallow_fullscreen)
        ui.null(height=group_spacing)
        unreadskip_p.render_preference()
        choiceskip_p.render_preference()
        ui.null(height=group_spacing)
        textspeed_p.render_preference()
        afm_p.render_preference()
        ui.null(height=group_spacing)
        musicvol_p.render_preference()
        ui.hbox()
        sfxvol_p.render_preference()
        ui.null(width=hb_rescale_x(20))
        widget_button(displayStrings.config_sfxtest_label, "ui/bt-musicplay.png", test_sound)

        ui.close()
        ui.null(height=group_spacing)
        widget_button(displayStrings.config_language_sel, "ui/bt-language.png", ui.jumps("language_screen"), xsize=300, widgetyoffset=3)
        if renpy.display.joystick.enabled:
            widget_button(displayStrings.config_gamepad_label, "ui/bt-gamepad.png", ui.jumps("joystick_screen"), xsize=300, widgetyoffset=3)


        ui.close()
        ui.close()
        ui.close()
        if entered_from_game:
            ui.add(renpy.Keymap(K_F4=gm_page_return_to_game))
        return_button(ui.returns("return"))

        if config.developer:
            ui.keymap(I=devlvl_I)
            ui.keymap(D=devlvl_D)
            ui.keymap(Q=devlvl_Q)
            ui.keymap(N=devlvl_N)


        gi_result = ui.interact()
        if gi_result == "return":
            if mm_context():
                renpy.jump("_return")
            elif entered_from_game:
                gm_page_return_to_game()
            else:
                renpy.transition(config.intra_transition)
                renpy.jump(_game_menu_screen)
        prefs_looped = True
        renpy.jump("prefs_screen")

label hb_language_screen:
    python:
        coming_from_prefs_sub = True

        renpy.transition(config.intra_transition)
        if mm_context():
            ui.image(style.mm_static.background)
        ui.image(style.gm_root.background)
        ui.image(hb_rescale_image("ui/bg-config.png"), xalign=0.5, yalign=0.5)
        layout.navigation(None)
        ui.vbox(xpos = hb_rescale_x(180), ypos = hb_rescale_y(120))
        ui.text(displayStrings.config_language_caption, style="page_caption")
        ui.null(height=hb_rescale_y(8))
        ui.hbox()
        ui.null(width=hb_rescale_x(10))
        ui.vbox()

        for language in available_languages:

            tl_progress = make_percentage(len(displayDict[language].s_scenes), len(displayDict[master_language].s_scenes))
            if tl_progress >= 100:
                tl_percentage = ""
            else:
                tl_percentage = ", " + str(tl_progress) + "%"

            if language == persistent.current_language:
                button_label = displayDict[language].activeLanguage
                button_state = "active"
                button_image = "ui/bt-language.png"
                button_function = None
            else:
                if language in displayStrings.allLanguages:
                    button_label = displayDict[language].activeLanguage+" ("+ displayStrings.allLanguages[language] + tl_percentage + ")"
                else:
                    if tl_percentage:
                        tl_percentage = " (" + tl_percentage + ")"
                    button_label = displayDict[language].activeLanguage + tl_percentage
                button_state = "button"
                button_image = "ui/bt-blank.png"
                button_function = renpy.curry(switch_language)(target=language)
            widget_button(text=button_label, displayable=button_image, clicked=button_function, state=button_state, xsize=500, widgetyoffset=3)

        ui.close()
        ui.close()
        ui.close()

        if not mm_context():
            ui.text(displayStrings.config_language_restart_note, text_align=0.5, xalign=0.5, yalign=0.98, size=hb_rescale_y(18))

        return_button(ui.jumps("prefs_screen"))

        ui.interact()

    jump language_screen

label hb_joystick_screen:
    python:
        coming_from_prefs_sub = True
        renpy.transition(config.intra_transition)
        if mm_context():
            ui.image(style.mm_static.background)
        ui.image(style.gm_root.background)
        ui.image(hb_rescale_image("ui/bg-config.png"), xalign=0.5, yalign=0.5)
        layout.navigation(None)
        ui.vbox(xpos = hb_rescale_x(180), ypos = hb_rescale_y(120))
        ui.text(displayStrings.gamepad_caption, style="page_caption")
        ui.null(height=hb_rescale_y(8))
        ui.hbox()
        ui.null(width=hb_rescale_x(10))
        ui.vbox()
        for label, key in config.joystick_keys:
            joy_button(label, key)
        ui.close()
        ui.close()
        ui.close()

        return_button(ui.jumps("prefs_screen"))

        ui.interact()

    jump joystick_screen

# label load_key:
#     $ renpy.transition(config.main_game_transition)
#     $ gm_active = True
#     $ entered_from_game = True

label hb_load_screen:
    $ animate_mm = False
    $ mode = "manual"

label hb_load_screen_loop:

    python:
        if mm_context():
            mybackground = LiveComposite(hb_rescale_xy(800, 600),
                                         (0, 0), style.mm_static.background,
                                         (0, 0), style.gm_root.background,
                                         hb_rescale_xy(150, 100), hb_rescale_image("ui/bg-config.png"))
        else:
            mybackground = LiveComposite(hb_rescale_xy(800, 600),
                                         (0, 0), style.gm_root.background,
                                         hb_rescale_xy(150, 100), hb_rescale_image("ui/bg-config.png"))

        if entered_from_game:
            ui.add(renpy.Keymap(K_F2=gm_page_return_to_game))
        fn, exists = custom_file_picker("load", False, mode, mybackground)

        if fn == "return":
            if mm_context():
                renpy.jump("_return")
            elif entered_from_game:
                gm_page_return_to_game()
            else:
                renpy.transition(config.intra_transition)
                renpy.jump(_game_menu_screen)
        elif fn == "_setmode":
            mode = exists
            renpy.jump("load_screen_loop")
        elif fn == "delete":
            background = None
            if mm_context():
                background = style.mm_static.background
            # TODO
            if _yesno_prompt(None, displayStrings.yesno_delete_savegame, im.Image("ui/sd-emi.png",xpos=510,ypos=275), background=background):
                renpy.unlink_save(exists)
        else:
            if not mm_context() and playthroughflag and statechangesincesave:
                # TODO
                if _yesno_prompt(None, displayStrings.yesno_load_in_game, im.Image("ui/sd-emi.png",xpos=510,ypos=275)):
                    renpy.load(fn)
            else:
                renpy.load(fn)

        renpy.jump("load_screen")

# label save_key:
#     $ renpy.transition(config.main_game_transition)
#     $ gm_active = True
#     $ entered_from_game = True

label hb_save_screen:

    python:
        mybackground = LiveComposite(hb_rescale_xy(800, 600),
                                     (0, 0), style.gm_root.background,
                                     hb_rescale_xy(150, 100), hb_rescale_image("ui/bg-config.png"))
        if entered_from_game:
            ui.add(renpy.Keymap(K_F3=gm_page_return_to_game))
        _fn, _exists = custom_file_picker("save", True, False, mybackground)

        if _fn == "return":
            if entered_from_game:
                gm_page_return_to_game()
            else:
                renpy.transition(config.intra_transition)
                renpy.jump(_game_menu_screen)
        elif _fn == "delete":
            # TODO
            if _yesno_prompt(None, displayStrings.yesno_delete_savegame, im.Image("ui/sd-emi.png",xpos=510,ypos=275)):

                renpy.unlink_save(_exists)
        else:
            # TODO
            if not _exists or _yesno_prompt(None, displayStrings.yesno_save_overwrite, im.Image("ui/sd-lilly.png",xpos=195,ypos=275)):
                minutes, seconds = divmod(int(renpy.get_game_runtime()), 60)
                if save_name:
                    full_save_name = save_name + "#" + str(renpy.get_game_runtime())
                else:
                    full_save_name = "#" + str(renpy.get_game_runtime())
                try:
                    renpy.save(_fn, full_save_name)
                except Exception, e:
                    if config.debug:
                        raise
                    message = ( _(u"The error message was:") + "\n\n" +
                                e.__class__.__name__  + ": " + unicode(e) + "\n\n" +
                                _(u"You may want to try saving in a different slot, or playing for a while and trying again later.") )
                    _show_exception(_(u"Save Failed."), message)
                else:
                    statechangesincesave = False
                    persistent.fpicker_yoffset = -1

                    # TODO
                    _prompt(None, displayStrings.yesno_savesuccess, im.Image("ui/sd-rin.png",xpos=510,ypos=165))
                renpy.jump(_game_menu_screen)
        renpy.jump("save_screen")

# label extra_from_mm:
#
#     $ renpy.transition(config.main_game_transition)
#     $ renpy.music.play(music_menus, fadeout=0.5, if_changed=True)

label hb_extra_menu:

    python:

        animate_mm = False


        gallery_predict()

        sceneselectstate = "disabled"
        if get_available_scenes():
            sceneselectstate = "button"
        gallerystate = "disabled"
        if get_available_images():
            gallerystate = "button"
        musicstate = "disabled"
        if get_available_music():
            musicstate = "button"
        videostate = "disabled"
        if ((len(persistent.seen_videos) >= 1) or has_devlvl()):
            videostate = "button"

        ui.image(LiveComposite(hb_rescale_xy(800, 600),
                               (0, 0), style.mm_static.background,
                               (0, 0), style.gm_root.background,
                               hb_rescale_xy(150, 100), hb_rescale_image("ui/bg-config.png")))

        ui.vbox(xpos=hb_rescale_x(180), ypos=hb_rescale_y(120), background=None)
        ui.text(displayStrings.extra_menu_caption, style="page_caption")
        ui.null(height=hb_rescale_y(10))
        ui.hbox(box_spacing=hb_rescale_x(15))
        extra_button(displayStrings.extra_music_button_label,"ui/sd-lilly.png",clicked=ui.jumps("music_menu", "intra_transition"), state=musicstate)
        extra_button(displayStrings.extra_gallery_button_label,"ui/sd-rin.png",clicked=ui.jumps("cg_gallery", "intra_transition"), state=gallerystate)
        extra_button(displayStrings.extra_scene_button_label,"ui/sd-shizune.png",clicked=ui.jumps("scene_select", "intra_transition"), state=sceneselectstate)
        extra_button(displayStrings.extra_opening_button_label,"ui/sd-emi.png",clicked=ui.jumps("video_menu", "intra_transition"), state=videostate)
        ui.close()
        ui.close()
        ui.hbox(xpos=hb_rescale_x(540), ypos=hb_rescale_y(346))
        extra_button(displayStrings.return_button_text,"ui/sd-hanako.png", clicked=ui.jumps("main_menu", "game_main_transition"), state="return")
        ui.close()


        if has_devlvl() and renpy.has_label("cruise_control"):
            layout.button("[[ flow tests ]", "mm", clicked=ui.jumpsoutofcontext("cruise_control"), xpos=hb_rescale_x(510), ypos=hb_rescale_y(115))

        ui.add(renpy.Keymap(game_menu=ui.jumps("main_menu", "game_main_transition")))
        ui.interact()
        renpy.transition(ImageDissolve(im.Tile(hb_rescale_image("ui/tr-checkwipe2.png")), 0.5, 8))
        renpy.jump("extra_menu")

# label act_op_ex(this_video, is_black=False):
#     if not is_black:
#         scene videowhite
#     else:
#         scene black
#     with config.game_main_transition
#     python:
#         renpy.movie_cutscene(this_video)
#         renpy.transition(config.main_game_transition)
#     return

label hb_video_menu:

    python:

        ui.image(LiveComposite(hb_rescale_xy(800, 600),
                               (0, 0), style.mm_static.background,
                               (0, 0), style.gm_root.background,
                               hb_rescale_xy(150, 100), hb_rescale_image("ui/bg-config.png")))

        ui.vbox(xpos=hb_rescale_x(180), ypos=hb_rescale_y(120), background=None)
        ui.text(displayStrings.video_page_caption, style="page_caption")
        ui.null(height=4)

        ui.grid(3,2, padding=hb_rescale_y(20), xpos=hb_rescale_x(35), ypos=hb_rescale_y(55))
        for this_video in displayStrings.videos:

            this_tn = this_video[1].replace(".mkv", "_tn.jpg")

            button_base = LiveComposite(hb_rescale_xy(110, 85),
                                        (0, 0), ib_base(hb_rescale_image("ui/bt-cg-locked.png")),
                                        hb_rescale_xy(5, 5), im.MatrixColor(hb_rescale_image(this_tn), im.matrix.desaturate()))
            button_hover = LiveComposite(hb_rescale_xy(110, 85),
                                         (0, 0), hb_rescale_image("ui/bt-cg-locked.png"),
                                         hb_rescale_xy(5, 5), this_tn)


            if this_video[1] in persistent.seen_videos or has_devlvl():

                ui.imagebutton(button_base,
                               button_hover,
                               clicked=ui.returns(this_video[1]))
            else:
                ui.image(ib_disabled(hb_rescale_image("ui/bt-cg-locked.png")))

        ui.close()
        ui.close()

        return_button(_intra_jumps("extra_menu", "intra_transition"))
        result = ui.interact()
        is_black = False

    if result == "video/op_1.mkv":
        $ is_black = True
    call act_op_ex (result, is_black)
    $ renpy.music.play(music_menus, fadein=5.0, fadeout=0.5)
    jump video_menu

label hb_music_menu:

    python:

        nowplaying = get_music_name()
        available_music = get_available_music()

label hb_music_menu_loop:

    python:
        auto_offset = 34
        if not persistent.mpicker_yoffset:
            persistent.mpicker_yoffset = 0

        yadj = ui.adjustment(range=auto_offset * (len(ex_m_tracks) - 8), value=persistent.mpicker_yoffset, changed=store_m_yoffset)

        viewportkeys(decrease_m_yoffset,increase_m_yoffset)

        ui.image(LiveComposite(hb_rescale_xy(800, 600),
                               (0, 0), style.mm_static.background,
                               (0, 0), style.gm_root.background,
                               hb_rescale_xy(150, 100), hb_rescale_image("ui/bg-config.png")))

        ui.vbox(xpos=hb_rescale_x(180), ypos=hb_rescale_y(120), background=None)
        ui.text(displayStrings.music_page_caption, style="page_caption")
        if nowplaying:
            ui.text(displayStrings.music_now_playing + ": " + nowplaying, style="prefs_label")
        else:
            ui.text(" ", style="prefs_label")
        ui.null(height=hb_rescale_y(4))
        ui.hbox(xpos=hb_rescale_x(4))
        vp = ui.viewport(yadjustment=yadj, mousewheel=False, draggable=False, xmaximum=hb_rescale_x(415), ymaximum = hb_rescale_y(267), xalign=0.5, yalign=0.5)
        ui.vbox(xfill=True)
        for name, file in ex_m_tracks:
            if file in available_music:
                if file == "" or not renpy.loadable(file):
                    layout.button(name, "mm", clicked=None)
                else:
                    layout.button(name, "mm", clicked=ui.returns((name, file)))
                ui.null(height=hb_rescale_y(1))
            else:
                ui.null(height=hb_rescale_y(1))
                ui.image(ib_disabled(hb_rescale_image("ui/bg-lockedtrack.png"), xpos=hb_rescale_x(14)))

            ui.null(height=hb_rescale_y(3+displayStrings.selector_padding))
        ui.close()
        ui.vbox()
        ui.imagebutton(ib_base(hb_rescale_image("ui/bt-vscrollup.png")), hb_rescale_image("ui/bt-vscrollup.png"), clicked=decrease_m_yoffset)
        ui.bar(style='vscrollbar2', adjustment = yadj, changed=store_m_yoffset)
        ui.imagebutton(ib_base(hb_rescale_image("ui/bt-vscrolldown.png")), hb_rescale_image("ui/bt-vscrolldown.png"), clicked=increase_m_yoffset)
        ui.close()
        ui.close()
        ui.close()

        ui.hbox(xpos=hb_rescale_x(180), ypos=hb_rescale_y(449))
        musicvol_p_jukebox.render_preference()
        ui.null(width=hb_rescale_x(20))
        widget_button(displayStrings.music_stop_button_text, "ui/bt-musicstop.png", clicked=ui.returns(("", "")), xsize=80)
        ui.close()

        return_button(_intra_jumps("extra_menu", "intra_transition"))

        result = ui.interact()
        renpy.music.play(result[1], fadeout=0.5, if_changed=True)
        nowplaying = result[0]
        renpy.jump("music_menu_loop")

label hb_scene_select:


    python:
        menu_init()
        ss_desc = ""
        if filter != "Act 1":
            persistent.spicker_yoffset = 0
        filter = "Act 1"

label hb_scene_select_loop:

    python:
        ui.image(LiveComposite(hb_rescale_xy(800, 600),
                               (0, 0), style.mm_static.background,
                               (0, 0), style.gm_root.background,
                               hb_rescale_xy(150, 100), hb_rescale_image("ui/bg-config.png")))

        available_scenes = get_available_scenes(filter, include_locked=True, include_acts=True) or ()

        scrollable = False
        if len(available_scenes) > 8:
            scrollable = True

        if scrollable:
            auto_offset = 1.0 / (len(available_scenes) - 8)

            auto_offset = 34
        else:
            auto_offset = 0

        if not persistent.spicker_yoffset:
            persistent.spicker_yoffset = 0.0

        ui.vbox(xpos=hb_rescale_x(180), ypos=hb_rescale_y(120), background=None)
        ui.text(displayStrings.scene_page_caption, style="page_caption")
        ui.hbox()
        shown_buttons = ("Act 1", "Emi", "Hanako", "Lilly", "Rin", "Shizune")

        for button in shown_buttons:
            if button == "Act 1":
                label = displayStrings.act_term + " 1"
            elif button == "Emi":
                label = displayStrings.name_emi
            elif button == "Hanako":
                label = displayStrings.name_ha
            elif button == "Lilly":
                label = displayStrings.name_li
            elif button == "Rin":
                label = displayStrings.name_rin
            else:
                label = displayStrings.name_shi

            path = button

            if button == filter:
                layout.button(label, "rpa_active", clicked=None)
            elif get_available_scenes(path):
                layout.button(label, "rpa", clicked=ui.returns(("_setfilter",path)))
            else:
                layout.button(label, "rpa", clicked=None)
        ui.close()
        ui.hbox()


        open_acts = []
        for (name, label, display, path, unlocked) in available_scenes:
            if unlocked and path not in open_acts:
                open_acts.append(path)

        yadj = ui.adjustment(range=auto_offset * (len(available_scenes) - 8), value=persistent.spicker_yoffset, changed=store_s_yoffset)

        scene_indent = 15

        vp = ui.viewport(yadjustment=yadj, mousewheel=False, draggable=False, xmaximum=hb_rescale_x(400), ymaximum = hb_rescale_y(270), xalign=0.5, yalign=0.5)
        ui.vbox(xfill=True)
        for (name, label, display, path, unlocked) in available_scenes:
            if label == rp_actmark:
                if path in open_acts:
                    ss_button("{color=00000066}" + name + "{/color}","", clicked=None)
                    ui.null(height=hb_rescale_y(1))
            elif unlocked:
                if False:
                    extension = " " + label
                else:
                    extension = ""
                ui.hbox()
                ui.null(width=hb_rescale_x(scene_indent))
                ss_button(name+extension, display, clicked=ui.returns((name,label)))
                ui.close()
                ui.null(height=hb_rescale_y(1))
            else:
                ui.null(height=hb_rescale_y(1))
                ui.hbox()
                ui.null(width=hb_rescale_x(scene_indent+12))
                ui.image(ib_disabled(hb_rescale_image("ui/bg-lockedtrack.png")))
                ui.close()

            ui.null(height=hb_rescale_y(3+displayStrings.selector_padding))

        viewportkeys(decrease_s_yoffset,increase_s_yoffset)

        ui.close()
        ui.null(width=hb_rescale_x(20))
        ui.vbox(box_spacing=hb_rescale_y(2))
        if scrollable:
            ui.imagebutton(ib_base(hb_rescale_image("ui/bt-vscrollup.png")), hb_rescale_image("ui/bt-vscrollup.png"), clicked=decrease_s_yoffset)
            ui.bar(style='vscrollbar2', adjustment = yadj, changed=store_s_yoffset)
            ui.imagebutton(ib_base(hb_rescale_image("ui/bt-vscrolldown.png")), hb_rescale_image("ui/bt-vscrolldown.png"), clicked=increase_s_yoffset)
        else:
            ui.imagebutton(ib_disabled(hb_rescale_image("ui/bt-vscrollup.png")), hb_rescale_image("ui/bt-vscrollup.png"), clicked=None)
            ui.bar(style='vscrollbar2_disabled')
            ui.imagebutton(ib_disabled(hb_rescale_image("ui/bt-vscrolldown.png")), hb_rescale_image("ui/bt-vscrolldown.png"), clicked=None)
        ui.close()
        ui.close()
        ui.close()

        return_button(ui.jumps("extra_menu", "intra_transition"))

        if has_devlvl():
            if playthroughflag:
                checkboximage = "ui/bt-cf-unchecked.png"
            else:
                checkboximage = "ui/bt-cf-checked.png"
            widget_button(displayStrings.scene_playthrough_label, checkboximage, toggle_playthrough, xsize=340, widgetyoffset=0, xpos=180, ypos=450)
        else:
            completion_percentage = get_completion_percentage()
            ui.text(displayStrings.scene_completion_label % completion_percentage, style="prefs_label", xpos=hb_rescale_x(180), ypos=hb_rescale_y(450))

        ui.add(DynamicDisplayable(refresh_label))


        what, where = ui.interact()


        readback_buffer = []
        if what not in ("_setfilter", "_pt_toggled"):




            init_vars()
            last_scene_label = where
            renpy.music.stop(fadeout=0.5)
            renpy.transition(config.game_main_transition)
            renpy.scene()
            renpy.show("black")
            renpy.block_rollback()
            ui.timer(1.0, ui.returns, kwargs={"value":True})
            ui.interact()
            save_name = where
            if playthroughflag or not renpy.has_label("replay_"+where):
                jumptarget = where
            else:
                jumptarget = "replay_"+where
            ui.jumpsoutofcontext(jumptarget)()
        elif what == "_setfilter":
            filter = where
            persistent.spicker_yoffset = 0
        elif what == "_pt_toggled":
            renpy.transition(ImageDissolve(im.Tile(hb_rescale_image("ui/tr-checkwipe2.png")), 0.5, 8))

        renpy.jump("scene_select_loop")

label hb_cg_gallery:

    python:
        if get_completion_percentage() >= 100 and persistent.bad and persistent.emi and persistent.emibad and persistent.lilly and persistent.lillybad and persistent.hanako and persistent.hanakosad and persistent.hanakorage and persistent.rin and persistent.rintrue and persistent.rinbad and persistent.shizune and persistent.shizunebad:
            ksgallery_unlock('completionbonus')

        mygallery = Gallery()
        mygallery.locked_background = hb_rescale_image("ui/bg-ex-gallery-lockedimage.png")
        mygallery.background = (LiveComposite(hb_rescale_xy(800, 600),
                                              (0, 0), style.mm_static.background,
                                              (0, 0), style.gm_root.background,
                                              hb_rescale_xy(150, 100), hb_rescale_image("ui/bg-config.png")))
        mygallery.locked_button = ib_disabled("ui/bt-cg-locked.png")
        mygallery.grid_layout((4,3),hb_rescale_xy(175,160),hb_rescale_xy(115,90))
        i = 0
        page = 0
        for i_image in ex_g_images:
            if i == 0:
                page += 1

                mygallery.page(str(page))
                i = 0
            mygallery.autobutton(i_image)
            i += 1
            if i >= 12:
                i = 0
        if page == 0:
            mygallery.page(displayStrings.gallery_num_page_error)
        mygallery.show()

# label simple_return:
#     return
#
# label custom_noisy_return:
#     $ renpy.play(config.exit_sound)
#
#
# label custom_return:
#     $ gm_active = False
#     $ statechangesincesave = True
#     if mm_context():
#         $ renpy.transition(config.game_main_transition)
#         jump _main_menu_screen
#     $ renpy.transition(config.exit_transition)
#     if gm_exit_to and previous_language != persistent.current_language:
#         nvl clear
#         stop music fadeout 1.0
#         $ temp_exit = gm_exit_to
#         $ gm_exit_to = None
#         $ previous_language = None
#         $ renpy.jump_out_of_context(temp_exit)
#     return
#
# label custom_return_dissolve:
#     $ gm_active = False
#     $ statechangesincesave = True
#     $ renpy.transition(config.intra_transition)
#     return
#
# label afm_on:
#
#     python:
#         turn_afm_on()
#         renpy.jump("_return")
#
# label start_from_mm:
#
#
#     stop music fadeout 1.0
#
#     scene black
#     with config.game_main_transition
#
#     python:
#         playthroughflag = True
#         init_vars()
#         renpy.pause(2)
#         renpy.jump_out_of_context("start")
#
# label softquit:
#
#     python:
#         if _preferences.transitions != 0 :
#             renpy.music.stop(fadeout=0.5)
#             renpy.transition(dissolve)
#             ui.image(Solid("#000"))
#             renpy.pause(0.7)
#         renpy.jump("_quit")

label hb_scene_deleted(is_end=False):
    window hide

    $ oldambient = renpy.music.get_playing(channel="ambient")
    $ renpy.music.set_volume(0.05, 2.0, channel="music")

    play ambient sfx_heartfast fadein 0.5

    scene pink
    with dissolve


    $ prawn_image = "ui/" + renpy.random.choice(("prawns.jpg", "climatic.jpg", "cantaloupes.jpg", "cuddlefish.jpg"))

    scene expression hb_rescale_image(prawn_image):
        zoom 1.0 xalign 0.5 yalign 0.5 subpixel True
        linear 0.05 zoom 1.05
        easein 0.7 zoom 1.0
        pause 0.118
        linear 0.02 zoom 1.025
        easein 0.3 zoom 1.0
        repeat
    show expression hb_rescale_image("ui/bt-logoonly.png"):
        xanchor 0 yanchor 0 xpos hb_rescale_x(610) ypos hb_rescale_y(10)
    with Dissolve(2.0)

    $ renpy.pause(2.0)

    scene white
    with Dissolve(6.0)

    stop music fadeout 1.0
    stop ambient fadeout 1.0
    $ renpy.pause(1.0)


    play ambient oldambient fadein 2.0

    $ renpy.transition(dissolve)
    if not is_end:
        window show
    else:
        stop music fadeout 1.0
        $ suppress_window_before_timeskip = True
        scene black
        with dissolve

    $ renpy.music.set_volume(1.0, 2.0, channel="music")

    return

# label en_timeskip:
#
#     if playthroughflag:
#         stop sound fadeout 2.0
#         stop music fadeout 2.0
#         stop ambient fadeout 2.0
#         if suppress_window_before_timeskip:
#             window hide None
#         else:
#             window hide
#         with Pause(2.0)
#
#         play music music_timeskip
#
#         show kslogo heart at Position(xalign=0.5, yalign=0.5)
#         with clockwipe
#
#         scene black
#         show kslogo words at Position(xalign=0.5, yalign=0.5)
#         with clockwipe
#
#         with Pause(2.0)
#
#         stop music fadeout 2.0
#
#         scene black
#         with clockwipe
#
#         with Pause(2.0)
#
#         if not suppress_window_after_timeskip:
#             window show
#
#     $ suppress_window_before_timeskip = False
#     $ suppress_window_after_timeskip = False
#
#     return


init 505 python:

#     show_skipcredits_button = False

    def skipcredits_overlay():
        if not show_skipcredits_button:
            return

        def clicked():
            renpy.jump("after_credits")

        ui.keymap(K_ESCAPE=clicked)
        ui.textbutton("{color=#000}Skip{/color}",clicked=clicked, xpos=hb_rescale_x(795), xanchor=1.0, ypos=hb_rescale_y(595), yanchor=1.0, xpadding=hb_rescale_x(6),
                      background=RoundRect((48, 48, 48, 255), 6),
                      hover_background=RoundRect((64, 64, 64, 255), 6),
                      color="#000",
                      )

    # TODO: This is just adding a second button
    config.overlay_functions.append(skipcredits_overlay)

label hb_credits(c_movie=False):
    window hide
    stop music
    stop ambient
    stop movie

    python:
        wdt_off()

        config.skipping = False

        old_game_menu_screen = _game_menu_screen
        _game_menu_screen = None

        this_rollback = config.rollback_enabled
        config.rollback_enabled = False

        config.allow_skipping = False

        ask_to_quit = False
        _preferences.afm_time = 0
        may_afm = False

        show_skipcredits_button = True

    scene videoblack
    show credits mask
    with Dissolve(2.0)

    $ renpy.pause(1.0, hard=True)

    python:

        creditstext_disp = Text(displayStrings.creditstring, color="#ffffff", text_align=0.5, min_width=hb_rescale_x(800))

        logo_top = LiveComposite(hb_rescale_xy(800, 600),
                                 hb_rescale_xy(313,233), hb_rescale_image("ui/cred_logo.png"))

        logo_bottom = LiveComposite(hb_rescale_xy(800, 600),
                                    hb_rescale_xy(311, 238), hb_rescale_image("ui/4lsl-small.png"))

        if c_movie:
            creditstext_offs = HBox(Null(width=hb_rescale_x(320)), creditstext_disp)
        else:
            creditstext_offs = creditstext_disp

        credits_final = VBox(logo_top, creditstext_offs, logo_bottom)

    show expression credits_final as roll behind credits at Position(xalign=0.5, yalign=0.0)
    with Dissolve(2.0)

    $ renpy.pause(2.0, hard=True)

    if c_movie:
        $ renpy.music.play(music_credits, loop=False)

    show expression credits_final as roll behind credits:
        xalign 0.5 yalign 0.0 subpixel True
        acdc20_warp 60.0 yalign 1.0
    with None

    $ renpy.pause(8.0, hard=True)

    if c_movie:
        play movie c_movie
        show expression Movie(fps=30, size=hb_rescale_xy(400, 300), yalign=0.5, xanchor=0.5, xpos=0.3) as themovie behind roll
    with None

    $ renpy.pause(52.0, hard=True)

    stop movie
    hide themovie
    with None

    show expression Text(u"©MMXV Four Leaf Studios", text_align=0.5, size=hb_rescale_y(15)) as copyright at Position(xalign=0.5, yalign=0.576)
    with Dissolve(2.0)
    $ renpy.pause(5.0, hard=True)

    $ renpy.music.set_volume(0.0,1.0)

    scene black
    with Dissolve(2.0)

    $ renpy.pause(1.0, hard=True)

label hb_after_credits:
    $ config.allow_skipping = True
    $ config.rollback_enabled = this_rollback
    $ renpy.block_rollback()
    $ _game_menu_screen = old_game_menu_screen

    stop music
    stop movie
    hide themovie
    with None

    $ renpy.music.set_volume(1.0,0.0)
    $ may_afm = True
    $ show_skipcredits_button = False

    scene black
    with None

    return

# label before_save:
#     python:
#
#
#         pass
#     return
#
#
# label after_load:
#     python:
#         initialize_prefs()
#         ask_to_quit = True
#         savegame_langage = np_current_language
#         if persistent.current_language != savegame_langage:
#             switch_language(persistent.current_language)
#             renpy.pop_return()
#             nvl_clear()
#             renpy.jump(wrap_label(last_visited_label))
#
#
#
#         persistent.breadcrumbs = copy.deepcopy(store.breadcrumbs)
#
#     return
#
#
#
# label start:
#
#
#     stop music
#
#     $ ask_to_quit = True
#
#     $ renpy.scene()
#     $ np_current_language = persistent.current_language
#     $ renpy.block_rollback()
#     $ readback_buffer = []
#     nvl clear
#
#     jump imachine
#
# label en_op_vid1:
#     $ wdt_off()
#     $ renpy.movie_cutscene("video/"+vid_op)
#     $ renpy.pause(1.0)
#     $ wdt_on()
#     return
#
# label act_op(this_video):
#     $ wdt_off()
#     scene videowhite
#     with Dissolve(2.0)
#     python:
#
#         _localized_tc = False
#         renpy.pause(1.0)
#         renpy.movie_cutscene("video/" + this_video)
#         renpy.pause(0.1)
#
#         act = this_video[6:7]
#         char = this_video[8:-4]
#
#         for count, (name, id, desc, sorts) in enumerate(s_scenes):
#             if id == rp_actmark:
#                 if (sorts[0] == "Act " + act and sorts[1].lower() == char) or (act == "1" and sorts == "Act 1"):
#                     if name != displayDict[master_language].s_scenes[count][0]:
#                         _localized_tc = name
#                         break
#
#     call localized_tc (_localized_tc, True)
#     $ wdt_on()
#
#     return

label hb_localized_tc(tc_text, auto=False):

    if not tc_text or not playthroughflag or not auto:
        return

    show videowhite
    show expression Text("{b}{color=#908778}{size="+int(hb_rescale_y(36))+"}"+tc_text+"{/size}{/color}{/b}", outlines=[]) as titlecard:
        subpixel True xalign 0.45 yalign 0.5
        linear 6.5 xalign 0.55
    with dissolve

    with Pause(4.0)

    hide titlecard
    with Dissolve(2.0)

    with Pause(0.5)

    return

# label path_end(character="", is_good=False):
#
#     if not playthroughflag:
#         return
#
#     window hide
#
#     $ end_id = character
#     if not is_good or not character:
#         $ end_id += "bad"
#
#     if character.endswith("true") or character.endswith("sad") or character.endswith("rage"):
#         $ is_good = False
#
#     $ setattr(persistent, end_id, getattr(persistent, end_id)+1)
#     $ setattr(multipersistent, end_id, True)
#     $ multipersistent.save()
#
#     if not automode:
#         if end_id == "bad":
#             scene bloodred
#             with Dissolve(4.0)
#             call credits
#         elif is_good:
#             if renpy.loadable("video/credits_"+character+".mkv"):
#                 $ myvid = "video/credits_"+character+".mkv"
#             else:
#                 $ myvid = False
#             scene white
#             with Dissolve(4.0)
#             call credits (myvid)
#         else:
#             scene black
#             with Dissolve(2.0)
#             call credits
#     return
#
#
#
#
# label restart:
#     if automode:
#         $ init_vars()
#         jump imachine
#     elif notextmode:
#         jump cruise_control
#     else:
#         $ renpy.full_restart()

# ==============================================================================
# ui_settings.rpy
# ==============================================================================

# init -1 python:
#
#
#     Position = Transform
#
#
#     layout.provides("load_save")
#     layout.provides("yesno_prompt")
#     layout.provides("preferences")
#     layout.button_menu()
#     theme.roundrect()
#
#     config.locked = False
#     config.time_format = "%b %d, %H:%M"
#     config.file_entry_format = ""
#     config.preferences = { }
#     config.all_preferences = { }
#     config.minimumvolume = -10.0
#
#     config.script_version = (6,10,2)
#
#
#
#
#
#
#
#
#
#
#     config.screenshot_callback = None
#     config.locked = True
#
# init -101 python:
#     config.developer = False
#
init 501:
#
#
#
#     python:
#         if config.developer:
#             config.searchpath.append('archived')
#         config.window_title = u""
#         multipersistent = MultiPersistent("actual.katawa-shoujo.com")
#         release_is_demo = False
#
#         config.has_autosave = False
#         config.nvl_show_display_say = say_wrapper
#
#
#
#     transform center_pro:
#         xalign 0.5
#         yalign 1.0
#         rotate None
#         zoom 1.0
#         alpha 1.0
#         around (0.0,0.0)
#         alignaround (0.0,0.0)
#         crop None
#         corner1 None
#         corner2 None
#         size None
#
#     transform twoleft:
#         xpos 0.3 xanchor 0.5 ypos 1.0 yanchor 1.0
#
#     transform tworight:
#         xpos 0.7 xanchor 0.5 ypos 1.0 yanchor 1.0
#
#     transform closeleft:
#         xpos 0.25 xanchor 0.5 ypos 1.0 yanchor 1.0
#
#     transform closeright:
#         xpos 0.75 xanchor 0.5 ypos 1.0 yanchor 1.0
#
#     transform twoleftoff:
#         xpos 0.32 xanchor 0.5 ypos 1.0 yanchor 1.0
#
#     transform tworightoff:
#         xpos 0.68 xanchor 0.5 ypos 1.0 yanchor 1.0
#
#     transform centeroff:
#         xpos 0.52 xanchor 0.5 ypos 1.0 yanchor 1.0
#
#     transform bgleft:
#         xpos 0.4 xanchor 0.5 ypos 1.0 yanchor 1.0
#
#     transform bgright:
#         xpos 0.6 xanchor 0.5 ypos 1.0 yanchor 1.0
#
#     transform Alphaout(var_time):
#         alpha 1.0
#         linear var_time alpha 0.0
#
#     transform Alphain(var_time):
#         alpha 0.0
#         linear var_time alpha 1.0
#
#     transform Slide(start_pos, start_anchor, end_pos, end_anchor, my_time, time_warp=_ease_in_time_warp):
#         xpos start_pos xanchor start_anchor yalign 1.0
#         warp time_warp my_time xpos end_pos xanchor end_anchor
#
#     transform titlecard_screen(mid_image, mid_imagetrans, final_disp):
#         Solid("#FFF")
#         4.0
#         "ui/tc-neutral.png" with Dissolve(1.0)
#         1.0
#         mid_image with ImageDissolve(mid_imagetrans, 5.0)
#         6.5
#         final_disp with wiperight
#         6.0
#         Solid("#FFF") with Dissolve(3.0)
#
#     transform note_enter:
#         xalign 0.5 yanchor 0.5 ypos 0.8 alpha 0.0
#         0.2
#         easein 0.5 ypos 0.5 alpha 1.0
#
#     transform note_exit:
#         xalign 0.5 yalign 0.5 alpha 1.0
#         easeout 0.5 ypos 0.8 alpha 0.0
#
#
#     transform move_ltr(my_time, my_warper, is_subpixel):
#         xalign 0.0 yalign 0.5 subpixel is_subpixel
#         warp my_warper my_time xalign 1.0
#
#     transform move_rtl(my_time, my_warper, is_subpixel):
#         xalign 1.0 yalign 0.5 subpixel is_subpixel
#         warp my_warper my_time xalign 0.0
#
#     transform move_ttb(my_time, my_warper, is_subpixel):
#         xalign 0.5 yalign 0.0 subpixel is_subpixel
#         warp my_warper my_time yalign 1.0
#
#     transform move_btt(my_time, my_warper, is_subpixel):
#         xalign 0.5 yalign 1.0 subpixel is_subpixel
#         warp my_warper my_time yalign 0.0
#
    python:
#         def Fullpan(time, dir="right", time_warp=acdc_warp, subpixel=True):
#             if dir == "right":
#                 return move_ltr(time, time_warp, subpixel)
#             elif dir == "down":
#                 return move_ttb(time, time_warp, subpixel)
#             elif dir == "up":
#                 return move_btt(time, time_warp, subpixel)
#             else:
#                 return move_rtl(time, time_warp, subpixel)
#
#
#
#
#         use_new_sprites = False
#
#         def make_sprite_path(char, expr, suffix='', close=False):
#
#             if suffix:
#                 suffix = '_' + suffix
#             close_dir = '/'
#             close_mod = ''
#             if close:
#                 close_dir = '/close/'
#                 close_mod = '_close'
#
#             return 'sprites/' + char + close_dir + char + '_' + expr + suffix + close_mod + '.png'

        def ks_sprite(char, expr_name, suffix_list=[]):

            missingsprite = 'sprites/others/generic_missing.png'

            regular_files = [(make_sprite_path(char,expr_name),expr_name),
                             (make_sprite_path(char,expr_name,close=True),expr_name+'_close')]

            for suffix in suffix_list:
                regular_files.append((make_sprite_path(char,expr_name, suffix),expr_name+'_'+suffix))
                regular_files.append((make_sprite_path(char,expr_name,suffix,True),expr_name+'_'+suffix+'_close'))

            for filename, mod_expr_name in regular_files:
                if not renpy.loadable(filename):
                    continue
                else:
                    checked_filename = filename
                renpy.image((char,mod_expr_name), hb_rescale_image(checked_filename))
                for filter, filtersuffix in sp_filters:
                    renpy.image((char,mod_expr_name+"_"+filtersuffix), filter(hb_rescale_image(checked_filename)))

#         def make_sprites(char, expr_list, suffix_list=[]):
#
#             if use_new_sprites:
#                 return
#
#             alpha_done = {'close':False,'regular':False}
#
#             for expr in expr_list:
#                 ks_sprite(char, expr, suffix_list)
#
#
#                 for distance in ('close', 'regular'):
#                     if not alpha_done[distance]:
#                         is_close = False
#                         target_expr = 'invis'
#                         loadname = ''
#                         if distance == 'close':
#                             is_close = True
#                             target_expr = 'invis_close'
#                         basename = make_sprite_path(char, expr, close=is_close)
#                         if renpy.loadable(basename):
#                             loadname = basename
#                         if not loadname and len(suffix_list):
#                             for suffix in suffix_list:
#                                 this_base = make_sprite_path(char, expr, suffix, is_close)
#                                 if renpy.loadable(this_base):
#                                     loadname = this_base
#                                     break
#                         if loadname:
#                             renpy.image((char,target_expr), im.Alpha(loadname, 0.0))
#                             alpha_done[distance] = True
#
#
#
#         def load_sjpg(file):
#             width, height = disp_size(file)
#             return im.AlphaMask(im.Crop(file,0,0,width/2,height),im.Crop(file,width/2,0,width/2,height))
#
#         def sjpg_set(basechar, expressions, defoffset=None, defoffset_close=None, suffix_list=None, alias=None):
#
#             if not use_new_sprites:
#                 return
#
#             basefolder = 'jsprites/'
#             invis_set = {'close':False,'regular':False}
#             char = basechar
#
#             if not isinstance(expressions, dict):
#                 expressions = {'':(expressions,defoffset,defoffset_close)}
#
#             if not suffix_list:
#                 suffix_list = ['']
#             else:
#                 suffix_list.append('')
#             if alias:
#                 char, suffix_list = alias
#
#             for pose, metadata in expressions.iteritems():
#                 baseoffset_close = None
#                 baseoffset = None
#                 if isinstance(metadata, list):
#                     expr_list = metadata
#
#                 elif len(metadata) == 3:
#                     expr_list, baseoffset, baseoffset_close = metadata
#                 else:
#                     expr_list, baseoffset = metadata
#                 distance_list = ['regular']
#                 if baseoffset_close:
#                     distance_list.append('close')
#
#                 pose_disp = ''
#                 if pose:
#                     pose_disp = pose + '_'
#                     pose = '_' + pose
#                 for distance in distance_list:
#                     offset = baseoffset
#                     folder = basefolder + basechar + '/'
#                     name_mod = ''
#                     if distance == 'close':
#                         folder += 'close/'
#                         name_mod = '_' + distance
#                         offset = baseoffset_close
#                     for suffix in suffix_list:
#                         if len(suffix):
#                             suffix = '_' + suffix
#                         suffix_path = suffix
#                         if alias:
#                             suffix_path = ''
#                         basefile = folder + basechar + pose + suffix_path + name_mod + '_BASE.jpg'
#                         alphafile = folder + basechar + pose + suffix_path + name_mod + '_ALPHA.png'
#                         invisfile = ''
#                         can_dynamic = False
#                         if renpy.loadable(basefile) and renpy.loadable(alphafile):
#                             can_dynamic = True
#                         else:
#                             invisfile = basefile
#
#                         expr_checked = []
#                         for expr in expr_list:
#                             expr_static = folder + basechar + pose + '_' + expr + suffix_path + name_mod + '.png'
#                             expr_jstatic = folder + basechar + pose + '_' + expr + suffix_path + name_mod + '.a.jpg'
#                             if renpy.loadable(expr_jstatic):
#                                 expr_checked.append((expr,"jpg"))
#                                 if not invisfile:
#                                     invisfile = load_sjpg(expr_jstatic)
#                             elif renpy.loadable(expr_static):
#                                 expr_checked.append((expr,"png"))
#                                 if not invisfile:
#                                     invisfile = expr_static
#                             elif can_dynamic:
#                                 expr_checked.append((expr,"comp"))
#                         if not len(expr_checked):
#                             continue
#
#                         if not invis_set[distance]:
#                             sjpg_image((char,'invis' + name_mod), im.Alpha(invisfile, 0.0))
#                             invis_set[distance] = True
#
#                         for expr, is_static in expr_checked:
#                             if is_static == "comp":
#                                 expr_comp = folder + basechar + pose + '_' + expr + name_mod + '_COMP.jpg'
#                                 if not renpy.loadable(expr_comp):
#                                     continue
#                                 curr_comp = im.Composite(None,(0,0),basefile,offset, expr_comp)
#                                 curr_disp = im.AlphaMask(curr_comp,alphafile)
#                             elif is_static == "png":
#                                 curr_disp = folder + basechar + pose + '_' + expr + suffix_path + name_mod + '.png'
#                             else:
#                                 curr_disp = load_sjpg(folder + basechar + pose + '_' + expr + suffix_path + name_mod + '.a.jpg')
#                             sjpg_image((char,pose_disp+expr+suffix+name_mod), curr_disp)
#                             for filter, filtersuffix in sp_filters:
#                                 sjpg_image((char,pose_disp+expr+suffix+name_mod+'_'+filtersuffix), filter(curr_disp))
#
#         def sjpg_image(alias, what):
#
#             renpy.image(alias, what)
#
#
#
#
        def ks_bg(bgid):
            path = "bgs/"
            tag = "bg"
            base_image = path + bgid + ".jpg"
            if not renpy.loadable(base_image):
                base_image = "bgs/_bg_missing.png"
            renpy.image((tag,bgid), hb_rescale_image(base_image))
            for filter, filtersuffix in bg_filters:
                prefiltered = path + bgid + "_" + filtersuffix + ".jpg"
                if renpy.loadable(prefiltered):
                    renpy.image((tag,bgid+"_"+filtersuffix), hb_rescale_image(prefiltered))
                else:
                    renpy.image((tag,bgid+"_"+filtersuffix), filter(hb_rescale_image(base_image)))

        ks_bg("city_alley")
        ks_bg("city_graveyard")
        ks_bg("city_karaokeext")
        ks_bg("city_karaokeint")
        ks_bg("city_othello")
        ks_bg("city_street1")
        ks_bg("city_street1_blurred")
        ks_bg("city_street2")
        ks_bg("city_street3")
        ks_bg("city_street4")
        ks_bg("city_subway")
        ks_bg("city_station")
        ks_bg("city_restaurant")
        ks_bg("city_clubint")
        ks_bg("city_clubpool")
        ks_bg("emi_dining")
        ks_bg("emi_houseext")
        ks_bg("emi_kitchen")
        ks_bg("gallery_atelier")
        ks_bg("gallery_ext")
        ks_bg("gallery_int")
        ks_bg("gallery_staircase")
        ks_bg("gallery_exhibition")
        ks_bg("hok_houseext")
        ks_bg("hok_kitchen")
        ks_bg("hok_lounge")
        ks_bg("hok_road")
        ks_bg("hok_wheat")
        ks_bg("hok_bath")
        ks_bg("lilly_hilltop")
        ks_bg("hosp_ceiling")
        ks_bg("hosp_ext")
        ks_bg("hosp_hallway")
        ks_bg("hosp_room")
        ks_bg("hosp_room2")
        ks_bg("misc_sky")
        ks_bg("misc_ceiling")
        ks_bg("misc_ceiling_blur")
        ks_bg("op_snowywoods")
        ks_bg("school_auditorium")
        ks_bg("school_backexit")
        ks_bg("school_backstage")
        ks_bg("school_cafeteria")
        ks_bg("school_classroomart")
        ks_bg("school_council")
        ks_bg("school_courtyard")
        ks_bg("school_dormbathroom")
        ks_bg("school_dormemi")
        ks_bg("school_dormext")
        ks_bg("school_dormext_start")
        ks_bg("school_dormext_half")
        ks_bg("school_dormext_full")
        ks_bg("school_dormhallground")
        ks_bg("school_dormhallway")
        ks_bg("school_girlsdormhall")
        ks_bg("school_dormhisao")
        ks_bg("school_dormhisao_blurred")
        ks_bg("school_dormkenji")
        ks_bg("school_dormlilly")
        ks_bg("school_dormrin")
        ks_bg("school_dormshizune")
        ks_bg("school_dormhanako")
        ks_bg("school_forest1")
        ks_bg("school_forest2")
        ks_bg("school_forestclearing")
        ks_bg("school_gate")
        ks_bg("school_gardens")
        ks_bg("school_gardens2")
        ks_bg("school_gardens3")
        ks_bg("school_greathall")
        ks_bg("school_hallway2")
        ks_bg("school_hallway3")
        ks_bg("school_hallway3_blurred")
        ks_bg("school_hallwayextra")
        ks_bg("school_hilltop_border")
        ks_bg("school_hilltop_border_summer")
        ks_bg("school_hilltop_spring")
        ks_bg("school_hilltop_summer")
        ks_bg("school_library")
        ks_bg("school_lobby")
        ks_bg("school_musicroom")
        ks_bg("school_nursehall")
        ks_bg("school_nurseoffice")
        ks_bg("school_principal")
        ks_bg("school_roof")
        ks_bg("school_roof_blurred")
        ks_bg("school_scienceroom")
        ks_bg("school_sportsstoreroom")
        ks_bg("school_sportsstoreext")
        ks_bg("school_staircase1")
        ks_bg("school_staircase2")
        ks_bg("school_stalls1")
        ks_bg("school_stalls2")
        ks_bg("school_track")
        ks_bg("school_track_on")
        ks_bg("school_track_running")
        ks_bg("school_road")
        ks_bg("school_miyagi")
        ks_bg("school_miyagi_blurred")
        ks_bg("school_room32")
        ks_bg("school_room34")
        ks_bg("school_parkinglot")
        ks_bg("school_nomiya")
        ks_bg("shizu_houseext")
        ks_bg("shizu_houseext_lights")
        ks_bg("shizu_living")
        ks_bg("shizu_guestbed")
        ks_bg("shizu_guesthisao")
        ks_bg("shizu_fishing")
        ks_bg("shizu_park")
        ks_bg("shizu_garden")
        ks_bg("suburb_konbiniext")
        ks_bg("suburb_konbiniint")
        ks_bg("suburb_park")
        ks_bg("suburb_roadcenter")
        ks_bg("suburb_shanghaiext")
        ks_bg("suburb_shanghaiint")
        ks_bg("suburb_tanabata")

    image completionbonus = hb_rescale_image("event/completionbonus.jpg")

    image heartattack alpha = im.Alpha(hb_rescale_image("vfx/heart_attack.png"), 0.3)
    image heartattack residual = im.Alpha(hb_rescale_image("vfx/heart_attack.png"), 0.17)
    image heartattack = hb_rescale_image("vfx/heart_attack.png")

    python:
#         emi_list = ['basic_happy',
#                     'basic_happyblush',
#                     'basic_confused',
#                     'basic_concentrate',
#                     'basic_shock',
#                     'basic_annoyed',
#                     'basic_hes',
#                     'basic_grin',
#                     'basic_closedhappy',
#                     'basic_closedgrin',
#                     'basic_closedsweat',
#                     'excited_laugh',
#                     'excited_amused',
#                     'excited_joy',
#                     'excited_happy',
#                     'excited_happyblush',
#                     'excited_hesitant',
#                     'excited_proud',
#                     'excited_circle',
#                     'excited_sad',
#                     'excited_smile',
#                     'excited_happy',
#                     'sad_angry',
#                     'sad_annoyed',
#                     'sad_pout',
#                     'sad_shy',
#                     'sad_shyblush',
#                     'sad_depressed',
#                     'sad_grit',
#                     'sad_grin',
#                     ]
        make_sprites('emi',emi_list, ['gym'])
#
#
#
#         emi_list_new = {'basic':(['happy','happyblush','confused','concentrate','shock','annoyed','hes','grin','closedhappy','closedgrin','closedsweat'],(94,209),(110,140)),
#                         'excited':(['laugh','amused','joy','happy','happyblush','hesitant','proud','circle','sad','smile','happy'],(88,222),(105,168)),
#                         'sad':(['angry','annoyed','pout','shy','shyblush','depressed','grit','grin'],(87,212),(99,150))}
#
#
#
#         emicas_list = ['angry',
#                        'awayfrown',
#                        'blush',
#                        'closedsmile',
#                        'evil',
#                        'frown',
#                        'grin',
#                        'grit',
#                        'happy',
#                        'neutral',
#                        'pout',
#                        'sad',
#                        'smile',
#                        'weaksmile',
#                        'wink',
#                        ]
        make_sprites('emicas',emicas_list, ['up'])
        make_sprites('emiwheel',emicas_list)
        make_sprites('eminude',emicas_list)
#
#
#
#         def scaled_runtime(length, expired):
#             return min((float(expired) / float(length)), 1.0)
#
#         def tremble_general(time, xpos, ypos, xanchor, yanchor, d, st, at):
#             import math, random
#
#             if not time:
#                 n = st - int(st)
#             else:
#                 n = scaled_runtime(time, st)
#
#             jitter = 0.0002 * (random.randint(-1,1))
#             m = math.sin(n*math.pi*40) * 0.001 + jitter
#             d.xanchor = xanchor
#             d.yanchor = yanchor
#             d.xpos = xpos + m
#             d.ypos = ypos + jitter
#             return 0
#
#         def tf_centertremble_loop(d, st, at):
#             return tremble_general(False, 0.5, 1.0, 0.5, 1.0, d, st, at)
#
#         def tf_centertremble(d, st, at):
#             return tremble_general(1.0, 0.5, 1.0, 0.5, 1.0, d, st, at)
#
#         def tf_centertremble_sit(d, st, at):
#             return tremble_general(1.0, 0.5, 1.09, 0.5, 1.0, d, st, at)
#
#         centertremble_sit = Transform(function=tf_centertremble_sit)
#
#         def tf_lefttremble(d, st, at):
#             return tremble_general(1.0, 0.3, 1.0, 0.5, 1.0, d, st, at)
#
#         def bounce_general(time, amplitude, num, d, st, at):
#             import math
#             n = scaled_runtime(time, st)
#             m = -math.fabs(math.sin(n*math.pi*num))
#             d.yanchor = 0.0
#             d.ypos = m*amplitude
#             return 0
#
#         def tf_fourbounce60(d, st, at):
#             return bounce_general(2.1, 0.1, 4, d, st, at)
#
#         def tf_fourbounce30(d, st, at):
#             return bounce_general(1.8, 0.04, 4, d, st, at)
#
#         def tf_onebounce(d, st, at):
#             return bounce_general(0.5, 0.04, 1, d, st, at)
#
#         def tf_leftrock(d, st, at):
#             import math
#             n = scaled_runtime(4.0, st)
#             m = math.sin(n*math.pi*8) * (1-n)
#             d.xpos = 0.3+(m*0.05)
#             d.ypos = 1.0
#             d.xanchor = 0.5
#             d.yanchor = 0.9
#             return 0

        emigymbouncecomp = im.Composite (hb_rescale_xy(295,630),
                                         (0,0),hb_rescale_image("sprites/emi/emi_basic_grin_gym.png"),
                                         hb_rescale_xy(0,600),hb_rescale_image("vfx/emi_gym_30pxlegs.png"))

        emigymclosedbouncecomp = im.Composite (hb_rescale_xy(295,630),
                                         (0,0),hb_rescale_image("sprites/emi/emi_basic_closedgrin_gym.png"),
                                         hb_rescale_xy(0,600),hb_rescale_image("vfx/emi_gym_30pxlegs.png"))

        emigymconcentratebouncecomp = im.Composite (hb_rescale_xy(295,630),
                                         (0,0),hb_rescale_image("sprites/emi/emi_basic_concentrate_gym.png"),
                                         hb_rescale_xy(0,600),hb_rescale_image("vfx/emi_gym_30pxlegs.png"))

        emiannoyedbouncecomp = im.Composite (hb_rescale_xy(295,660),
                                         (0,0),hb_rescale_image("sprites/emi/emi_basic_annoyed.png"),
                                         hb_rescale_xy(0,600),hb_rescale_image("vfx/emi_uni_60pxlegs.png"))

        emihappybouncecomp = im.Composite (hb_rescale_xy(295,630),
                                         (0,0),hb_rescale_image("sprites/emi/emi_basic_closedhappy.png"),
                                         hb_rescale_xy(0,600),hb_rescale_image("vfx/emi_uni_60pxlegs.png"))

    image emi annoyedbounce = At(emiannoyedbouncecomp,Transform(function=tf_fourbounce60))
    image emi gymbounce = At(emigymbouncecomp,Transform(function=tf_fourbounce30))
    image emi gymconcentratebounce = At(emigymconcentratebouncecomp,Transform(function=tf_fourbounce30))
    image emi gymbounceclosed = At(emigymclosedbouncecomp,Transform(function=tf_fourbounce30))
    image emi gymbounce_once = At(emigymclosedbouncecomp,Transform(function=tf_onebounce))
    image emi happybounce = At(emihappybouncecomp,Transform(function=tf_fourbounce30))

    image emi blur = hb_rescale_image("vfx/emi_blur.png")

    image bg school_track_fb = past(hb_rescale_image("bgs/school_track.jpg"))
    image emi basic_closedhappy_gym_fb = past(hb_rescale_image("sprites/emi/emi_basic_closedhappy_gym.png"))
    image emi basic_grin_gym_fb = past(hb_rescale_image("sprites/emi/emi_basic_grin_gym.png"))
    image emi sad_grin_gym_fb = past(hb_rescale_image("sprites/emi/emi_sad_grin_gym.png"))
    image emi excited_proud_gym_fb = past(hb_rescale_image("sprites/emi/emi_excited_proud_gym.png"))

    python:
#         rin_list = ['basic_absent',
#                     'basic_amused',
#                     'basic_awayabsent',
#                     'basic_blush',
#                     'basic_delight',
#                     'basic_lucid',
#                     'basic_sad',
#                     'basic_surprised',
#                     'basic_upset',
#                     'basic_crying',
#                     'basic_deadpanupset',
#                     'basic_deadpan',
#                     'basic_deadpanamused',
#                     'basic_deadpancontemplation',
#                     'basic_deadpandelight',
#                     'basic_deadpannormal',
#                     'basic_deadpansurprised',
#                     'negative_angry',
#                     'negative_annoyed',
#                     'negative_confused',
#                     'negative_crying',
#                     'negative_sad',
#                     'negative_spaciness',
#                     'negative_worried',
#                     'relaxed_boredom',
#                     'relaxed_disgust',
#                     'relaxed_doubt',
#                     'relaxed_nonchalant',
#                     'relaxed_sleepy',
#                     'relaxed_surprised',
#                     'back',
#                     ]
        make_sprites('rin',rin_list, ['cas'])
        make_sprites('rinpan', rin_list)
#
#
#
#         rin_list_new = {'basic':(['absent','basic_amused','awayabsent','blush','delight','lucid','sad','surprised','upset','crying','deadpanupset','deadpan','deadpanamused','deadpancontemplation','deadpandelight','deadpannormal','deadpansurprised'],(34,134),(100,77)),
#                         'negative':(['angry','annoyed','confused','crying','sad','spaciness','worried'],(30,139),(86,99)),
#                         'relaxed':(['boredom','disgust','doubt','nonchalant','sleepy','surprised'],(72,130),(158,83)),
#                         '':(['back'],(0,0),(0,0))}
#
#
#         lilly_list = ['basic_ara',
#                     'basic_arablush',
#                     'basic_cheerful',
#                     'basic_cheerfulblush',
#                     'basic_concerned',
#                     'basic_displeased',
#                     'basic_emb',
#                     'basic_giggle',
#                     'basic_listen',
#                     'basic_oops',
#                     'basic_planned',
#                     'basic_pout',
#                     'basic_reminisce',
#                     'basic_sad',
#                     'basic_satisfied',
#                     'basic_sleepy',
#                     'basic_smile',
#                     'basic_smileclosed',
#                     'basic_surprised',
#                     'basic_veryemb',
#                     'basic_weaksmile',
#                     'behind_cheerful',
#                     'behind_displeased',
#                     'behind_emb',
#                     'behind_cheerful',
#                     'behind_giggle',
#                     'behind_listen',
#                     'behind_cheerful',
#                     'behind_pout',
#                     'behind_cheerful',
#                     'behind_reminisce',
#                     'behind_sleepy',
#                     'behind_smile',
#                     'behind_smileclosed',
#                     'behind_weaksmile',
#                     'cane_ara',
#                     'cane_arablush',
#                     'cane_cheerful',
#                     'cane_cheerfulblush',
#                     'cane_concerned',
#                     'cane_displeased',
#                     'cane_emb',
#                     'cane_giggle',
#                     'cane_listen',
#                     'cane_mad',
#                     'cane_oops',
#                     'cane_planned',
#                     'cane_pout',
#                     'cane_reminisce',
#                     'cane_sad',
#                     'cane_satisfied',
#                     'cane_sleepy',
#                     'cane_smile',
#                     'cane_smileclosed',
#                     'cane_surprised',
#                     'cane_weaksmile',
#                     'back_devious',
#                     'back_giggle',
#                     'back_listen',
#                     'back_pout',
#                     'back_smile',
#                     'back_smileclosed',
#                     'back_surprise',
#                     'back_sad',
#                     ]
        make_sprites('lilly',lilly_list,['paj','cas','che','nak'])
#
#         lilly_list_new = {'basic':(['ara','arablush','cheerful','cheerfulblush','concerned','displeased','emb','giggle','listen','oops','planned','pout','reminisce','sad','satisfied','sleepy','smile','smileclosed','surprised','veryemb','weaksmile'],(85,98),()),
#                           'behind':(['cheerful','displeased','emb','cheerful','giggle','listen','cheerful','pout','cheerful','reminisce','sleepy','smile','smileclosed','weaksmile'],(85,98),()),
#                           'cane':(['ara','arablush','cheerful','cheerfulblush','concerned','displeased','emb','giggle','listen','mad','oops','planned','pout','reminisce','sad','satisfied','sleepy','smile','smileclosed','surprised','weaksmile'],(85,98),()),
#                           'back':(['devious','giggle','listen','pout','smile','smileclosed','surprise','sad'],(124,94),())}
#
#
#
#
#         hana_list = ['basic_bashful',
#                     'basic_distant',
#                     'basic_normal',
#                     'basic_smile',
#                     'basic_worry',
#                     'cover_bashful',
#                     'cover_distant',
#                     'cover_smile',
#                     'cover_worry',
#                     'def_shock',
#                     'def_strain',
#                     'def_worry',
#                     'defarms_shock',
#                     'defarms_strain',
#                     'defarms_worry',
#                     'emb_blushing',
#                     'emb_blushtimid',
#                     'emb_downsad',
#                     'emb_downsmile',
#                     'emb_downtimid',
#                     'emb_emb',
#                     'emb_sad',
#                     'emb_smile',
#                     'emb_timid',
#                     ]
        make_sprites('hanako',hana_list,['cas'])
#
#         hana_list_new = {'basic':(['bashful','distant','normal','smile','worry'],(113,129),(186,84)),
#                          'cover':(['bashful','distant','smile','worry'],(113,129),(186,84)),
#                          'def':(['def_shock','def_strain','def_worry'],(106,130),(171,77)),
#                          'defarms':(['shock','strain','worry'],(106,130),(171,77)),
#                          'emb':(['blushing','blushtimid','downsad','downsmile','downtimid','emb','sad','smile','timid'],(97,141),(155,93)),}
#
#
#         hanag_list = ['distant',
#                     'drunkdistant',
#                     'drunkgiggle',
#                     'drunknormal',
#                     'drunkpout',
#                     'drunksad',
#                     'drunksmile',
#                     'drunkworry',
#                     'stockdistant',
#                     'stocknormal',
#                     'stockworry',
#                     'worry',
#                     'normal',
#                     'irritated',
#                     'smile',
#                     'worry',
#                     ]
        make_sprites('hanagown',hanag_list,['blush'])
#
#
#         hanag_list_new = ['distant',
#                     'distant_blush',
#                     'drunkdistant',
#                     'drunkgiggle',
#                     'drunknormal',
#                     'drunkpout',
#                     'drunksad',
#                     'drunksmile',
#                     'drunkworry',
#                     'stockdistant_blush',
#                     'stocknormal_blush',
#                     'stockworry_blush',
#                     'worry',
#                     'normal',
#                     'normal_blush',
#                     'irritated',
#                     'smile',
#                     'worry',
#                     'worry_blush',
#                     ]
#
#
#
#
#     $ centertremble = Transform(function=tf_centertremble_loop)
#     $ centertremble_nl = Transform(function=tf_centertremble)

    image hanako tremble = At(hb_rescale_image("sprites/hanako/hanako_def_shock.png"),Transform(function=tf_centertremble))
    image hanako emb_downtimid_tremble = At(hb_rescale_image("sprites/hanako/hanako_emb_downtimid.png"),Transform(function=tf_centertremble_loop))

    python:
#         shizu_list = ['adjust_happy',
#                     'adjust_angry',
#                     'adjust_blush',
#                     'adjust_smug',
#                     'adjust_frown',
#                     'adjust_noglasses',
#                     'basic_normal',
#                     'basic_normal2',
#                     'basic_frown',
#                     'basic_happy',
#                     'basic_angry',
#                     'basic_sparkle',
#                     'behind_blank',
#                     'behind_frown',
#                     'behind_frustrated',
#                     'behind_sad',
#                     'behind_smile',
#                     'behind_smilelow',
#                     'cross_angry',
#                     'cross_rage',
#                     'cross_rageclosed',
#                     'cross_shock',
#                     'cross_stunned',
#                     'cross_wut',
#                     'out_serious',
#                     ]
#
#         shizu_list_new = {'adjust':(['happy','angry','blush','smug','frown','noglasses'],(66,161),(98,108)),
#                           'basic':(['normal','normal2','frown','happy','angry','sparkle'],(136,171),(204,131)),
#                           'behind':(['blank','frown','frustrated','sad','smile','smilelow'],(68,173),(103,131)),
#                           'cross':(['angry','rage','rageclosed','shock','stunned','wut'],(84,193),(125,163)),
#                           'out':(['serious'],(0,0),(0,0))}
#
        make_sprites('shizu',shizu_list,['cas','nak'])
#
#         shizuyu_list = ['basic_angry',
#                         'basic_aside',
#                         'basic_blush',
#                         'basic_happy',
#                         'cross_angry',
#                         'cross_aside',
#                         'cross_blush',
#                         'cross_happy',
#                         ]
        make_sprites('shizuyu',shizuyu_list)
#
#         shizuyu_list_new = {'basic':(['angry','aside','blush','happy'],(84,167),(150,110)),
#                             'cross':(['angry','aside','blush','happy'],(84,167),(150,110))}
#
#
#
#         misha_list = ['perky_sad',
#                     'perky_confused',
#                     'perky_smile',
#                     'sign_sad',
#                     'sign_confused',
#                     'sign_smile',
#                     'hips_frown',
#                     'hips_laugh',
#                     'hips_grin',
#                     'hips_smile',
#                     'cross_frown',
#                     'cross_laugh',
#                     'cross_grin',
#                     'cross_smile',
#                     ]
        make_sprites('misha',misha_list,['cas','yuk'])
        make_sprites('mishashort',misha_list,['cas'])
#
#         misha_list_new = {'perky':(['sad','confused','smile'],(103,163),(150,110)),
#                           'sign':(['sad','confused','smile'],(103,164),(150,110)),
#                           'hips':(['frown','laugh','grin','smile'],(102,168),(150,110)),
#                           'cross':(['frown','laugh','grin','smile'],(101,164),(150,110))}
#
#
#         yuuko_list = ['closedhappy',
#                       'cry',
#                       'happy',
#                       'neurotic',
#                       'neutral',
#                       'panic',
#                       'smile',
#                       'worried',
#                       'noglasses',
#                       ]
        make_sprites('yuuko',yuuko_list,['up','down'])
        make_sprites('yuukoshang',yuuko_list,['up','down'])

        make_sprites('akira',['basic_smile','basic_lost','basic_kill','basic_annoyed','basic_resigned','basic_boo', 'basic_distant','basic_laugh','basic_ending','basic_evil'])
        make_sprites('hideaki',['angry','angry_up','bored','bored_up','closed_up','confused','darkside','disapproves','evil','happy','happy_up','kiss','normal','normal_up','ohshit','sad','surprise','surprise_up','thinking','serious','serious_up','triangle'])
        make_sprites('jigoro',['angry','laugh','neutral','smug'])
        make_sprites('kenji',['neutral','happy','tsun','rage'], ['naked'])
        make_sprites('nurse',['neutral','concern','fabulous','grin'])
        make_sprites('muto',['normal','smile','irritated'])
        make_sprites('nomiya',['dreamy','frown','serious','smile','stern','talk','talktongue','veryhappy'])
        make_sprites('sae',['neutral','smile','scowl','doubt','scold'], ['smoke'])
        make_sprites('meiko',['happy','serious','smile','wink','worry'])
        make_sprites('shopkeep',['happy', 'neutral', 'surprised', 'thinking'])
        make_sprites('miki',['angry','confused','grin','grinclosed','serious','smile','whistle','wink'])

    image ev other_iwanako_start:
        hb_rescale_image("event/other_iwanako_nosnow.jpg")
        xalign 0.5 yalign 0.9 zoom 1.2 subpixel True
        acdc_warp 20.0 zoom 1.0 yalign 0.5

    image ev other_iwanako = hb_rescale_image("event/other_iwanako_nosnow.jpg")
    image evul other_iwanako = hb_rescale_image("event/other_iwanako.jpg")

    image ev hisao_class_start = im.Crop(hb_rescale_image("event/hisao_class.jpg"), 0, 0, hb_rescale_x(800), hb_rescale_y(600))

    image ev hisao_class_move:
        hb_rescale_image("event/hisao_class.jpg")
        xalign 0.0 subpixel True
        acdc_warp 40.0 xalign 1.0

    image ev hisao_class_end = im.Crop(hb_rescale_image("event/hisao_class.jpg"), hb_rescale_x(800), 0, hb_rescale_x(800), hb_rescale_y(600))

    image ev emi_knockeddown = hb_rescale_image("event/emi_knockeddown.jpg")

    image ev emi_knockeddown_facepullout:
        hb_rescale_image("event/emi_knockeddown_large.jpg")
        size hb_rescale_xy(800,600) crop (hb_rescale_x(840), hb_rescale_y(50), hb_rescale_x(800), hb_rescale_y(600)) subpixel True
        easeout 10.0 crop (hb_rescale_x(840), hb_rescale_y(50), hb_rescale_x(880), hb_rescale_y(660))

    image ev emi_knockeddown_largepullout:
        hb_rescale_image("event/emi_knockeddown.jpg")
        size hb_rescale_xy(800,600) crop (hb_rescale_x(40), hb_rescale_y(30), hb_rescale_x(720), hb_rescale_y(540)) subpixel True
        easein 10.0 crop (0, 0, hb_rescale_x(800), hb_rescale_y(600))

    image ev emi_knockeddown_face = im.Crop(hb_rescale_image("event/emi_knockeddown_large.jpg"), hb_rescale_x(840), hb_rescale_y(50), hb_rescale_x(800), hb_rescale_y(600))

    image ev emi_knockeddown_legs:
        size None crop None subpixel True
        hb_rescale_image("event/emi_knockeddown_large.jpg")
        xpos hb_rescale_x(2350) ypos hb_rescale_y(1010) xanchor hb_rescale_x(2400) yanchor hb_rescale_y(1800)
        ease 8.0 xpos hb_rescale_x(1900) ypos hb_rescale_y(900)

    image ev emi_run_face_zoomin:
        hb_rescale_image("event/emi_run_face.jpg")
        size hb_rescale_xy(800,600) crop (0, 0, hb_rescale_x(800), hb_rescale_y(600)) subpixel True
        ease 10.0 crop (hb_rescale_x(40), hb_rescale_y(30), hb_rescale_x(720), hb_rescale_y(540))

    image ev emi_run_face = hb_rescale_image("event/emi_run_face.jpg")

    image ev emi_run_face_zoomout_ss:
        sunset(hb_rescale_image("event/emi_run_face.jpg"))
        size hb_rescale_xy(800,600) crop (hb_rescale_x(40), hb_rescale_y(30), hb_rescale_x(720), hb_rescale_y(540)) subpixel True
        ease 10.0 crop (0, 0, hb_rescale_x(800), hb_rescale_y(600))

    image ev emi_firstkiss = hb_rescale_image("event/emi_firstkiss.jpg")

    image insert startpistol = hb_rescale_image("vfx/startpistol.png")

    image ev emitrack_running = hb_rescale_image("event/emitrack_running.jpg")
    image ev emitrack_blocks:
        hb_rescale_image("event/emitrack_blocks.jpg")
        xalign 0.0
    image ev emitrack_blocks_close = hb_rescale_image("event/emitrack_blocks_close.jpg")
    image ev emitrack_blocks_close_grin = hb_rescale_image("event/emitrack_blocks_close_grin.jpg")
    image ev emitrack_finishtop = hb_rescale_image("event/emitrack_finishtop.jpg")
    image ev emitrack_finish = hb_rescale_image("event/emitrack_finish.jpg")

    image ev emi_sleepy = hb_rescale_image("event/emi_sleepy.jpg")
    image ev emi_sleepy_face = hb_rescale_image("event/emi_sleepy_face.jpg")
    image ev emi_sleepy_legs = hb_rescale_image("event/emi_sleepy_legs.jpg")

    image ev emi_bed_frown = hb_rescale_image("event/emi_bed_frown.jpg")
    image ev emi_bed_happy = hb_rescale_image("event/emi_bed_happy.jpg")
    image ev emi_bed_normal = hb_rescale_image("event/emi_bed_normal.jpg")
    image ev emi_bed_smile = hb_rescale_image("event/emi_bed_smile.jpg")
    image ev emi_bed_unsure = hb_rescale_image("event/emi_bed_unsure.jpg")

    image ev emi_forehead = hb_rescale_image("event/emi_forehead.jpg")

    image ev emi_sleep_cry = hb_rescale_image("event/emi_sleep_cry.jpg")
    image ev emi_sleep_normal = hb_rescale_image("event/emi_sleep_normal.jpg")
    image ev emi_sleep_unsure = hb_rescale_image("event/emi_sleep_unsure.jpg")
    image ev emi_sleep_weep = hb_rescale_image("event/emi_sleep_weep.jpg")

    image ev emi_parkback = hb_rescale_image("event/emi_parkback.jpg")
    image ev emi_parkback_frown = hb_rescale_image("event/emi_parkback_frown.jpg")

    image ev emi_ending_smile = hb_rescale_image("event/emi_ending_smile.jpg")
    image ev emi_ending_serious = hb_rescale_image("event/emi_ending_serious.jpg")
    image ev emi_ending_glad = hb_rescale_image("event/emi_ending_glad.jpg")

    image ev picnic_normal:
        hb_rescale_image("event/picnic_normal.jpg")
        xalign 0.5 yalign 0.0
    image ev picnic_rain:
        hb_rescale_image("event/picnic_rain.jpg")
        xalign 0.5 yalign 0.0

#     python:
#         def slow_out(trans, st, at):
#             trans.yalign = 0.5
#             trans.xalign = 0.5
#             n = scaled_runtime(60.0, at)
#             trans.zoom = 0.8 + (((-1.0 * acdc_warp(n)) + 1.0 ) * 0.2)
#             return 0
#
#     transform slow_out_tf:
#         subpixel True
#         function slow_out
#         repeat

    image ev emi_cry_down = hb_rescale_image("event/emi_cry_down.jpg")
    image evul emi_cry_down:
        hb_rescale_image("event/emi_cry_down.jpg")
        zoom 0.8
    image ev emi_grave = hb_rescale_image("event/emi_grave.jpg")

    image evh emi_grinding_victorytall = hb_rescale_image("event/emi_grinding/emi_grinding_victorytall.jpg")
    image evh emi_grinding_victory = hb_rescale_image("event/emi_grinding/emi_grinding_victory.jpg")
    image evh emi_grinding_wink = hb_rescale_image("event/emi_grinding/emi_grinding_wink.jpg")
    image evh emi_grinding_grin = hb_rescale_image("event/emi_grinding/emi_grinding_grin.jpg")
    image evh emi_grinding_half_undress = hb_rescale_image("event/emi_grinding/emi_grinding_half_undress.jpg")
    image evh emi_grinding_half_grin = hb_rescale_image("event/emi_grinding/emi_grinding_half_grin.jpg")
    image evh emi_grinding_off_yawn = hb_rescale_image("event/emi_grinding/emi_grinding_off_yawn.jpg")
    image evh emi_grinding_off_closesurprise = hb_rescale_image("event/emi_grinding/emi_grinding_off_closesurprise.jpg")
    image evh emi_grinding_off_closearoused = hb_rescale_image("event/emi_grinding/emi_grinding_off_closearoused.jpg")
    image evh emi_grinding_off_aroused = hb_rescale_image("event/emi_grinding/emi_grinding_off_aroused.jpg")
    image evh emi_grinding_off_arousedclosed = hb_rescale_image("event/emi_grinding/emi_grinding_off_arousedclosed.jpg")
    image evh emi_grinding_off_come = hb_rescale_image("event/emi_grinding/emi_grinding_off_come.jpg")
    image evh emi_grinding_off_end = hb_rescale_image("event/emi_grinding/emi_grinding_off_end.jpg")

    image evh emi_shed_base1 = hb_rescale_image("event/emi_shed/emi_shed_base1.jpg")
    image evh emi_shed_base2 = hb_rescale_image("event/emi_shed/emi_shed_base2.jpg")
    image evh emi_shed_base3 = hb_rescale_image("event/emi_shed/emi_shed_base3.jpg")
    image evh emi_shed_base4 = hb_rescale_image("event/emi_shed/emi_shed_base4.jpg")

    image evh_l emi_shed_up = hb_rescale_image("event/emi_shed/emi_shed_lhand_up.png")
    image evh_l emi_shed_down = hb_rescale_image("event/emi_shed/emi_shed_lhand_down.png")

    image evh_r emi_shed_up = hb_rescale_image("event/emi_shed/emi_shed_rhand_up.png")
    image evh_r emi_shed_down = hb_rescale_image("event/emi_shed/emi_shed_rhand_down.png")

    image hisao emi_shed_closed = hb_rescale_image("event/emi_shed/emi_shed_hisao_closed.png")
    image hisao emi_shed_neutral = hb_rescale_image("event/emi_shed/emi_shed_hisao_neutral.png")
    image hisao emi_shed_sweat = hb_rescale_image("event/emi_shed/emi_shed_hisao_sweat.png")

    image emi emi_shed_closed = hb_rescale_image("event/emi_shed/emi_shed_emi_closed.png")
    image emi emi_shed_grin = hb_rescale_image("event/emi_shed/emi_shed_emi_grin.png")
    image emi emi_shed_hesitant = hb_rescale_image("event/emi_shed/emi_shed_emi_hesitant.png")
    image emi emi_shed_shock = hb_rescale_image("event/emi_shed/emi_shed_emi_shock.png")

    image evh emi_miss_closed = hb_rescale_image("event/emi_miss_closed.jpg")
    image evh emi_miss_open = hb_rescale_image("event/emi_miss_open.jpg")

    image prop rin_cigarette = hb_rescale_image("vfx/prop_rin_cigarette.png")
    image prop rin_cigarette_close = hb_rescale_image("vfx/prop_rin_cigarette_close.png")

    image ev rin_eating = hb_rescale_image("event/rin_eating.jpg")

    image ev rin_eating_zoomout:
        hb_rescale_image("event/rin_eating.jpg")
        size hb_rescale_xy(800,600) crop (hb_rescale_x(159),0,hb_rescale_x(640),hb_rescale_y(480)) subpixel True
        acdc_warp 10.0 crop (0, 0, hb_rescale_x(800), hb_rescale_y(600))

    image ev rin_artclass1:
        hb_rescale_image("event/rin_artclass1.jpg")
        yalign 0.5 xalign 1.0
    image ev rin_artclass2:
        hb_rescale_image("event/rin_artclass2.jpg")
        yalign 0.5 xalign 1.0
    image ev rin_artclass3:
        hb_rescale_image("event/rin_artclass3.jpg")
        yalign 0.5 xalign 1.0
    image ev rin_artclass4:
        hb_rescale_image("event/rin_artclass4.jpg")
        yalign 0.5 xalign 1.0 subpixel True
        0.5
        acdc_warp 10.0 xalign 0.0

#     python:
#         def slide_left_p(trans, st, at):
#             trans.xalign = _ease_in_time_warp(scaled_runtime(6.0, at))
#             return 0
#
#         def generic_rotateloop(length, dir, has_zoom, trans, st, at):
#             trans.yalign = 0.5
#             trans.xalign = 0.5
#             n = 0.0
#             if has_zoom:
#                 n = scaled_runtime(60.0, at)
#             trans.zoom = 0.834 + (((-1.0 * acdc_warp(n)) + 1.0 ) * 0.266)
#             trans.rotate = 360 / (length / (at + 0.1)) * dir
#             return 0
#
#         def wisp_func(trans, st, at):
#             return generic_rotateloop(180, 1, True, trans, st, at)
#
#         def smoke_func(trans, st, at):
#             return generic_rotateloop(180, -1, False, trans, st, at)
#
#     transform slide_left:
#         subpixel True
#         function slide_left_p
#
#     transform wispturn:
#         subpixel True
#         function wisp_func
#         repeat
#
#     transform smoketurn:
#         subpixel True
#         function smoke_func
#         repeat
#
#     transform wispturn_old:
#         rotate 0 zoom 1.0 xalign 0.5 yalign 0.5 subpixel True
#         linear 180 rotate 360
#         repeat
#
#     transform smoketurn_old:
#         rotate 0 zoom 1.3 xalign 0.5 yalign 0.5
#         linear 180 rotate -360
#         repeat

    image ev rin_wisp1:
        hb_rescale_image("event/rin_wisp1.jpg")
        truecenter
    image ev rin_wisp2:
        hb_rescale_image("event/rin_wisp2.jpg")
        truecenter
    image ev rin_wisp3:
        hb_rescale_image("event/rin_wisp3.jpg")
        truecenter
    image ev rin_wisp4:
        hb_rescale_image("event/rin_wisp4.jpg")
        truecenter
    image ev rin_wisp5:
        hb_rescale_image("event/rin_wisp5.jpg")
        truecenter
    image ev rin_wisp_blurred:
        hb_rescale_image("event/rin_wisp_blurred.jpg")
        truecenter
    image smoke focused = hb_rescale_image("event/rin_wisp_smoke_focused.png")
    image smoke blurred = hb_rescale_image("event/rin_wisp_smoke_blurred.png")

    image ovl rinbyhisao = hb_rescale_image("vfx/rinbyhisao.png")
    image ovl hisaobyrin = hb_rescale_image("vfx/hisaobyrin.png")

    python:
        def roofcomposite(comppath):
            return im.Composite(None,
                                (0,0), hb_rescale_image("event/rin_roof/rin_roof_boredom.jpg"),
                                hb_rescale_xy(300, 200), comppath)

    image ev hisao_mirror = hb_rescale_image("event/hisao_mirror.jpg")
    image ev hisao_mirror_800 = im.FactorScale(hb_rescale_image("event/hisao_mirror.jpg"), 0.8)

    image ev busride = hb_rescale_image("event/busride.jpg")
    image ev busride_ni = hb_rescale_image("event/busride_ni.jpg")

    image ev rin_roof_boredom = hb_rescale_image("event/rin_roof/rin_roof_boredom.jpg")
    image ev rin_roof_disgust = roofcomposite(hb_rescale_image("event/rin_roof/rin_roof_disgust.jpg"))
    image ev rin_roof_doubt = roofcomposite(hb_rescale_image("event/rin_roof/rin_roof_doubt.jpg"))
    image ev rin_roof_nonchalant = roofcomposite(hb_rescale_image("event/rin_roof/rin_roof_nonchalant.jpg"))
    image ev rin_roof_surprised = roofcomposite(hb_rescale_image("event/rin_roof/rin_roof_surprised.jpg"))

    image hisao rin_roof = hb_rescale_image("event/rin_roof/hisao_shadow.png")
    image emi rin_roof = hb_rescale_image("event/rin_roof/emi_shadow.png")

    image ev hisaobird_0 = hb_rescale_image("event/hisaobird/bird_0.jpg")
    image ev hisaobird_1 = hb_rescale_image("event/hisaobird/bird_1.jpg")
    image ev hisaobird_2 = hb_rescale_image("event/hisaobird/bird_2.jpg")
    image ev hisaobird_3 = hb_rescale_image("event/hisaobird/bird_3.jpg")
    image ev hisaobird_4 = hb_rescale_image("event/hisaobird/bird_4.jpg")
    image ev hisaobird_5 = hb_rescale_image("event/hisaobird/bird_5.jpg")
    image ev hisaobird_6 = hb_rescale_image("event/hisaobird/bird_6.jpg")
    image ev hisaobird_7 = hb_rescale_image("event/hisaobird/bird_7.jpg")
    image ev hisaobird_8 = hb_rescale_image("event/hisaobird/bird_8.jpg")
    image ev hisaobird_9 = hb_rescale_image("event/hisaobird/bird_9.jpg")

    image ev watch_black = hb_rescale_image("event/watch_black.jpg")
    image ev watch_worn = hb_rescale_image("vfx/watch_worn.png")
    image ev watch_worn_330 = night(hb_rescale_image("vfx/watch_worn_330.png"))
    image bg watchhallway_blur = hb_rescale_image("vfx/watchhallway_blur.jpg")

    image bg worrytree = hb_rescale_image("vfx/worrytree.jpg")
    image bg worrytree_ss = sunset(hb_rescale_image("vfx/worrytree.jpg"))

    image ev rin_painting_base = hb_rescale_image("event/rin_painting_base.jpg")
    image ev rin_painting_reply = hb_rescale_image("event/rin_painting_reply.jpg")
    image ev rin_painting_concerned = hb_rescale_image("event/rin_painting_concerned.jpg")
    image ev rin_painting_foot = hb_rescale_image("event/rin_painting_foot.jpg")
    image ev rin_painting_faceconcerned = hb_rescale_image("event/rin_painting_faceconcerned.jpg")

    image ev hisao_letter_closed = hb_rescale_image("event/hisao_letter_closed.jpg")
    image ev hisao_letter_open = hb_rescale_image("event/hisao_letter_open.jpg")
    image ev hisao_letter_open_2 = hb_rescale_image("event/hisao_letter_open_2.jpg")

    image ev rin_rain_away = hb_rescale_image("event/rin_rain_away.jpg")
    image ev rin_rain_towards = hb_rescale_image("event/rin_rain_towards.jpg")
    image ev rin_rain_away_close = hb_rescale_image("event/rin_rain_away_close.jpg")
    image ev rin_rain_towards_close = hb_rescale_image("event/rin_rain_towards_close.jpg")

    image ovl rin_rain_hisaotowards_close = im.Crop(hb_rescale_image("event/rin_rain_towards_close.jpg"), hb_rescale_x(400),0,hb_rescale_x(400),hb_rescale_y(1200))
    image ovl rin_rain_hisaotowards = im.Crop(hb_rescale_image("event/rin_rain_towards.jpg"), hb_rescale_x(400),0,hb_rescale_x(400),hb_rescale_y(600))

    image rin basic_deadpan_superclose = hb_rescale_image("sprites/rin/superclose/rin_basic_deadpan_superclose.png")
    image rin basic_deadpannormal_superclose = hb_rescale_image("sprites/rin/superclose/rin_basic_deadpannormal_superclose.png")
    image rin basic_lucid_superclose = hb_rescale_image("sprites/rin/superclose/rin_basic_lucid_superclose.png")
    image rin basic_crying_superclose_ss = sunset(hb_rescale_image("sprites/rin/superclose/rin_basic_crying_superclose.png"))
    image rin relaxed_doubt_superclose = hb_rescale_image("sprites/rin/superclose/rin_relaxed_doubt_superclose.png")
    image rin relaxed_sleepy_superclose = hb_rescale_image("sprites/rin/superclose/rin_relaxed_sleepy_superclose.png")
    image rin relaxed_surprised_superclose = hb_rescale_image("sprites/rin/superclose/rin_relaxed_surprised_superclose.png")
    image rin negative_crying_superclose = hb_rescale_image("sprites/rin/superclose/rin_negative_crying_superclose.png")
    image rin negative_crying_superclose_ss = sunset(hb_rescale_image("sprites/rin/superclose/rin_negative_crying_superclose.png"))

    image bg gallery_atelier_close = hb_rescale_image("vfx/gallery_atelier_close.jpg")
    image rin back_cas_superclose = hb_rescale_image("sprites/rin/superclose/rin_back_cas_superclose.png")

    image ev rin_kiss = hb_rescale_image("event/rin_kiss.jpg")

    image ev rin_high_frown = hb_rescale_image("event/rin_high_frown.jpg")
    image ev rin_high_grin = hb_rescale_image("event/rin_high_grin.jpg")
    image ev rin_high_grinwide = hb_rescale_image("event/rin_high_grinwide.jpg")
    image ev rin_high_oneeye = hb_rescale_image("event/rin_high_oneeye.jpg")
    image ev rin_high_open = hb_rescale_image("event/rin_high_open.jpg")
    image ev rin_high_sleep = hb_rescale_image("event/rin_high_sleep.jpg")
    image ev rin_high_smile = hb_rescale_image("event/rin_high_smile.jpg")

    image ev dandelion = hb_rescale_image("vfx/dandelion.jpg")

    image dandelion full = hb_rescale_image("vfx/dandelion_full.png")
    image dandelion gone = hb_rescale_image("vfx/dandelion_gone.png")
    image dandelionbg = hb_rescale_image("vfx/dandelionbg.jpg")

    image ev rin_nap_close_hand = hb_rescale_image("event/rin_nap_close_hand.jpg")
    image ev rin_nap_close_nohand = hb_rescale_image("event/rin_nap_close_nohand.jpg")
    image ev rin_nap_close_tears = hb_rescale_image("event/rin_nap_close_tears.png")
    image ev rin_nap_close_wind = hb_rescale_image("event/rin_nap_close_wind.jpg")
    image ev rin_nap_close = hb_rescale_image("event/rin_nap_close.jpg")
    image ev rin_nap_total_tears = hb_rescale_image("event/rin_nap_total_tears.png")
    image ev rin_nap_total_wind = hb_rescale_image("event/rin_nap_total_wind.jpg")
    image ev rin_nap_total = hb_rescale_image("event/rin_nap_total.jpg")

#     image ev rin_nap_total_awind:
#         "ev rin_nap_total"
#         block:
#             0.2
#             "ev rin_nap_total_wind" with Dissolve(0.4)
#             0.2
#             "ev rin_nap_total" with Dissolve(0.4)
#             repeat
#
#     image ev rin_nap_close_awind:
#         "ev rin_nap_close"
#         block:
#             0.2
#             "ev rin_nap_close_wind" with Dissolve(0.4)
#             0.2
#             "ev rin_nap_close" with Dissolve(0.4)
#             repeat

    image ev rin_nap_total_awind_tears = LiveComposite(hb_rescale_xy(800,600),
                                                       (0,0), "ev rin_nap_total_awind",
                                                       (0,0), "ev rin_nap_total_tears")
    image ev rin_nap_close_awind_tears = LiveComposite(hb_rescale_xy(800,600),
                                                       (0,0), "ev rin_nap_close_awind",
                                                       (0,0), "ev rin_nap_close_tears")

    image ev rin_masturbate_away = hb_rescale_image("event/rin_masturbate_away.jpg")
    image ev rin_masturbate_surprise = hb_rescale_image("event/rin_masturbate_surprise.jpg")
    image ev rin_masturbate_frown = hb_rescale_image("event/rin_masturbate_frown.jpg")
    image ev rin_masturbate_doubt = hb_rescale_image("event/rin_masturbate_doubt.jpg")
    image ev rin_masturbate_hug = hb_rescale_image("event/rin_masturbate_hug.jpg")

    image ovl rin_galleryskylight = hb_rescale_image("event/rin_galleryskylight.jpg")

    image ev rin_orange = hb_rescale_image("event/rin_orange.jpg")
    image ev rin_orange_large = hb_rescale_image("event/rin_orange_large.jpg")

    image evh rin_relief_up = hb_rescale_image("event/rin_relief_up.jpg")
    image evh rin_relief_up_large = hb_rescale_image("event/rin_relief_up_large.jpg")
    image evh rin_relief_down = hb_rescale_image("event/rin_relief_down.jpg")
    image evh rin_relief_down_large = hb_rescale_image("event/rin_relief_down_large.jpg")

    image ev rin_gallery:
        truecenter
        hb_rescale_image("event/rin_gallery.jpg")

    image doodlewhite = im.MatrixColor(hb_rescale_image("vfx/rin_doodle.png"),im.matrix.brightness(1))

    image ev rin_doodle:
        "doodlewhite"
        xalign 0.0 yalign 0.0 subpixel True
        parallel:
            acdc_warp 20.0 xalign 1.0 yalign 1.0
        parallel:
            1.0
            hb_rescale_image("vfx/rin_doodle.png") with ImageDissolve(hb_rescale_image("vfx/rin_doodle_tr.png"), 19.0, 32, alpha=True)

    image ev rin_doodle_all:
        hb_rescale_image("vfx/rin_doodle.png")
        truecenter
        zoom 0.9 subpixel True
        easein 12.0 zoom 0.75

    image bg misc_sky_rays = hb_rescale_image("bgs/misc_sky_rays.jpg")

    python:
        def rin_trueend_comp(list_in):
            baselist = [None, (0,0), hb_rescale_image("event/rin_trueend/rin_trueend_gone.jpg")]
            for offset, item in list_in:
                baselist.append(offset)
                baselist.append(hb_rescale_image("event/rin_trueend/rin_trueend_"+item+".jpg"))
            return im.Composite(*baselist)

    image ev rin_trueend_gone = hb_rescale_image("event/rin_trueend/rin_trueend_gone.jpg")
    image ev rin_trueend_gone_ni = night(hb_rescale_image("event/rin_trueend/rin_trueend_gone.jpg"))
    image ev rin_trueend_normal = rin_trueend_comp([(hb_rescale_xy(113,124),"normal")])
    image ev rin_trueend_closed = rin_trueend_comp([(hb_rescale_xy(113,124),"normal"), (hb_rescale_xy(177,129),"closed")])
    image ev rin_trueend_sad = rin_trueend_comp([(hb_rescale_xy(113,124),"normal"), (hb_rescale_xy(177,129),"sad")])
    image ev rin_trueend_smile = rin_trueend_comp([(hb_rescale_xy(113,124),"normal"), (hb_rescale_xy(177,129),"smile")])
    image ev rin_trueend_weaksmile = rin_trueend_comp([(hb_rescale_xy(113,124),"normal"), (hb_rescale_xy(177,129),"weaksmile")])
    image ev rin_trueend_hug = rin_trueend_comp([(hb_rescale_xy(335,51),"hug")])
    image ev rin_trueend_hugclosed = rin_trueend_comp([(hb_rescale_xy(335,51),"hug"), (hb_rescale_xy(484,154), "hugclosed")])

    image ev rin_wet_pan_down = hb_rescale_image("event/rin_wet/rin_wet_pan_down.jpg")
    image ev rin_wet_arms = hb_rescale_image("event/rin_wet/rin_wet_arms.jpg")
    image ev rin_wet_face_up = hb_rescale_image("event/rin_wet/rin_wet_face_up.jpg")
    image ev rin_wet_face_down = hb_rescale_image("event/rin_wet/rin_wet_face_down.jpg")
    image ev rin_wet_towel_up = hb_rescale_image("event/rin_wet/rin_wet_towel_up.jpg")
    image ev rin_wet_towel_down = hb_rescale_image("event/rin_wet/rin_wet_towel_down.jpg")
    image ev rin_wet_towel_touch = hb_rescale_image("event/rin_wet/rin_wet_towel_touch.jpg")

    image ev rin_pair_base = hb_rescale_image("event/rin_pair/rin_pair_base.jpg")
    image ev rin_pair_base_clothes = im.Composite(None,
                                                  (0,0),hb_rescale_image("event/rin_pair/rin_pair_base.jpg"),
                                                  (0,0),hb_rescale_image("event/rin_pair/rin_pair_hisao_clothes.png"))

    image rp_hisao normal = Solid("#00000000")
    image rp_hisao frown = hb_rescale_image("event/rin_pair/rin_pair_hisao_frown.png")
    image rp_hisao smile = hb_rescale_image("event/rin_pair/rin_pair_hisao_smile.png")
    image rp_rin normal = Solid("#00000000")
    image rp_rin closed = hb_rescale_image("event/rin_pair/rin_pair_rin_closed.png")
    image rp_rin frown = hb_rescale_image("event/rin_pair/rin_pair_rin_frown.png")
    image rp_rin smile = hb_rescale_image("event/rin_pair/rin_pair_rin_smile.png")
    image rp_rin talk = hb_rescale_image("event/rin_pair/rin_pair_rin_talk.png")

    $ d_rin_h2_pan_surprise = im.Composite(hb_rescale_xy(800,900),
                                           (0,0), hb_rescale_image("event/rin_h2/rin_h2_u_surprise.jpg"),
                                           hb_rescale_xy(0,300), hb_rescale_image("event/rin_h2/rin_h2_l_pan.jpg"))
    image evh rin_h2_pan_surprise = d_rin_h2_pan_surprise
    image unlock_evh rin_h2_pan_surprise = im.Crop(d_rin_h2_pan_surprise,0,0,hb_rescale_x(800),hb_rescale_y(600))

    $ d_rin_h2_pan_away = im.Composite(hb_rescale_xy(800,900),
                                       (0,0), hb_rescale_image("event/rin_h2/rin_h2_u_away.jpg"),
                                       hb_rescale_xy(0,300), hb_rescale_image("event/rin_h2/rin_h2_l_pan.jpg"))
    image evh rin_h2_pan_away = d_rin_h2_pan_away
    image unlock_evh rin_h2_pan_away = im.Crop(d_rin_h2_pan_away,0,0,hb_rescale_x(800),hb_rescale_y(600))

    $ d_rin_h2_pan_closed = im.Composite(hb_rescale_xy(800,900),
                                         (0,0), hb_rescale_image("event/rin_h2/rin_h2_u_closed.jpg"),
                                         hb_rescale_xy(0,300), hb_rescale_image("event/rin_h2/rin_h2_l_pan.jpg"))
    image evh rin_h2_pan_closed = d_rin_h2_pan_closed
    image unlock_evh rin_h2_pan_closed = im.Crop(d_rin_h2_pan_closed,0,0,hb_rescale_x(800),hb_rescale_y(600))

    image evh rin_h2_nopan_closed = im.Composite(hb_rescale_xy(800,900),
                                     (0,0), hb_rescale_image("event/rin_h2/rin_h2_u_closed.jpg"),
                                     hb_rescale_xy(0,300), hb_rescale_image("event/rin_h2/rin_h2_l_nopan.jpg"))

    image evh rin_h2_hisao_surprise = im.Composite(hb_rescale_xy(800,900),
                                     (0,0), hb_rescale_image("event/rin_h2/rin_h2_u_surprise.jpg"),
                                     hb_rescale_xy(0,300), hb_rescale_image("event/rin_h2/rin_h2_l_hisao.jpg"))

    image evh rin_h2_hisao_away = im.Composite(hb_rescale_xy(800,900),
                                     (0,0), hb_rescale_image("event/rin_h2/rin_h2_u_away.jpg"),
                                     hb_rescale_xy(0,300), hb_rescale_image("event/rin_h2/rin_h2_l_hisao.jpg"))

    image evh rin_h2_hisao_closed = im.Composite(hb_rescale_xy(800,900),
                                     (0,0), hb_rescale_image("event/rin_h2/rin_h2_u_closed.jpg"),
                                     hb_rescale_xy(0,300), hb_rescale_image("event/rin_h2/rin_h2_l_hisao.jpg"))

    python:
        def rin_h_comp(im_in, is_close=False):
            closer = ""
            if is_close:
                closer = "_close"
            return im.Composite(None,
                                (0,0), hb_rescale_image("event/rin_h/rin_h_closed"+closer+".jpg"),
                                (0,0), hb_rescale_image("event/rin_h/rin_h_"+im_in+closer+".png"))
    image evh rin_h_closed = hb_rescale_image("event/rin_h/rin_h_closed.jpg")
    image evh rin_h_left = rin_h_comp("left")
    image evh rin_h_normal = rin_h_comp("normal")
    image evh rin_h_right = rin_h_comp("right")
    image evh rin_h_strain = rin_h_comp("strain")

    image evh rin_h_closed_close = hb_rescale_image("event/rin_h/rin_h_closed_close.jpg")
    image evh rin_h_left_close = rin_h_comp("left", True)
    image evh rin_h_normal_close = rin_h_comp("normal", True)
    image evh rin_h_right_close = rin_h_comp("right", True)
    image evh rin_h_strain_close = rin_h_comp("strain", True)

    image ev rin_goodend_1 = hb_rescale_image("event/rin_goodend/rin_goodend_1.jpg")
    image ev rin_goodend_1b = hb_rescale_image("event/rin_goodend/rin_goodend_1b.jpg")
    image ev rin_goodend_2 = hb_rescale_image("event/rin_goodend/rin_goodend_2.jpg")

    image evbg rin_goodend_base = hb_rescale_image("event/rin_goodend/rin_goodend_base.jpg")
    image rin goodend_1 = hb_rescale_image("event/rin_goodend/rin_goodend_1.png")
    image rin goodend_1b = hb_rescale_image("event/rin_goodend/rin_goodend_1b.png")
    image rin goodend_2 = hb_rescale_image("event/rin_goodend/rin_goodend_2.png")
    image rin goodend_2_hires = hb_rescale_image("event/rin_goodend/rin_goodend_2_hires.png")
    image evfg rin_goodend = hb_rescale_image("event/rin_goodend/rin_goodend_fg.png")

#     image ev lilly_tearoom = hb_rescale_image("event/lilly_tearoom.jpg")
#     image ev lilly_tearoom_open = hb_rescale_image("event/lilly_tearoom_open.jpg")
#
#     image ev lilly_crane = hb_rescale_image("event/lilly_crane.jpg")
#
#     image ev lilly_touch_uni = hb_rescale_image("event/lilly_touch_uni.jpg")
#     image ev lilly_touch_cas = sunset(hb_rescale_image("event/lilly_touch_cas.jpg"))
#     image ev lilly_touch_cheong = hb_rescale_image("event/lilly_touch_cheong.jpg")
#
#     image ev braille = "vfx/braille.jpg"
#     image ev icecream = "vfx/icecream.jpg"
#
#     image evfg lilly_sunsetwalk = sunset(hb_rescale_image("event/lilly_sunsetwalk_fg.png"))
#     image evbg lilly_sunsetwalk = hb_rescale_image("event/lilly_sunsetwalk_bg.jpg")
#
#     image ev lilly_bedroom = hb_rescale_image("event/lilly_bedroom.jpg")
#     image ev lilly_bedroom_large = hb_rescale_image("event/lilly_bedroom_large.jpg")
#
#     image ev lilly_hanako_hug = hb_rescale_image("event/lilly_hanako_hug.jpg")
#     image unlock_ev lilly_hanako_hug_end:
#         hb_rescale_image("event/lilly_hanako_hug.jpg")
#         xalign 0.5 yalign 1.0 subpixel True
#         easein 6.0 yalign 0.0
#
#     image ev lilly_kissing = hb_rescale_image("event/lilly_kissing.jpg")
#
#     image ev lilly_sleeping = hb_rescale_image("event/lilly_sleeping.jpg")
#     image ev lilly_sleeping_smile = hb_rescale_image("event/lilly_sleeping_smile.jpg")
#
#     image train_scenery:
#         hb_rescale_image("event/lilly_train/train_scenery.jpg")
#         xalign 0.0
#         linear 2.0 xalign 1.0
#         repeat
#
#     image train_scenery_fg:
#         hb_rescale_image("event/lilly_train/train_scenery_fg.png")
#         offscreenright
#         linear 0.3 offscreenleft
#         4.0
#         repeat
#
#     image evfg lilly_trainride = hb_rescale_image("event/lilly_train/lilly_trainride.png")
#     image evfg lilly_trainride_smiles = im.Composite(None,
#                                                 (0,0), hb_rescale_image("event/lilly_train/lilly_trainride.png"),
#                                                 (338,301), hb_rescale_image("event/lilly_train/lilly_trainride_smile.png"),
#                                                 (573,331), hb_rescale_image("event/lilly_train/lilly_trainride_hanasmile.png"))
#
#     image ev lilly_trainride = hb_rescale_image("event/lilly_train/lilly_trainride.jpg")
#     image ev lilly_trainride_smiles = hb_rescale_image("event/lilly_train/lilly_trainride_smiles.jpg")
#
#     image train_scenery_ni:
#         hb_rescale_image("event/lilly_train/train_scenery_ni.jpg")
#         xalign 0.0
#         linear 2.0 xalign 1.0
#         repeat
#
#     image train_scenery_fg_ni:
#         hb_rescale_image("event/lilly_train/train_scenery_fg_ni.png")
#         offscreenright
#         linear 0.3 offscreenleft
#         4.0
#         repeat
#
#     python:
#         def trainwave(trans, st, at):
#             import math
#             trans.yalign = (math.sin(((at/2.0) % 2.0)*math.pi) + 1.0) / 2
#             return 0
#
#     transform train_shake:
#         subpixel True
#         function trainwave
#         repeat
#
#     image lilly_trainride_ni norm = hb_rescale_image("event/lilly_train/lilly_trainride_ni_norm.png")
#
#     python:
#         def traincomposite(comppath):
#             return im.Composite(None,
#                                 (0,0), hb_rescale_image("event/lilly_train/lilly_trainride_ni_norm.png"),
#                                 (321, 200), comppath)
#
#     image lilly_trainride_ni ara = traincomposite(hb_rescale_image("event/lilly_train/lilly_trainride_ni_ara.png"))
#     image lilly_trainride_ni opensmile = traincomposite(hb_rescale_image("event/lilly_train/lilly_trainride_ni_opensmile.png"))
#     image lilly_trainride_ni pout = traincomposite(hb_rescale_image("event/lilly_train/lilly_trainride_ni_pout.png"))
#     image lilly_trainride_ni smile = traincomposite(hb_rescale_image("event/lilly_train/lilly_trainride_ni_smile.png"))
#     image lilly_trainride_ni weaksmile = traincomposite(hb_rescale_image("event/lilly_train/lilly_trainride_ni_weaksmile.png"))
#
#
#     image ev lilly_trainride_ni = hb_rescale_image("event/lilly_train/lilly_trainride_ni.jpg")
#
#     image ev lilly_wheat_large = hb_rescale_image("event/lilly_wheat_large.jpg")
#     image ovl lilly_wheat_foreground = hb_rescale_image("event/lilly_wheat_foreground.png")
#
#     image unlock_ev lilly_wheat_close = im.Crop(hb_rescale_image("event/lilly_wheat_large.jpg"), 500,0,800,600)
#     image ev lilly_wheat_small = hb_rescale_image("event/lilly_wheat_small.jpg")
#
#
#     python:
#         def restaurant_out_f(trans, st, at):
#             trans.yalign = 0.5
#             trans.xalign = 0.5
#             n = scaled_runtime(20.0, at)
#             trans.zoom = 0.834 + (((-1.0 * acdc_warp(n)) + 1.0 ) * 0.266)
#             return 0
#
#     transform restaurant_out:
#         subpixel True
#         function restaurant_out_f
#
#     image ev lilly_restaurant_listen = hb_rescale_image("event/lilly_restaurant_listen.jpg")
#     image ev lilly_restaurant_sheepish = hb_rescale_image("event/lilly_restaurant_sheepish.jpg")
#
#     image evul lilly_restaurant_listen:
#         hb_rescale_image("event/lilly_restaurant_listen.jpg")
#         zoom 0.8
#     image evul lilly_restaurant_sheepish:
#         hb_rescale_image("event/lilly_restaurant_sheepish.jpg")
#         zoom 0.8
#
#     image ev lilly_restaurant_wine = hb_rescale_image("event/lilly_restaurant_wine.jpg")
#     image ev lilly_restaurant_eat = hb_rescale_image("event/lilly_restaurant_eat.jpg")
#     image ev lilly_restaurant_chew = hb_rescale_image("event/lilly_restaurant_chew.jpg")
#
#     image ev hisao_teacup = hb_rescale_image("event/hisao_teacup.jpg")
#     image evul hisao_teacup:
#         hb_rescale_image("event/hisao_teacup.jpg")
#         zoom 0.8
#
#     image ev akira_park = hb_rescale_image("event/akira_park.jpg")
#     image evul akira_park:
#         hb_rescale_image("event/akira_park.jpg")
#         zoom 0.8
#
#     image ev lilly_sheets = hb_rescale_image("event/lilly_sheets.jpg")
#
#     image ev lilly_hospitalwindow = hb_rescale_image("event/lilly_hospitalwindow.jpg")
#
#
#     image origami_hand = night("vfx/origami_hand.png")
#
#     image ev lilly_airport = hb_rescale_image("event/lilly_airport.jpg")
#     image ev lilly_airport_end = hb_rescale_image("event/lilly_airport_end.jpg")
#
#     image bg school_dormhisao_ni_fb = past("bgs/school_dormhisao_blurred_ni.jpg")
#     image origami_fb = past_night("vfx/origami_hand.png")
#     image bg shizu_houseext_lights_fb = past("bgs/shizu_houseext_lights.jpg")
#     image hideaki serious_up_fb = past_night("sprites/hideaki/hideaki_serious_up.png")
#     image bg hosp_ext_fb = past_night("bgs/hosp_ext.jpg")
#     image crowd_still1_fb = past_night("vfx/crowd1.png")
#     image crowd_still2_fb = past_night("vfx/crowd2.png")
#     image ev lilly_airport_end_fb = past(hb_rescale_image("event/lilly_airport_end.jpg"))
#
#
#     python:
#         def l_hosp_out_f(trans, st, at):
#             trans.yalign = 0.5
#             trans.xalign = 0.5
#             n = scaled_runtime(30.0, at)
#             trans.zoom = 0.8 + (((-1.0 * acdc_warp(n)) + 1.0 ) * 0.2)
#             return 0
#
#     transform l_hosp_out:
#         subpixel True
#         function l_hosp_out_f
#
#     image unlock_ev lilly_hospital:
#         hb_rescale_image("event/lilly_hospital.jpg")
#         zoom 0.8
#     image ev lilly_hospital = hb_rescale_image("event/lilly_hospital.jpg")
#     image unlock_ev lilly_hospitalclosed:
#         hb_rescale_image("event/lilly_hospitalclosed.jpg")
#         zoom 0.8
#     image ev lilly_hospitalclosed = hb_rescale_image("event/lilly_hospitalclosed.jpg")
#
#     image unlock_ev lilly_goodend:
#         hb_rescale_image("event/lilly_goodend.jpg")
#         zoom 0.8
#     image ev lilly_goodend = hb_rescale_image("event/lilly_goodend.jpg")
#     image evbg lilly_goodend = hb_rescale_image("event/lilly_goodend_bg.jpg")
#     image evfg lilly_goodend = hb_rescale_image("event/lilly_goodend_fg.png")
#
#
#     image evhunlock lilly_handjob_chest_frown_small:
#         hb_rescale_image("event/lilly_handjob/lilly_hcg_handjob_chest_frown.jpg")
#         zoom 0.667
#     image evhunlock lilly_handjob_chest_normal_small:
#         hb_rescale_image("event/lilly_handjob/lilly_hcg_handjob_chest_normal.jpg")
#         zoom 0.667
#
#     image evh lilly_handjob_chest_frown = hb_rescale_image("event/lilly_handjob/lilly_hcg_handjob_chest_frown.jpg")
#     image evh lilly_handjob_chest_normal = hb_rescale_image("event/lilly_handjob/lilly_hcg_handjob_chest_normal.jpg")
#     image evh lilly_handjob_stroke_normopen = hb_rescale_image("event/lilly_handjob/lilly_hcg_handjob_stroke_normopen.jpg")
#
#     image evh lilly_handjob_stroke_flustopen_small = hb_rescale_image("event/lilly_handjob/lilly_hcg_handjob_stroke_flustopen_small.jpg")
#     image evh lilly_handjob_stroke_normopen_small = hb_rescale_image("event/lilly_handjob/lilly_hcg_handjob_stroke_normopen_small.jpg")
#     image evh lilly_handjob_stroke_normshut_small = hb_rescale_image("event/lilly_handjob/lilly_hcg_handjob_stroke_normshut_small.jpg")
#
#     image evh lilly_cowgirl_cry_small = hb_rescale_image("event/lilly_cowgirl/lilly_hcg_cowgirl_cry_small.jpg")
#     image evh lilly_cowgirl_frown_small = hb_rescale_image("event/lilly_cowgirl/lilly_hcg_cowgirl_frown_small.jpg")
#     image evh lilly_cowgirl_smile_small = hb_rescale_image("event/lilly_cowgirl/lilly_hcg_cowgirl_smile_small.jpg")
#     image evh lilly_cowgirl_strain_small = hb_rescale_image("event/lilly_cowgirl/lilly_hcg_cowgirl_strain_small.jpg")
#     image evh lilly_cowgirl_weaksmile_small = hb_rescale_image("event/lilly_cowgirl/lilly_hcg_cowgirl_weaksmile_small.jpg")
#
#     image evh lilly_bath_emb_small = hb_rescale_image("event/lilly_bath/lilly_hcg_bath_emb_small.jpg")
#     image evh lilly_bath_grab_small = hb_rescale_image("event/lilly_bath/lilly_hcg_bath_grab_small.jpg")
#     image evh lilly_bath_moan_small = hb_rescale_image("event/lilly_bath/lilly_hcg_bath_moan_small.jpg")
#     image evh lilly_bath_open_small = hb_rescale_image("event/lilly_bath/lilly_hcg_bath_open_small.jpg")
#     image evh lilly_bath_smile_small = hb_rescale_image("event/lilly_bath/lilly_hcg_bath_smile_small.jpg")
#
#     image evh lilly_afterbath_open_small = hb_rescale_image("event/lilly_afterbath/lilly_hcg_afterbath_open_small.jpg")
#     image evh lilly_afterbath_shut_small = hb_rescale_image("event/lilly_afterbath/lilly_hcg_afterbath_shut_small.jpg")
#
#     image evh lilly_masturbate = hb_rescale_image("event/lilly_masturbate.jpg")
#     image evh lilly_masturbate_come = hb_rescale_image("event/lilly_masturbate_come.jpg")
#     image evh lilly_masturbate_come_face = hb_rescale_image("event/lilly_masturbate_come_face.jpg")
#
#
#
#     image ev hana_library_read = sunset(hb_rescale_image("event/hana_library_read.jpg"))
#     image ev hana_library = sunset(hb_rescale_image("event/hana_library.jpg"))
#     image ev hana_library_gasp = sunset(hb_rescale_image("event/hana_library_gasp.jpg"))
#     image ev hana_library_smile = sunset(hb_rescale_image("event/hana_library_smile.jpg"))
#
#     image ev hana_library_read_std = hb_rescale_image("event/hana_library_read.jpg")
#     image ev hana_library_std = hb_rescale_image("event/hana_library.jpg")
#     image ev hana_library_gasp_std = hb_rescale_image("event/hana_library_gasp.jpg")
#     image ev hana_library_smile_std = hb_rescale_image("event/hana_library_smile.jpg")
#
#     image ev hanako_presents1 = hb_rescale_image("event/hanako_presents1.jpg")
#     image ev hanako_presents2 = hb_rescale_image("event/hanako_presents2.jpg")
#
#     image evbg hanako_breakdown = hb_rescale_image("event/hanako_breakdown/hanako_breakdown_bg.jpg")
#     image evfg hanako_breakdown_down = hb_rescale_image("event/hanako_breakdown/hanako_breakdown_down.png")
#     image evfg hanako_breakdown_up = im.Composite(None,
#                                                   (0,0),hb_rescale_image("event/hanako_breakdown/hanako_breakdown_down.png"),
#                                                   (588,71),hb_rescale_image("event/hanako_breakdown/hanako_breakdown_up.jpg"))
#     image evfg hanako_breakdown_closed = im.Composite(None,
#                                                       (0,0),hb_rescale_image("event/hanako_breakdown/hanako_breakdown_down.png"),
#                                                       (588,71),hb_rescale_image("event/hanako_breakdown/hanako_breakdown_closed.jpg"))
#
#     image evul hanako_breakdown_down = im.Composite(None,
#                                                   (0,0), hb_rescale_image("event/hanako_breakdown/hanako_breakdown_bg.jpg"),
#                                                   (0,0),hb_rescale_image("event/hanako_breakdown/hanako_breakdown_down.png"))
#     image evul hanako_breakdown_up = im.Composite(None,
#                                                   (0,0), hb_rescale_image("event/hanako_breakdown/hanako_breakdown_bg.jpg"),
#                                                   (0,0),hb_rescale_image("event/hanako_breakdown/hanako_breakdown_down.png"),
#                                                   (588,71),hb_rescale_image("event/hanako_breakdown/hanako_breakdown_up.jpg"))
#     image evul hanako_breakdown_closed = im.Composite(None,
#                                                   (0,0), hb_rescale_image("event/hanako_breakdown/hanako_breakdown_bg.jpg"),
#                                                   (0,0),hb_rescale_image("event/hanako_breakdown/hanako_breakdown_down.png"),
#                                                   (588,71),hb_rescale_image("event/hanako_breakdown/hanako_breakdown_closed.jpg"))
#
#
#     image ev hanako_crayon1 = hb_rescale_image("event/hanako_crayon1.jpg")
#     image ev hanako_crayon2 = hb_rescale_image("event/hanako_crayon2.jpg")
#
#     image ev hanako_cry_open = hb_rescale_image("event/hanako_cry_open.jpg")
#     image ev hanako_cry_closed = hb_rescale_image("event/hanako_cry_closed.jpg")
#     image ev hanako_cry_away = hb_rescale_image("event/hanako_cry_away.jpg")
#
#     image ev hanako_cry_closed_fb = past(hb_rescale_image("event/hanako_cry_closed.jpg"))
#
#     image hanako_door_base = "vfx/hanako_door_base.jpg"
#     image hanako_door_door = "vfx/hanako_door_door.jpg"
#
#     image ev hanako_billiards_break = hb_rescale_image("event/hanako_billiards_break.jpg")
#
#     image ev hanako_billiards_distant:
#         hb_rescale_image("event/hanako_billiards_distant.jpg")
#         zoom 0.8
#     image ev hanako_billiards_distant_med:
#         hb_rescale_image("event/hanako_billiards_distant.jpg")
#         yanchor 0.0 ypos -0.1 xalign 1.0
#
#     image ev hanako_billiards_serious:
#         hb_rescale_image("event/hanako_billiards_serious.jpg")
#         zoom 0.8
#     image ev hanako_billiards_serious_med:
#         hb_rescale_image("event/hanako_billiards_serious.jpg")
#         yanchor 0.0 ypos -0.1 xalign 1.0
#
#     image ev hanako_billiards_smile:
#         hb_rescale_image("event/hanako_billiards_smile.jpg")
#         zoom 0.8
#     image ev hanako_billiards_smile_med:
#         hb_rescale_image("event/hanako_billiards_smile.jpg")
#         yanchor 0.0 ypos -0.1 xalign 1.0
#
#     image ev hanako_billiards_smile_close:
#         hb_rescale_image("event/hanako_billiards_smile_close.jpg")
#
#     image ev hanako_billiards_timid:
#         hb_rescale_image("event/hanako_billiards_timid.jpg")
#         zoom 0.8
#     image ev hanako_billiards_timid_med:
#         hb_rescale_image("event/hanako_billiards_timid.jpg")
#         yanchor 0.0 ypos -0.1 xalign 1.0
#
#     image evul hanako_emptyclassroom = im.FactorScale(im.Composite(None,
#                                                                  (0,0), hb_rescale_image("event/hanako_emptyclassroom_bg.jpg"),
#                                                                  (0,0), hb_rescale_image("event/hanako_emptyclassroom_fg.png")), 0.8)
#
#     image evbg hanako_emptyclassroom = hb_rescale_image("event/hanako_emptyclassroom_bg.jpg")
#     image evfg hanako_emptyclassroom = hb_rescale_image("event/hanako_emptyclassroom_fg.png")
#
#
#     image ev hanako_dolls = hb_rescale_image("event/hanako_dolls.jpg")
#
#     image ev hanako_rage = hb_rescale_image("event/hanako_rage.jpg")
#     image ev hanako_rage_sad = hb_rescale_image("event/hanako_rage_sad.jpg")
#
#     image ev hanako_eye = "vfx/hanako_eye.jpg"
#
#     image ev hisao_scar_large = hb_rescale_image("event/hisao_scar_large.jpg")
#     image ev hisao_scar = hb_rescale_image("event/hisao_scar.jpg")
#
#     image ev hanako_scars_large = hb_rescale_image("event/hanako_scars_large.jpg")
#     image ev hanako_scars = hb_rescale_image("event/hanako_scars.jpg")
#
#     image evh hanako_bed_boobs_blush = hb_rescale_image("event/hanako_bed_boobs_blush.jpg")
#     image evh hanako_bed_boobs_glance = hb_rescale_image("event/hanako_bed_boobs_glance.jpg")
#     image evh hanako_bed_crotch_blush = hb_rescale_image("event/hanako_bed_crotch_blush.jpg")
#     image evh hanako_bed_crotch_glance = hb_rescale_image("event/hanako_bed_crotch_glance.jpg")
#
#     image evh hanako_missionary_underwear = hb_rescale_image("event/hanako_missionary_underwear.jpg")
#     image evh hanako_missionary_open = hb_rescale_image("event/hanako_missionary_open.jpg")
#     image evh hanako_missionary_closed = hb_rescale_image("event/hanako_missionary_closed.jpg")
#     image evh hanako_missionary_clench = hb_rescale_image("event/hanako_missionary_clench.jpg")
#
#     image ev hanako_after_worry = hb_rescale_image("event/hanako_after_worry.jpg")
#     image ev hanako_after_smile = hb_rescale_image("event/hanako_after_smile.jpg")
#
#     image ev hanako_park_alone = hb_rescale_image("event/hanako_park_alone.jpg")
#     image ev hanako_park_away = hb_rescale_image("event/hanako_park_away.jpg")
#     image ev hanako_park_closed = hb_rescale_image("event/hanako_park_closed.jpg")
#     image ev hanako_park_look = hb_rescale_image("event/hanako_park_look.jpg")
#
#     image ev hanako_goodend_close = hb_rescale_image("event/hanako_goodend_close.jpg")
#     image ev hanako_goodend_muffin = hb_rescale_image("event/hanako_goodend_muffin.jpg")
#     image ev hanako_goodend = hb_rescale_image("event/hanako_goodend.jpg")
#
#     image unlock_ev hanako_goodend_close:
#         hb_rescale_image("event/hanako_goodend_close.jpg")
#         zoom 0.8
#     image unlock_ev hanako_goodend_muffin:
#         hb_rescale_image("event/hanako_goodend_muffin.jpg")
#         zoom 0.8
#     image unlock_ev hanako_goodend:
#         hb_rescale_image("event/hanako_goodend.jpg")
#         xalign 0.5 yalign 0.0
#         zoom 0.85
#
#
#     image ev shizu_shanghai = hb_rescale_image("event/shizu_shanghai.jpg")
#     image ev shizu_shanghai_boredlaugh = hb_rescale_image("event/shizu_shanghai_boredlaugh.jpg")
#     image ev shizu_shanghai_borednormal = hb_rescale_image("event/shizu_shanghai_borednormal.jpg")
#     image ev shizu_shanghai_normallaugh = hb_rescale_image("event/shizu_shanghai_normallaugh.jpg")
#     image ev shizu_shanghai_smirklaugh = hb_rescale_image("event/shizu_shanghai_smirklaugh.jpg")
#     image ev shizu_shanghai_smirknormal = hb_rescale_image("event/shizu_shanghai_smirknormal.jpg")
#
#
#     image ev shizuconfess_normal = hb_rescale_image("event/shizu_yukata/shizuconfess_normal.jpg")
#     image ev shizuconfess_smile = hb_rescale_image("event/shizu_yukata/shizuconfess_smile.jpg")
#     image ev shizuconfess_closed = hb_rescale_image("event/shizu_yukata/shizuconfess_closed.jpg")
#
#     python:
#         def zoomin_generic_f(trans, st, at):
#             trans.xalign = 0.5
#             trans.yalign = 0.5
#             n = scaled_runtime(20.0, at)
#             trans.zoom = 0.95 - (acdc_warp(n) * 0.15)
#             return 0
#
#     transform kenji_mg_out:
#         subpixel True
#         function zoomin_generic_f
#
#     image evbg kenji_glasses = hb_rescale_image("event/kenji_glasses/kenji_glasses_bg.jpg")
#     image evmg kenji_glasses_normal = hb_rescale_image("event/kenji_glasses/kenji_glasses_mg.png")
#     image evmg kenji_glasses_frown = im.Composite(None,
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_mg.png"),
#                                           (400,190), hb_rescale_image("event/kenji_glasses/kenji_glasses_frown.png"))
#     image evmg kenji_glasses_closed = im.Composite(None,
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_mg.png"),
#                                           (400,190), hb_rescale_image("event/kenji_glasses/kenji_glasses_closed.png"))
#     image evfg kenji_glasses = hb_rescale_image("event/kenji_glasses/kenji_glasses_fg.png")
#
#
#     image evul kenji_glasses_normal = im.FactorScale(im.Composite(None,
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_bg.jpg"),
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_mg.png"),
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_fg.png")),0.8)
#     image evul kenji_glasses_frown = im.FactorScale(im.Composite(None,
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_bg.jpg"),
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_mg.png"),
#                                           (400,190), hb_rescale_image("event/kenji_glasses/kenji_glasses_frown.png"),
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_fg.png")),0.8)
#     image evul kenji_glasses_closed = im.FactorScale(im.Composite(None,
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_bg.jpg"),
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_mg.png"),
#                                           (400,190), hb_rescale_image("event/kenji_glasses/kenji_glasses_closed.png"),
#                                           (0,0), hb_rescale_image("event/kenji_glasses/kenji_glasses_fg.png")),0.8)
#
#
#     image ev shizutanabata = hb_rescale_image("event/shizu_yukata/shizutanabata.jpg")
#
#     image ev shizu_flashback = hb_rescale_image("event/shizu_flashback.jpg")
#
#     image ev shizu_hands = hb_rescale_image("event/shizu_hands.jpg")
#
#     image ev shizune_car:
#         hb_rescale_image("event/shizune_car.jpg")
#         subpixel True yalign 0.5 xalign 0.0
#         easein 12.0 xalign 1.0
#
#     image ev shizu_fishing_sl = hb_rescale_image("event/shizu_fishing_sl.jpg")
#     image ev shizu_fishing_ah = hb_rescale_image("event/shizu_fishing_ah.jpg")
#
#     image ev shizu_couch = hb_rescale_image("event/shizu_couch.jpg")
#
#     python:
#         def shizu_roof_in_f(trans, st, at):
#             trans.xalign = 0.5
#             trans.yalign = 1.0
#             n = scaled_runtime(60.0, at)
#             trans.zoom = 0.8 + (acdc_warp(n) * 0.2)
#             return 0
#
#     transform shizu_roof_in:
#         subpixel True
#         function shizu_roof_in_f
#
#     image ev shizu_roof = hb_rescale_image("event/shizu_roof/shizu_roof.jpg")
#     image ev shizu_roof_towardsangry = im.Composite(None,
#                                             (0,0),  hb_rescale_image("event/shizu_roof/shizu_roof.jpg"),
#                                             (299,296), hb_rescale_image("event/shizu_roof/shizu_roof_towardsangry.jpg"))
#     image ev shizu_roof_towardsnormal = im.Composite(None,
#                                             (0,0),  hb_rescale_image("event/shizu_roof/shizu_roof.jpg"),
#                                             (299,296), hb_rescale_image("event/shizu_roof/shizu_roof_towardsnormal.jpg"))
#     image ev shizu_roof_smile = im.Composite(None,
#                                             (0,0),  hb_rescale_image("event/shizu_roof/shizu_roof.jpg"),
#                                             (299,296), hb_rescale_image("event/shizu_roof/shizu_roof_smile.jpg"))
#
#     image ev shizu_roof2 = im.Composite(None,
#                                             (0,0),  hb_rescale_image("event/shizu_roof/shizu_roof.jpg"),
#                                             (601,316), hb_rescale_image("event/shizu_roof/shizu_roof_hisao2.jpg"))
#     image ev shizu_roof2_towardsangry = im.Composite(None,
#                                             (0,0),  hb_rescale_image("event/shizu_roof/shizu_roof.jpg"),
#                                             (601,316), hb_rescale_image("event/shizu_roof/shizu_roof_hisao2.jpg"),
#                                             (299,296), hb_rescale_image("event/shizu_roof/shizu_roof_towardsangry.jpg"))
#     image ev shizu_roof2_towardsnormal = im.Composite(None,
#                                             (0,0),  hb_rescale_image("event/shizu_roof/shizu_roof.jpg"),
#                                             (601,316), hb_rescale_image("event/shizu_roof/shizu_roof_hisao2.jpg"),
#                                             (299,296), hb_rescale_image("event/shizu_roof/shizu_roof_towardsnormal.jpg"))
#     image ev shizu_roof2_smile = im.Composite(None,
#                                             (0,0),  hb_rescale_image("event/shizu_roof/shizu_roof.jpg"),
#                                             (601,316), hb_rescale_image("event/shizu_roof/shizu_roof_hisao2.jpg"),
#                                             (299,296), hb_rescale_image("event/shizu_roof/shizu_roof_smile.jpg"))
#
#     image evh shizune_hcg_tied_blush_small = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_blush_small.jpg")
#     image evh shizune_hcg_tied_blush = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_blush.jpg")
#     image evh shizune_hcg_tied_close_small = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_close_small.jpg")
#     image evh shizune_hcg_tied_close = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_close.jpg")
#     image evh shizune_hcg_tied_kinky1_small = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_kinky1_small.jpg")
#
#     image evh shizune_hcg_tied_kinky2_small = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_kinky2_small.jpg")
#     image evh shizune_hcg_tied_kinky2 = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_kinky2.jpg")
#     image evh shizune_hcg_tied_kinky3_small = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_kinky3_small.jpg")
#
#     image evh shizune_hcg_tied_smile_small = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_smile_small.jpg")
#
#     image evh shizune_hcg_tied_stare_small = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_stare_small.jpg")
#     image evh shizune_hcg_tied_stare = hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_stare.jpg")
#
#     image evh_hi shizune_hcg_tied_hisao2 = im.Composite((1600,1200),
#                                                         (1080,290),hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_hisao2.png"))
#     image evh_hi shizune_hcg_tied_hisao2_small = im.Composite((800,600),
#                                                               (540,145),hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_hisao2_small.png"))
#
#
#     image evhul shizune_hcg_tied_hisao2_small = im.Composite((800,600),
#                                                              (0,0), hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_kinky3_small.jpg"),
#                                                              (540,145),hb_rescale_image("event/shizune_hcg_tied/shizune_hcg_tied_hisao2_small.png"))
#
#
#     image evh shizu_undressing_clothed_stare = hb_rescale_image("event/shizu_undressing/shizu_undressing_clothed_stare.jpg")
#     image evh shizu_undressing_clothed_kiss = hb_rescale_image("event/shizu_undressing/shizu_undressing_clothed_kiss.jpg")
#     image evh shizu_undressing_clothed_blush = hb_rescale_image("event/shizu_undressing/shizu_undressing_clothed_blush.jpg")
#     image evh shizu_undressing_unclothed_blush = hb_rescale_image("event/shizu_undressing/shizu_undressing_unclothed_blush.jpg")
#     image evh shizu_undressing_unclothed_closed = hb_rescale_image("event/shizu_undressing/shizu_undressing_unclothed_closed.jpg")
#     image evh shizu_undressing_unclothed_kiss = hb_rescale_image("event/shizu_undressing/shizu_undressing_unclothed_kiss.jpg")
#     image evh shizu_undressing_unclothed_talk = hb_rescale_image("event/shizu_undressing/shizu_undressing_unclothed_talk.jpg")
#
#     image evh shizu_pushdown = hb_rescale_image("event/shizu_pushdown.jpg")
#
#     image evh shizu_straddle_open:
#         hb_rescale_image("event/shizu_straddle_open.jpg")
#         xalign 0.7 yalign 1.0 subpixel True
#         easein 16.0 xalign 0.0 yalign 0.0
#     image evh shizu_straddle_tease = hb_rescale_image("event/shizu_straddle_tease.jpg")
#     image evh shizu_straddle_closed = hb_rescale_image("event/shizu_straddle_closed.jpg")
#     image evh shizu_straddle_smile = hb_rescale_image("event/shizu_straddle_smile.jpg")
#     image evh shizu_straddle_come = hb_rescale_image("event/shizu_straddle_come.jpg")
#
#     image evh shizu_table_smile = hb_rescale_image("event/shizu_table_smile.jpg")
#     image evh shizu_table_normal = hb_rescale_image("event/shizu_table_normal.jpg")
#     image evh shizu_table_comeopen = hb_rescale_image("event/shizu_table_comeopen.jpg")
#     image evh shizu_table_comeclosed = hb_rescale_image("event/shizu_table_comeclosed.jpg")
#
#     image ev misha_sad = hb_rescale_image("event/misha_sad.jpg")
#
#     image ev misha_nightclass = hb_rescale_image("event/misha_nightclass.jpg")
#     image ovl misha_nightclass_aperture = "vfx/misha_nightclass_aperture.png"
#
#     image evh misha_naked:
#         hb_rescale_image("event/misha_naked.jpg")
#         xalign 0.0 yalign 0.0
#     image evh misha_sex_aside = hb_rescale_image("event/misha_sex_aside.jpg")
#     image evh misha_sex_closed = hb_rescale_image("event/misha_sex_closed.jpg")
#
#     image ev misha_roof_normal:
#         hb_rescale_image("event/misha_roof_normal.jpg")
#         xalign 0.5 yalign 0.0
#     image ev misha_roof_angry = hb_rescale_image("event/misha_roof_angry.jpg")
#     image ev misha_roof_closed = hb_rescale_image("event/misha_roof_closed.jpg")
#     image ev misha_roof_sad = hb_rescale_image("event/misha_roof_sad.jpg")
#
#     image aoi_keiko = sunset("vfx/aoi_keiko.png")
#
#     image ev shizu_goodend:
#         hb_rescale_image("event/shizu_goodend.jpg")
#         xalign 0.5 yalign 1.0 subpixel True
#
#     image ev shizu_goodend_pan:
#         hb_rescale_image("event/shizu_goodend.jpg")
#         xalign 0.5 yalign 1.0 subpixel True
#         0.5
#         acdc_warp 15.0 yalign 0.0
#
#     image ev shizu_badend = hb_rescale_image("event/shizu_badend.jpg")
#
#
#     image ev showdown = hb_rescale_image("event/lilly_shizu_showdown.jpg")
#     image ev showdown_large = hb_rescale_image("event/lilly_shizu_showdown_large.jpg")
#     image ev showdown_lilly = im.Crop(hb_rescale_image("event/lilly_shizu_showdown_large.jpg"), 280,100,800,600)
#     image ev showdown_shizu = im.Crop(hb_rescale_image("event/lilly_shizu_showdown_large.jpg"), 1400,160,800,600)
#
#     image showdown_lilly_slice = im.Crop(hb_rescale_image("event/lilly_shizu_showdown_large.jpg"), 440,260,800,299)
#     image showdown_shizu_slice = im.Crop(hb_rescale_image("event/lilly_shizu_showdown_large.jpg"), 1360,320,800,299)
#
#
#     image ev kenji_rooftop = hb_rescale_image("event/kenji_rooftop.jpg")
#     image ev kenji_rooftop_large = hb_rescale_image("event/kenji_rooftop_large.jpg")
#     image ev kenji_rooftop_kenji = im.Crop(hb_rescale_image("event/kenji_rooftop_large.jpg"), 288,376,800,600)
#
#
#
#
#     $ ex_g_images = (("thumb/other_iwanako.jpg", "evul other_iwanako"),
#                      ("thumb/hisao_class.jpg","ev hisao_class_start","ev hisao_class_move","ev hisao_class_end"),
#                      ("thumb/kenji_rooftop.jpg","ev kenji_rooftop"),
#                      ("thumb/hisao_teacup.jpg","evul hisao_teacup"),
#                      ("thumb/hisao_letter.jpg","ev hisao_letter_closed", "ev hisao_letter_open", "ev hisao_letter_open_2"),
#                      ("thumb/akira_park.jpg","evul akira_park"),
#                      ("thumb/emi_knockeddown.jpg","ev emi_knockeddown"),
#                      ("thumb/emi_run_face.jpg","ev emi_run_face"),
#                      ("thumb/emitrack_blocks.jpg", "ev emitrack_blocks", "ev emitrack_blocks_close", "ev emitrack_blocks_close_grin"),
#                      ("thumb/emitrack_running.jpg", "ev emitrack_running"),
#                      ("thumb/emitrack_finishtop.jpg","ev emitrack_finishtop"),
#                      ("thumb/emitrack_finish.jpg","ev emitrack_finish"),
#                      ("thumb/picnic.jpg", "ev picnic_normal", "ev picnic_rain"),
#                      ("thumb/emi_sleep.jpg","ev emi_sleep_unsure", "ev emi_sleep_normal", "ev emi_sleep_weep", "ev emi_sleep_cry"),
#                      ("thumb/emi_sleepy.jpg","ev emi_sleepy", "ev emi_sleepy_face", "ev emi_sleepy_legs"),
#                      ("thumb/emi_firstkiss.jpg","ev emi_firstkiss"),
#                      ("thumb/emi_bed.jpg","ev emi_bed_normal", "ev emi_bed_smile", "ev emi_bed_happy", "ev emi_bed_unsure", "ev emi_bed_frown", "ev emi_parkback", "ev emi_parkback_frown"),
#                      ("thumb/emi_forehead.jpg","ev emi_forehead"),
#                      ("thumb/emi_grinding.jpg","evh emi_grinding_victory", "evh emi_grinding_wink", "evh emi_grinding_grin", "evh emi_grinding_half_undress", "evh emi_grinding_half_grin", "evh emi_grinding_off_yawn", "evh emi_grinding_off_closesurprise", "evh emi_grinding_off_closearoused", "evh emi_grinding_off_aroused", "evh emi_grinding_off_arousedclosed", "evh emi_grinding_off_come", "evh emi_grinding_off_end"),
#                      ("thumb/emi_shed.jpg","evh emi_shed_base1","evh emi_shed_base3","evh emi_shed_base3","evh emi_shed_base4",),
#                      ("thumb/emi_grave.jpg","ev emi_grave"),
#                      ("thumb/emi_cry_down.jpg","evul emi_cry_down"),
#                      ("thumb/emi_miss.jpg","evh emi_miss_closed", "evh emi_miss_open"),
#                      ("thumb/emi_ending.jpg","ev emi_ending_smile", "ev emi_ending_serious", "ev emi_ending_glad"),
#                      ("thumb/hana_library.jpg","ev hana_library","ev hana_library_read","ev hana_library_gasp"),
#                      ("thumb/hanako_shanghaiwindow.jpg","ev hanako_shanghaiwindow"),
#                      ("ev hanako_presents1", "ev hanako_presents2"),
#                      ("thumb/hanako_crayon.jpg","ev hanako_crayon1", "ev hanako_crayon2"),
#                      ("thumb/hanako_breakdown.jpg","evul hanako_breakdown_down", "evul hanako_breakdown_up", "evul hanako_breakdown_closed"),
#                      ("thumb/hanako_cry.jpg","ev hanako_cry_closed", "ev hanako_cry_open", "ev hanako_cry_away"),
#                      ("thumb/hanako_billiards.jpg", "ev hanako_billiards_distant", "ev hanako_billiards_serious", "ev hanako_billiards_timid","ev hanako_billiards_smile","ev hanako_billiards_smile_close"),
#                      ("thumb/hanako_emptyclassroom.jpg", "evul hanako_emptyclassroom"),
#                      ("thumb/hanako_rage.jpg","ev hanako_rage", "ev hanako_rage_sad"),
#                      ("thumb/hisao_scar.jpg","ev hisao_scar"),
#                      ("thumb/hanako_scars.jpg","ev hanako_scars"),
#                      ("thumb/hanako_bed.jpg","evh hanako_bed_boobs_blush", "evh hanako_bed_boobs_glance", "evh hanako_bed_crotch_blush", "evh hanako_bed_crotch_glance"),
#                      ("thumb/hanako_missionary.jpg","evh hanako_missionary_underwear", "evh hanako_missionary_open", "evh hanako_missionary_closed", "evh hanako_missionary_clench"),
#                      ("thumb/hanako_after.jpg","ev hanako_after_worry","ev hanako_after_smile"),
#                      ("thumb/hanako_park.jpg","ev hanako_park_alone","ev hanako_park_away","ev hanako_park_look","ev hanako_park_closed"),
#                      ("thumb/hanako_goodend.jpg","unlock_ev hanako_goodend_close","unlock_ev hanako_goodend","unlock_ev hanako_goodend_muffin"),
#                      ("thumb/lilly_tearoom.jpg","ev lilly_tearoom", "ev lilly_tearoom_open"),
#                      ("thumb/lilly_touch.jpg","ev lilly_touch_uni", "ev lilly_touch_cheong", "ev lilly_touch_cas"),
#                      ("thumb/lilly_crane.jpg","ev lilly_crane"),
#                      ("thumb/lilly_bedroom.jpg","ev lilly_bedroom"),
#                      ("thumb/lilly_hanako_hug.jpg","unlock_ev lilly_hanako_hug_end"),
#                      ("thumb/lilly_sleeping.jpg","ev lilly_sleeping", "ev lilly_sleeping_smile"),
#                      ("thumb/lilly_trainride.jpg","ev lilly_trainride","ev lilly_trainride_smiles","ev lilly_trainride_ni"),
#                      ("thumb/lilly_wheat.jpg","unlock_ev lilly_wheat_close","ev lilly_wheat_small"),
#                      ("thumb/lilly_handjob.jpg","evhunlock lilly_handjob_chest_frown_small","evhunlock lilly_handjob_chest_normal_small","evh lilly_handjob_stroke_flustopen_small","evh lilly_handjob_stroke_normopen_small","evh lilly_handjob_stroke_normshut_small"),
#                      ("thumb/lilly_cowgirl.jpg","evh lilly_cowgirl_cry_small", "evh lilly_cowgirl_frown_small", "evh lilly_cowgirl_smile_small", "evh lilly_cowgirl_strain_small", "evh lilly_cowgirl_weaksmile_small"),
#                      ("thumb/lilly_bath.jpg","evh lilly_bath_emb_small", "evh lilly_bath_grab_small", "evh lilly_bath_moan_small", "evh lilly_bath_open_small", "evh lilly_bath_smile_small"),
#                      ("thumb/lilly_afterbath.jpg","evh lilly_afterbath_open_small", "evh lilly_afterbath_shut_small"),
#                      ("thumb/lilly_kissing.jpg","ev lilly_kissing"),
#                      ("thumb/lilly_masturbate.jpg","evh lilly_masturbate", "evh lilly_masturbate_come", "evh lilly_masturbate_come_face"),
#                      ("thumb/lilly_restaurant.jpg","evul lilly_restaurant_listen", "evul lilly_restaurant_sheepish", "ev lilly_restaurant_eat", "ev lilly_restaurant_chew", "ev lilly_restaurant_wine"),
#                      ("thumb/lilly_sheets.jpg","ev lilly_sheets"),
#                      ("thumb/lilly_airport.jpg","ev lilly_airport", "ev lilly_airport_end"),
#                      ("thumb/lilly_hospitalwindow.jpg","ev lilly_hospitalwindow"),
#                      ("thumb/lilly_hospital.jpg", "unlock_ev lilly_hospitalclosed", "unlock_ev lilly_hospital"),
#                      ("thumb/lilly_goodend.jpg", "unlock_ev lilly_goodend"),
#                      ("thumb/rin_eating.jpg","ev rin_eating"),
#                      ("thumb/rin_artclass.jpg", "ev rin_artclass1", "ev rin_artclass2", "ev rin_artclass3", "ev rin_artclass4"),
#                      ("thumb/hisao_mirror.jpg","ev hisao_mirror_800"),
#                      ("thumb/rin_painting.jpg","ev rin_painting_base", "ev rin_painting_foot", "ev rin_painting_faceconcerned", "ev rin_painting_concerned", "ev rin_painting_reply"),
#                      ("thumb/rin_rain.jpg","ev rin_rain_away", "ev rin_rain_towards"),
#                      ("thumb/rin_high.jpg","ev rin_high_frown", "ev rin_high_grin", "ev rin_high_grinwide", "ev rin_high_oneeye", "ev rin_high_open", "ev rin_high_smile", "ev rin_high_sleep"),
#                      ("thumb/rin_kiss.jpg","ev rin_kiss"),
#                      ("thumb/rin_nap.jpg","ev rin_nap_total", "ev rin_nap_total_awind", "ev rin_nap_close_awind", "ev rin_nap_close_awind_tears"),
#                      ("thumb/rin_wisp.jpg", "ev rin_wisp1", "ev rin_wisp2", "ev rin_wisp3", "ev rin_wisp4", "ev rin_wisp5"),
#                      ("thumb/rin_galleryskylight.jpg","ovl rin_galleryskylight"),
#                      ("thumb/rin_orange.jpg","ev rin_orange", "ev rin_orange_large"),
#                      ("thumb/rin_masturbate.jpg","ev rin_masturbate_away","ev rin_masturbate_surprise", "ev rin_masturbate_frown", "ev rin_masturbate_doubt", "ev rin_masturbate_hug"),
#                      ("thumb/rin_relief.jpg","evh rin_relief_down", "evh rin_relief_up"),
#                      ("thumb/rin_gallery.jpg", "ev rin_gallery"),
#                      ("thumb/rin_trueend.jpg","ev rin_trueend_normal", "ev rin_trueend_smile", "ev rin_trueend_weaksmile", "ev rin_trueend_sad", "ev rin_trueend_closed", "ev rin_trueend_hug", "ev rin_trueend_hugclosed", "ev rin_trueend_gone"),
#                      ("thumb/rin_wet.jpg", "ev rin_wet_pan_down", "ev rin_wet_arms", "ev rin_wet_face_up", "ev rin_wet_face_down", "ev rin_wet_towel_up", "ev rin_wet_towel_down", "ev rin_wet_towel_touch"),
#                      ("thumb/rin_h2.jpg","unlock_evh rin_h2_pan_surprise", "unlock_evh rin_h2_pan_away", "unlock_evh rin_h2_pan_closed", "evh rin_h2_pan_closed", "evh rin_h2_nopan_closed", "evh rin_h2_hisao_closed"),
#                      ("thumb/rin_pair.jpg","ev rin_pair_base_clothes","ev rin_pair_base"),
#                      ("thumb/rin_h.jpg","evh rin_h_closed","evh rin_h_left","evh rin_h_normal","evh rin_h_right","evh rin_h_strain","evh rin_h_closed_close","evh rin_h_left_close","evh rin_h_normal_close","evh rin_h_right_close","evh rin_h_strain_close"),
#                      ("thumb/rin_goodend.jpg","ev rin_goodend_1","ev rin_goodend_1b","ev rin_goodend_2"),
#                      ("thumb/shizu_shanghai.jpg","ev shizu_shanghai", "ev shizu_shanghai_borednormal", "ev shizu_shanghai_smirknormal", "ev shizu_shanghai_smirklaugh"),
#                      ("thumb/showdown.jpg","ev showdown"),
#                      ("thumb/shizu_chess.jpg","ev shizu_chess_base", "ev shizu_chess_base3", "ev shizu_chess_base2"),
#                      ("thumb/kenji_glasses.jpg", "evul kenji_glasses_closed", "evul kenji_glasses_frown", "evul kenji_glasses_normal"),
#                      ("thumb/shizu_tanabata.jpg","ev shizutanabata"),
#                      ("thumb/shizu_confess.jpg","ev shizuconfess_normal", "ev shizuconfess_closed", "ev shizuconfess_smile"),
#                      ("thumb/shizu_hands.jpg","ev shizu_hands"),
#                      ("thumb/shizu_couch.jpg","ev shizu_couch"),
#                      ("thumb/shizune_car.jpg", "ev shizune_car"),
#                      ("thumb/shizu_fishing.jpg","ev shizu_fishing_ah", "ev shizu_fishing_sl"),
#                      ("thumb/shizune_tied.jpg","evh shizune_hcg_tied_blush_small", "evh shizune_hcg_tied_smile_small", "evh shizune_hcg_tied_stare_small", "evh shizune_hcg_tied_close_small", "evh shizune_hcg_tied_kinky1_small", "evh shizune_hcg_tied_kinky2_small", "evh shizune_hcg_tied_kinky3_small", "evhul shizune_hcg_tied_hisao2_small"),
#                      ("thumb/misha_sad.jpg","ev misha_sad"),
#                      ("thumb/misha_naked.jpg", "evh misha_naked"),
#                      ("thumb/misha_sex.jpg","evh misha_sex_aside", "evh misha_sex_closed"),
#                      ("thumb/misha_roof.jpg", "ev misha_roof_closed","ev misha_roof_angry","ev misha_roof_normal","ev misha_roof_sad"),
#                      ("thumb/shizu_roof.jpg", "ev shizu_roof","ev shizu_roof_smile", "ev shizu_roof_towardsnormal", "ev shizu_roof_towardsangry", "ev shizu_roof2_towardsangry"),
#                      ("thumb/shizu_flashback.jpg","ev shizu_flashback"),
#                      ("thumb/shizu_undressing.jpg","evh shizu_undressing_clothed_stare", "evh shizu_undressing_clothed_kiss", "evh shizu_undressing_clothed_blush", "evh shizu_undressing_unclothed_blush", "evh shizu_undressing_unclothed_closed", "evh shizu_undressing_unclothed_kiss", "evh shizu_undressing_unclothed_talk"),
#                      ("thumb/shizu_pushdown.jpg","evh shizu_pushdown"),
#                      ("thumb/shizu_straddle.jpg","evh shizu_straddle_open", "evh shizu_straddle_tease", "evh shizu_straddle_closed","evh shizu_straddle_smile","evh shizu_straddle_come"),
#                      ("thumb/shizu_table.jpg","evh shizu_table_smile","evh shizu_table_normal", "evh shizu_table_comeopen", "evh shizu_table_comeclosed"),
#                      ("thumb/misha_nightclass.jpg","ev misha_nightclass"),
#                      ("thumb/shizu_badend.jpg","ev shizu_badend"),
#                      ("thumb/shizu_goodend.jpg", "ev shizu_goodend", "ev shizu_goodend_pan"),
#                      ("thumb/cutin.png","pills","stuffedcat","teaset","shangpai", "wine", "musicbox closed", "musicbox open", "hanaphone", "phonestrap", "hanaphonestrap", "insert startpistol","invite", "sc_comp", "brailler", "chessboard", "kenjibox", "jigorocard", "letter_insert", "letter_open_insert", "letter_open_insert_2","stallphoto_insert"),
#                      ("thumb/completionbonus.jpg","completionbonus"))
#
#
#
#
#
#
#     image bg tearoom_lillyhisao_noon = hb_rescale_image("event/Lilly_supercg/tearoom_lillyhisao_noon.jpg")
#     image bg tearoom_lillyhisao_sunset = hb_rescale_image("event/Lilly_supercg/tearoom_lillyhisao_sunset.jpg")
#
#     image tearoom_hisao calm = hb_rescale_image("event/Lilly_supercg/tearoom_hisao_calm.png")
#     image tearoom_hisao oops = hb_rescale_image("event/Lilly_supercg/tearoom_hisao_oops.png")
#     image tearoom_hisao outside = hb_rescale_image("event/Lilly_supercg/tearoom_hisao_outside.png")
#     image tearoom_hisao relief = hb_rescale_image("event/Lilly_supercg/tearoom_hisao_relief.png")
#     image tearoom_hisao sigh = hb_rescale_image("event/Lilly_supercg/tearoom_hisao_sigh.png")
#     image tearoom_hisao smile = hb_rescale_image("event/Lilly_supercg/tearoom_hisao_smile.png")
#     image tearoom_hisao think = hb_rescale_image("event/Lilly_supercg/tearoom_hisao_think.png")
#     image tearoom_hisao thinkclosed = hb_rescale_image("event/Lilly_supercg/tearoom_hisao_thinkclosed.png")
#     image tearoom_hisao unsure = hb_rescale_image("event/Lilly_supercg/tearoom_hisao_unsure.png")
#
#     image tearoom_lilly ara = hb_rescale_image("event/Lilly_supercg/tearoom_lilly_ara.png")
#     image tearoom_lilly giggle = hb_rescale_image("event/Lilly_supercg/tearoom_lilly_giggle.png")
#     image tearoom_lilly smileclosed = hb_rescale_image("event/Lilly_supercg/tearoom_lilly_smileclosed.png")
#     image tearoom_lilly thinking = hb_rescale_image("event/Lilly_supercg/tearoom_lilly_thinking.png")
#     image tearoom_lilly weaksmile = hb_rescale_image("event/Lilly_supercg/tearoom_lilly_weaksmile.png")
#
#     image bg tearoom_everyone_noon = hb_rescale_image("event/Lilly_supercg/tearoom_everyone_noon.jpg")
#
#     image tearoom_hanae happy = hb_rescale_image("event/Lilly_supercg/tearoom_hanae_happy.png")
#     image tearoom_hanae open = hb_rescale_image("event/Lilly_supercg/tearoom_hanae_open.png")
#     image tearoom_hanae sad = hb_rescale_image("event/Lilly_supercg/tearoom_hanae_sad.png")
#     image tearoom_hanae shy = hb_rescale_image("event/Lilly_supercg/tearoom_hanae_shy.png")
#
#     image tearoom_lillye ara = hb_rescale_image("event/Lilly_supercg/tearoom_lillye_ara.png")
#     image tearoom_lillye giggle = hb_rescale_image("event/Lilly_supercg/tearoom_lillye_giggle.png")
#     image tearoom_lillye smileclosed = hb_rescale_image("event/Lilly_supercg/tearoom_lillye_smileclosed.png")
#     image tearoom_lillye thinking = hb_rescale_image("event/Lilly_supercg/tearoom_lillye_thinking.png")
#     image tearoom_lillye weaksmile = hb_rescale_image("event/Lilly_supercg/tearoom_lillye_weaksmile.png")
#     image tearoom_lillye smile = hb_rescale_image("event/Lilly_supercg/tearoom_lillye_smile.png")
#
#     image tearoom_hisaoe calm = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_calm.png")
#     image tearoom_hisaoe outside = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_outside.png")
#     image tearoom_hisaoe sigh = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_sigh.png")
#     image tearoom_hisaoe thinkclosed = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_thinkclosed.png")
#     image tearoom_hisaoe hoops = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_hoops.png")
#     image tearoom_hisaoe hrelief = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_hrelief.png")
#     image tearoom_hisaoe hsmile = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_hsmile.png")
#     image tearoom_hisaoe hthink = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_hthink.png")
#     image tearoom_hisaoe hunsure = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_hunsure.png")
#     image tearoom_hisaoe loops = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_loops.png")
#     image tearoom_hisaoe relief = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_relief.png")
#     image tearoom_hisaoe lsmile = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_lsmile.png")
#     image tearoom_hisaoe lthink = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_lthink.png")
#     image tearoom_hisaoe lunsure = hb_rescale_image("event/Lilly_supercg/tearoom_hisaoe_lunsure.png")
#
#     image tearoom_chess = hb_rescale_image("event/Lilly_supercg/tearoom_chess.png")
#
#
#
#     image ev shizu_chess_large = hb_rescale_image("event/shizu_supercg/shizu_chess_large.jpg")
#     image ev shizu_chess_base = hb_rescale_image("event/shizu_supercg/shizu_chess_base.jpg")
#     image ev shizu_chess_base2 = hb_rescale_image("event/shizu_supercg/shizu_chess_base2.jpg")
#     image ev shizu_chess_base3 = hb_rescale_image("event/shizu_supercg/shizu_chess_base3.jpg")
#
#     image sc_shiz normal = im.Crop(hb_rescale_image("event/shizu_supercg/shizu_chess_base3.jpg"), 400, 0, 400, 600)
#
#
#
#
#
#
#     python:
#         def silhouette(disp, color=0):
#             return im.Map(disp, rmap=im.ramp(color, color), gmap=im.ramp(color, color), bmap=im.ramp(color, color))
#
#     image kenji silhouette = silhouette("sprites/kenji/kenji_neutral.png")
#     image kenji silhouette_naked = silhouette("sprites/kenji/kenji_neutral_naked.png",10)
#     image hanako silhouette = silhouette("sprites/hanako/hanako_basic_bashful.png")
#     image rin silhouette = silhouette("sprites/rin/rin_relaxed_surprised.png")
#
#     python:
#         shizuepiccomp = im.Composite ((874,836),
#                                   (0,0),sp_night("sprites/shizu/close/shizu_out_serious_close.png"),
#                                   (2.5,600),sp_night("vfx/shizu_out_serious_legs.png"))
#
#         shizuepiccomp_sil = im.Composite ((874,836),
#                                   (0,0),silhouette("sprites/shizu/close/shizu_out_serious_close.png"),
#                                   (2.5,600),silhouette("vfx/shizu_out_serious_legs.png"))
#
#     image shizu epictransition:
#         zoom 1.0 xalign 0.5 yalign 0.0
#         parallel:
#             shizuepiccomp
#             pause 0.2
#             shizuepiccomp_sil with Dissolve(1.8, alpha=True)
#         parallel:
#             ease 2.0 zoom 0.1 ypos 0.665
#
#     image ksgallerybg = "ui/tc-neutral.png"
#
#     image bloodred = Solid("#d00")
#     image white = Solid("#fff")
#     image pink = Solid("#FF7FD4")
#     image videowhite = Solid("#FFFFFF")
#     image videoblack = Solid("#0d0d0d")
#     image darkgrey = Solid("#0D0D0D")
#     image bg school_roof_ni_crop = im.Crop("bgs/school_roof_ni.jpg",200,0,800,600)
#     image n_vignette = "vfx/narrowvignette.png"
#
#     image fw_screen = Solid("#000000CC")
#
#     image fakenvltextbox = "ui/bg-nvl.png"
#
#     image hisaowindow = "vfx/hisaowindow.png"
#
#     image alivetext = renpy.ParameterizedText(yalign=0.5, xanchor=0, xpos = 130, style="alive_text", drop_shadow=None, color="#666666")
#     image alivetext_j = renpy.ParameterizedText(yalign=0.5, xanchor=0, xpos = 70, style="alive_text", drop_shadow=None, color="#666666")
#
#     image bg mural_start = "vfx/mural_start.jpg"
#     image bg mural_unfinished = "vfx/mural_unfinished.jpg"
#     image bg mural_part = At("vfx/mural.jpg", Transform(xalign=0.0))
#     image mural all = "vfx/mural_wide.jpg"
#     image bg mural = "vfx/mural.jpg"
#     image bg mural_ss = sunset("vfx/mural.jpg")
#     image mural pan = At("vfx/mural.jpg",Fullpan(60.0, dir="left"))
#
#     image rin_exhibition_paintings = At("vfx/rin_exhibition_paintings.jpg",Fullpan(40.0, dir="right"))
#     image rin_exhibition_sold = "vfx/rin_exhibition_sold.jpg"
#     image rin_exhibition_c = "vfx/rin_exhibition_c.jpg"
#
#     transform shadowalpha:
#         alpha 0.7
#
#     image rin_shadow basic = At(silhouette("sprites/rin/close/rin_basic_deadpan_close.png"), shadowalpha)
#     image rin_shadow negative = At(silhouette("sprites/rin/close/rin_negative_spaciness_close.png"), shadowalpha)
#
#     image nightsky rotation:
#         "bgs/misc_sky_ni.jpg"
#         xalign 0.5 yalign 0.5 rotate 0 zoom 1.5 alpha 0.0
#         parallel:
#             linear 40 rotate 360
#             repeat
#         parallel:
#             linear 10 zoom 3.0
#         parallel:
#             linear 5.0 alpha 1.0
#
#     image cityscape zoom:
#         "vfx/cityscape.png"
#         xpos -0.25 ypos 1.0 xanchor 0.0 yanchor 0.0 zoom 1.5
#         ease 2.0 xpos 0.0 ypos 1.0 xanchor 0.0 yanchor 1.0 zoom 1.0
#
#     image hill enter:
#         "vfx/hillouette.png"
#         xpos 0.5 ypos 1.0 xanchor 0.5 yanchor 0.0
#         ease 2.0 xpos 0.5 ypos 1.0 xanchor 0.5 yanchor 1.0
#
#     image hill pairtouch = "vfx/hillpair1.png"
#     image hill pairnotouch = "vfx/hillpair2.png"
#
#     image nightwash = "vfx/nightwash.png"
#
#     python:
#         def noisetile(bitmap, opacity=0.1):
#             return im.Tile(im.Alpha(bitmap,opacity))
#
#     image noiseoverlay:
#         noisetile("vfx/noise1.png")
#         pause 0.1
#         noisetile("vfx/noise2.png")
#         pause 0.1
#         noisetile("vfx/noise3.png")
#         pause 0.1
#         repeat
#
#     image comic vfx1 = "vfx/comic_vfx1.png"
#     image comic vfx2 = "vfx/comic_vfx2.png"
#     image comic vfx3 = "vfx/comic_vfx3.png"
#     image comic vfx4 = "vfx/comic_vfx4.png"

    image ev emi_bed_full = LiveComposite(hb_rescale_xy(800,1280),
                                        (0,0),hb_rescale_image("event/emi_bed_normal.jpg"),
                                        hb_rescale_xy(0,600),hb_rescale_image("event/emi_bed_legs.jpg"))

    image passoutOP1:
        Solid("#0000")
        Solid("#000") with ImageDissolve(hb_rescale_image("ui/tr-flashback.png"), 6.0, 32, alpha=True)

#     image wine = "vfx/wine.png"
#     image musicbox open = "vfx/musicbox_open.png"
#     image musicbox closed = "vfx/musicbox_closed.png"
#     image boxstrip = "vfx/boxstrip.png"
#     image teaset = "vfx/teaset.png"
#     image stuffedcat = "vfx/stuffedcat.png"
#     image chessboard = "vfx/chessboard.png"
#     image shangpai = "vfx/shangpai.png"
#     image pills = "vfx/pills.png"
#     image invite = "vfx/invite.png"
#     image brailler = "vfx/brailler.png"
#     image sc_comp = "vfx/sc_comp.png"
#     image letter_insert = "vfx/letter_insert.png"
#     image letter_open_insert = "vfx/letter_open_insert.png"
#     image letter_open_insert_2 = "vfx/letter_open_insert_2.png"
#
#     image hanaphone = "vfx/hanaphone.png"
#     image phonestrap = "vfx/phonestrap.png"
#     image hanaphonestrap = "vfx/hanaphonestrap.png"
#
#     image kenjibox = "vfx/kenjibox.png"
#
#     image jigorocard = "vfx/jigorocard.png"
#
#     image stallphoto_insert = "vfx/stallphoto_insert.png"
#
#     image lilly superclose = "vfx/lilly_superclose.png"
#     image lilly superclose_ouch = "vfx/lilly_superclose_ouch.png"
#     image lilly superclose_shock = "vfx/lilly_superclose_shock.png"
#
#
#     image blanknote = "vfx/note_blur.png"
#
#     image lillyprop back_cane = "vfx/prop_lilly_back_cane.png"
#     image lillyprop back_cane_close = "vfx/prop_lilly_back_cane_close.png"
#     image lillyprop back_cane_ss = sp_sunset("vfx/prop_lilly_back_cane.png")
#     image lillyprop back_cane_ni = sp_night("vfx/prop_lilly_back_cane.png")
#
#     image bg gallery_atelier_bw = im.Grayscale("bgs/gallery_atelier.jpg")
#     image bg school_scienceroom_bw = im.Grayscale("bgs/school_scienceroom.jpg")
#     image bg school_library_bw = im.Grayscale("bgs/school_library.jpg")
#     image bg city_street4_bw = im.Grayscale("bgs/city_street4.jpg")
#     image bg city_street3_bw = im.Grayscale("bgs/city_street3.jpg")
#     image bg school_council_bw = im.Grayscale("bgs/school_council.jpg")
#     image bg school_dormhisao_bw = im.Grayscale("bgs/school_dormhisao.jpg")
#
#
#
#
#     image fw_blank = Solid("#00000000")
#     image fw_flash = Solid("#FFFFFF66")
#
#     $ fw_dis_fast = Dissolve(0.04, alpha=True)
#     $ fw_dis_medium = Dissolve(0.2, alpha=True)
#     $ fw_dis_slow = Dissolve(3.0, alpha=True)
#     $ fw_sparkle_out = ImageDissolve(im.Tile("vfx/tr-pronoise.png"), 3.0, 32, alpha=True)
#
#     transform fw_constructor(my_image):
#         "fw_blank"
#         choice 15:
#             0.2
#         choice:
#             "fw_flash" with fw_dis_fast
#             0.2
#             choice:
#                 my_image with fw_dis_medium
#                 "fw_blank" with fw_dis_slow
#                 6.0
#             choice:
#                 my_image with fw_dis_medium
#                 "fw_blank" with fw_sparkle_out
#                 6.0
#
#         repeat
#
#
#     image fireworks = LiveComposite((800,600),
#                                   (0,0), fw_constructor("vfx/fw1.png"),
#                                   (0,0), fw_constructor("vfx/fw2.png"),
#                                   (0,0), fw_constructor("vfx/fw3.png"),
#                                   (0,0), fw_constructor("vfx/fw4.png"),
#                                   (0,0), fw_constructor("vfx/fw5.png"),
#                                   (0,0), fw_constructor("vfx/fw6.png"),
#                                   (0,0), fw_constructor("vfx/fw7.png"),
#                                   (0,0), fw_constructor("vfx/fw8.png"),
#                                   (0,0), fw_constructor("vfx/fw9.png"))
#
#     transform fw_constructor_nosparkle(my_image):
#         "fw_blank"
#         choice 15:
#             0.2
#         choice:
#             "fw_flash" with fw_dis_fast
#             0.2
#             my_image with fw_dis_medium
#             "fw_blank" with fw_dis_slow
#             6.0
#         repeat
#
#     image fireshine = LiveComposite((800,600),
#                                   (0,0), fw_constructor_nosparkle(Solid("#FF000009")),
#                                   (0,0), fw_constructor_nosparkle(Solid("#00FF0009")),
#                                   (0,0), fw_constructor_nosparkle(Solid("#0000FF09")),
#                                   (0,0), fw_constructor_nosparkle(Solid("#CC00CC09")),
#                                   (0,0), fw_constructor_nosparkle(Solid("#CCCC0009")),
#                                   (0,0), fw_constructor_nosparkle(Solid("#0000CC09")))
#
#     transform hanako_fw_constructor(in_r, in_g, in_b):
#         alpha 0.0
#         "fw_blank"
#         choice 15:
#             0.2
#         choice:
#             parallel:
#                 linear 0.04 alpha 1.0
#                 linear 3.0 alpha 0.0
#             parallel:
#                 "fw_flash" with fw_dis_fast
#                 0.05
#                 im.MatrixColor(hb_rescale_image("event/hanako_fw_flash.jpg"), im.matrix.tint(in_r, in_g, in_b)) with fw_dis_medium
#                 8.0
#         repeat
#
#     image hanako_fw = LiveComposite((800,600),
#                                     (0,0), hanako_fw_constructor(1.0, 1.0, 1.0),
#                                     (0,0), hanako_fw_constructor(1.1, 0.9, 0.9),
#                                     (0,0), hanako_fw_constructor(0.9, 1.1, 0.9),
#                                     (0,0), hanako_fw_constructor(0.9, 0.9, 1.1),
#                                     (0,0), hanako_fw_constructor(0.9, 1.05, 1.05),
#                                     (0,0), hanako_fw_constructor(1.05, 0.9, 1.05),
#                                     (0,0), hanako_fw_constructor(1.05, 1.05, 0.9))
#
#     image ev hanako_shanghaiwindow = hb_rescale_image("event/hanako_fw.jpg")
#
#     image bg school_library_yuuko_blurred = "vfx/school_library_yuuko_blurred.jpg"
#
#     image phone mobile = "vfx/mobile-sprite.png"
#
#     $ rain_trans = Dissolve(0.1, alpha=True)
#
#     transform rainAnim_tf(my_offset, my_zoom, my_alpha):
#         xalign 0.5 yalign 1.0 zoom my_zoom alpha my_alpha
#         "fw_blank"
#         my_offset
#         block:
#             choice:
#                 "vfx/fx-rain-bg1.png" with rain_trans
#                 0.2
#             choice:
#                 "vfx/fx-rain-bg2.png" with rain_trans
#                 0.2
#             choice:
#                 "vfx/fx-rain-bg3.png" with rain_trans
#                 0.2
#             choice:
#                 "vfx/fx-rain-bg4.png" with rain_trans
#                 0.2
#             choice:
#                 "vfx/fx-rain-bg5.png" with rain_trans
#                 0.2
#             choice:
#                 "vfx/fx-rain-bg6.png" with rain_trans
#                 0.2
#             repeat
#
#     python:
#
#         def rainAnim(my_offset=0.0, zoom=1.0, alpha=1.0):
#             return rainAnim_tf(my_offset, zoom, alpha)
#
#
#     image rain normal = LiveComposite((800, 600),
#                              (0,0), rainAnim(),
#                              (0,0), rainAnim(my_offset=0.1, zoom=1.2),
#                              )
#
#
#     image rain light = LiveComposite((800, 600),
#                              (0,0), rainAnim(alpha=0.3),
#                              (0,0), rainAnim(my_offset=0.1, zoom=1.2, alpha=0.3),
#                              )
#
#     image rain medium = LiveComposite((800, 600),
#                              (0,0), rainAnim(alpha=0.6),
#                              (0,0), rainAnim(my_offset=0.1, zoom=1.2, alpha=0.6),
#                              )
#
#     transform mm_widget_in(my_disp, my_delay):
#         my_disp
#         xalign 0.0 yanchor 0.0 ypos 0.2 subpixel True alpha 0.0
#         my_delay
#         easein 0.3 ypos 0.0 alpha 1.0
#
#
#
#     $ crowdtrans = Dissolve(0.3, alpha=True)
#
#     transform crowdgen(img1, img2, img3):
#         img1
#         block:
#             1.0
#             img2 with crowdtrans
#             1.0
#             img3 with crowdtrans
#             1.0
#             img1 with crowdtrans
#             repeat
#
#     image crowd = crowdgen("vfx/crowd1.png","vfx/crowd2.png","vfx/crowd3.png")
#     image crowd_ss = crowdgen(sunset("vfx/crowd1.png"),sunset("vfx/crowd2.png"),sunset("vfx/crowd3.png"))
#     image crowd_ni = crowdgen(night("vfx/crowd1.png"),night("vfx/crowd2.png"),night("vfx/crowd3.png"))
#     image crowd_fb = crowdgen(past("vfx/crowd1.png"),past("vfx/crowd2.png"),past("vfx/crowd3.png"))
#     image crowd_ni_fb = crowdgen(past_night("vfx/crowd1.png"),past_night("vfx/crowd2.png"),past_night("vfx/crowd3.png"))

init 505:
    image wallodrugs = drugsDisp(hb_rescale_x(1600),hb_rescale_y(800))

init 500 python:
#
#
#
#     def night(img_in):
#         return im.MatrixColor(img_in, im.matrix.tint(0.6, 0.6, 0.7) * im.matrix.saturation(0.6))
#
#     def sp_night(img_in):
#         return im.MatrixColor(img_in, im.matrix.tint(0.9, 0.92, 1.0) * im.matrix.brightness(-0.05))
#
#     def sunset(img_in):
#         return im.MatrixColor(img_in, im.matrix.tint(1.1, 0.95, 0.85) * im.matrix.brightness(0.1) * im.matrix.saturation(1.2))
#
#     def sp_sunset(img_in):
#         return im.MatrixColor(img_in, im.matrix.tint(1.02, 0.95, 0.9) * im.matrix.brightness(0.05) * im.matrix.saturation(1.1))
#
#     def past(img_in):
#         return im.MatrixColor(img_in, im.matrix.saturation(0.15) * im.matrix.tint(1.0, .94, .76))
#
#     def rain(img_in):
#         return im.MatrixColor(img_in, im.matrix.saturation(0.4) * im.matrix.tint(0.95, 0.95, 1.0) * im.matrix.brightness(-0.1))
#
#     def sp_rain(img_in):
#         return im.MatrixColor(img_in, im.matrix.saturation(0.6) * im.matrix.tint(0.96, 0.96, 1.0) * im.matrix.brightness(-0.05))
#
#     def past_night(img_in):
#         return im.MatrixColor(img_in, im.matrix.tint(0.6, 0.6, 0.7) * im.matrix.saturation(0.6) * im.matrix.saturation(0.15) * im.matrix.tint(1.0, .94, .76))
#
#
#     bg_filters = ((sunset,'ss'),
#                   (night,'ni'),
#                   (rain,'rn'),
#                   )
#
#     sp_filters = ((sp_sunset,'ss'),
#                   (sp_night,'ni'),
#                   (sp_rain,'rn'),
#                   )
#
#
#
#
#
#     letterbox = LiveComposite((800,600),
#                               (0,0), Solid("#000", ymaximum=75),
#                               (0,525), Solid("#000", ymaximum=75))
#     renpy.image("letterbox",letterbox)
#
#
#
#     flashback = ImageDissolve("ui/tr-flashback.png", 2.0, 32, reverse=True)
#     flashforward = ImageDissolve("ui/tr-flashback.png", 2.0, 32)
#     flashforward_fast = ImageDissolve("ui/tr-flashback.png", 0.5, 32)
#
#     letter_in = ImageDissolve(im.Tile("ui/tr-letter.png"), 1.0, 8, reverse=True)
#     letter_out = ImageDissolve(im.Tile("ui/tr-letter.png"), 1.0, 8)
#
#     hands_in = ImageDissolve("vfx/handsdissolve.png", 0.2, ramplen=256, reverse=True)
#     hands_out = ImageDissolve("vfx/handsdissolve.png", 0.2, ramplen=256)
#
#     softwipedown = ImageDissolve(im.Tile("vfx/tr-wipeh.png"), 1.5, 16)
#     softwipeup = ImageDissolve(im.Tile("vfx/tr-wipeh.png"), 1.5, 16, reverse=True)

    snowfg = SnowBlossom(hb_rescale_image("vfx/snowflake.png"), start=3.0, count=30, border=hb_rescale_x(50), xspeed=(hb_rescale_x(20), hb_rescale_x(50)), yspeed=(hb_rescale_y(120), hb_rescale_y(180)), fast=True)
    snowbg = SnowBlossom(im.Scale(hb_rescale_image("vfx/snowflake.png"),hb_rescale_x(5),hb_rescale_y(5)), count=60, yspeed=(hb_rescale_y(80), hb_rescale_y(120)), start=3.0, border=hb_rescale_x(50), xspeed=(hb_rescale_x(20), hb_rescale_x(50)), fast=True)
    renpy.image("snow",LiveComposite(hb_rescale_xy(800,600),
                                     (0,0),snowbg,
                                     (0,0),snowfg))

#     def GenericWhiteout(rise,hold,fall):
#         return transition_attach_sound(Fade(rise, hold, fall, color="#FFF"), "sfx/whiteout.ogg")
#
#     def SilentWhiteout(rise,hold,fall):
#         return Fade(rise, hold, fall, color="#FFF")
#
#     silentflash = Fade(0.25, 0,.75, color="#FFFFFF")
#     flash = transition_attach_sound(silentflash, "sfx/flash.ogg")
#     whiteout = GenericWhiteout(1.0,0.0,1.0)
#     silentwhiteout = SilentWhiteout(1.0,0.0,1.0)
#
#     cameraflash = Fade(0.05, 0.1 ,.75, color="#FFFFFF")
#     cameraflashlong = Fade(0.05, 0.1 ,4.0, color="#FFFFFF")
#
#
#     sakura = SnowBlossom(anim.Filmstrip("vfx/sakura.png", (10, 10), (2, 1), .25), xspeed=(150, 100), yspeed=(75, 150), count=80, fast=True)
#     renpy.image("sakura", sakura)
#     renpy.image("hospitalmask", "vfx/hospitalroommask_new.png")
#
#
#     dandeliontfg = SnowBlossom("vfx/dandelion.png", count=10, border=25, xspeed=(100, 200), yspeed=(-40, -15), start=8.0, fast=False, horizontal=True)
#     dandeliontbg = SnowBlossom(im.Scale("vfx/dandelion.png",13,16), count=20, border=25, xspeed=(50, 100), yspeed=(-30, -10), start=8.0, fast=False, horizontal=True)
#     renpy.image("dandelions thin",LiveComposite((800,600),
#                                     (0,0),dandeliontbg,
#                                     (0,0),dandeliontfg))
#
#     renpy.image("dandelionsfg thin",dandeliontfg)
#     renpy.image("dandelionsbg thin",dandeliontbg)
#
#
#     dandeliondfg = SnowBlossom("vfx/dandelion.png", count=20, border=25, xspeed=(100, 200), yspeed=(-40, -15), start=8.0, fast=True, horizontal=True)
#     dandeliondbg = SnowBlossom(im.Scale("vfx/dandelion.png",13,16), count=40, border=25, xspeed=(50, 100), yspeed=(-30, -10), start=8.0, fast=True, horizontal=True)
#     renpy.image("dandelions dense",LiveComposite((800,600),
#                                     (0,0),dandeliondbg,
#                                     (0,0),dandeliondfg))
#
#     renpy.image("dandelionsfg dense",dandeliondfg)
#     renpy.image("dandelionsbg dense",dandeliondbg)
#
#     dandelionbg_blur = SnowBlossom(im.Scale("vfx/dandelion_blur.png",13,16), count=40, border=25, xspeed=(50, 100), yspeed=(-30, -10), start=8.0, fast=True, horizontal=True)
#     renpy.image("dandelions_blurbg",dandelionbg_blur)
#
#     dandelionfg_blur= SnowBlossom("vfx/dandelion_blur.png", count=20, border=25, xspeed=(100, 200), yspeed=(-40, -15), start=8.0, fast=True, horizontal=True)
#     renpy.image("dandelions_blurfg",dandelionfg_blur)
#
#
#     trans_dandelion = CropMove(5.0, "wipeleft")
#
#
#
#
#
#     steam = anim.TransitionAnimation("vfx/steam1.png", 1.5, Dissolve(0.5, alpha=True),
#                                      "vfx/steam2.png", 1.5, Dissolve(0.5, alpha=True),
#                                      "vfx/steam3.png", 1.5, Dissolve(0.5, alpha=True))
#     steam2 = anim.TransitionAnimation("vfx/steam3.png", 0.75, Dissolve(0.5, alpha=True),
#                                      "vfx/steam1.png", 1.5, Dissolve(0.5, alpha=True),
#                                      "vfx/steam2.png", 1.5, Dissolve(0.5, alpha=True),
#                                      "vfx/steam3.png", 0.75, None)
#
#     renpy.image("steam", steam)
#     renpy.image("steam2", steam2)
#
#
#     flashback = ImageDissolve(im.Tile("ui/tr-flashback.png"), 2.0, 32, alpha=True)
#     clockwipe = ImageDissolve(im.Tile("ui/tr-clockwipe.png"), 2.0, 8)
#     renpy.image("kslogo heart", "ui/bt-logolarge-heartonly.png")
#     renpy.image("kslogo words", "ui/bt-logolarge.png")
#
#     renpy.image("credits mask", "ui/roll_mask.png")
#
#     runningspline = SplineMotion([
#         ((0.470, 0.485,),),
#         ((0.508, 0.525,), (0.484, 0.464,), (0.482, 0.541,),),
#         ((0.518, 0.483,), (0.534, 0.512,), (0.532, 0.488,),),
#         ((0.480, 0.528,), (0.506, 0.464,), (0.500, 0.544,),),
#         ((0.470, 0.485,), (0.466, 0.520,), (0.452, 0.507,),),
#         ], 1.1, anchors=(0.5, 0.5), repeat=True)
#
#     fastrunningspline = SplineMotion([
#         ((0.470, 0.485,),),
#         ((0.508, 0.525,), (0.484, 0.464,), (0.482, 0.541,),),
#         ((0.518, 0.483,), (0.534, 0.512,), (0.532, 0.488,),),
#         ((0.480, 0.528,), (0.506, 0.464,), (0.500, 0.544,),),
#         ((0.470, 0.485,), (0.466, 0.520,), (0.452, 0.507,),),
#         ], 0.8, anchors=(0.5, 0.5), repeat=True)
#
#
#
#
#
#
#
#
#
#
#
#     config.nvl_page_ctc = anim.Filmstrip("ui/ctc_strip.png", (16,16), (8,8), 0.03, ypos=560, xpos=772)
#     config.nvl_page_ctc_position = "fixed"
#
#
#     def say_wrapper(who, what, **kwargs):
#         store_current_line(who, what)
#         return renpy.show_display_say(who, what, **kwargs)
#
#
#     def change_quotes(what):
#         innerwhat = what [1:-1]
#         innerwhat = innerwhat.replace(displayStrings.quote_outer_open,displayStrings.quote_inner_open)
#         innerwhat = innerwhat.replace(displayStrings.quote_outer_close,displayStrings.quote_inner_close)
#         what = what[0] + innerwhat + what[-1]
#         return what
#
#
#     def quotefixer(who, what, **kwargs):
#         return say_wrapper(who, change_quotes(what), **kwargs)
#
#     def define_characters():
#
#         add_styles = prefix_dict(displayStrings.styleoverrides, prefix="what_", combine=True)
#
#
#
#         store.adv = ReadbackADVCharacter(name=None,
#                                          ctc=config.nvl_page_ctc,
#                                          ctc_position = "fixed",
#                                          show_function=quotefixer,
#                                          what_prefix=displayStrings.quote_outer_open,
#                                          what_suffix=displayStrings.quote_outer_close,
#                                          **add_styles)
#
#         store.name_only = Character(None, kind=adv)
#
#
#
#         store.hi = Character(displayStrings.name_hi, color="#629276")
#
#         store.ha = Character(displayStrings.name_ha, color="#897CBF")
#         store.emi = Character(displayStrings.name_emi, color = "#FF8D7C")
#         store.rin = Character(displayStrings.name_rin, color = "#b14343")
#         store.li = Character(displayStrings.name_li, color="#F9EAA0")
#         store.shi = Character(displayStrings.name_shi, color="#72ADEE")
#         store.mi = Character(displayStrings.name_mi, color="#FF809F")
#
#         store.mi_shi = Character(displayStrings.name_shi, color="#FF809F")
#         store.mi_not_shi = Character("{s}"+displayStrings.name_shi+"{/s} "+displayStrings.name_mi, color="#FF809F")
#
#         store.ke = Character(displayStrings.name_ke, color="#CC7C2A")
#         store.mu = Character(displayStrings.name_mu, color = "#FFFFFF")
#         store.nk = Character(displayStrings.name_nk, color = "#FFFFFF")
#         store.no = Character(displayStrings.name_no, color="#E0E0E0")
#         store.yu = Character(displayStrings.name_yu, color="#2c9e31")
#         store.sa = Character(displayStrings.name_sa, color="#D4D4FF")
#         store.aki = Character(displayStrings.name_aki, color="#eb243b")
#         store.hh = Character(displayStrings.name_hh, color="#6299FF")
#         store.hx = Character(displayStrings.name_hx, color="#99AACC")
#         store.emm = Character(displayStrings.name_emm, color="#995050")
#         store.sk = Character(displayStrings.name_sk, color="#7187A8")
#         store.mk = Character(displayStrings.name_mk, color="#AD735E")
#
#         store.mystery = Character(displayStrings.name_mystery)
#
#
#
#         store.ssh = Character(displayStrings.name_shi, kind=shi, what_prefix=u"[ ", what_suffix=u" ]")
#         store.his = Character(displayStrings.name_hi, kind=hi, what_prefix=u"[ ", what_suffix=u" ]")
#
#
#         store.ha_ = Character(displayStrings.name_ha_, kind=ha)
#         store.emi_ = Character(displayStrings.name_emi_, kind=emi)
#         store.rin_ = Character(displayStrings.name_rin_, kind=rin)
#         store.li_ = Character(displayStrings.name_li_, kind=li)
#         store.mi_ = Character(displayStrings.name_mi_, kind=mi)
#         store.ke_ = Character(displayStrings.name_ke_, kind=ke)
#         store.mu_ = Character(displayStrings.name_mu_, kind=mu)
#         store.yu_ = Character(displayStrings.name_yu_, kind=yu)
#         store.no_ = Character(displayStrings.name_no_, kind=no)
#         store.sa_ = Character(displayStrings.name_sa_, kind=sa)
#         store.aki_ = Character(displayStrings.name_aki_, kind=aki)
#         store.nk_ = Character(displayStrings.name_nk_, kind=nk)
#         store.hx_ = Character(displayStrings.name_hx_, kind=hx)
#         store.hh_ = Character(displayStrings.name_hh_, kind=hh)
#         store.emm_ = Character(displayStrings.name_emm_, kind=emm)
#
#
#
#
#         store.n = ReadbackNVLCharacter(None,
#                                        kind=nvl,
#                                        ctc=anim.Blink(im.Rotozoom("ui/ctc.png", 270, 1.0),
#                                        offset=1.0, ypos=560, xpos=772),
#                                        ctc_position = "fixed",
#                                        **add_styles)
#
#         store.nb = Character(None,
#                                kind=adv,
#                                ctc=None,
#                                what_color="#666666",
#                                what_line_spacing=8,
#                                what_prefix="",
#                                what_suffix="",
#                                show_function=say_wrapper,
#                                window_style="b_nvl_window")
#
#         store.rinbabble = Character(None,
#                                     kind=n,
#                                     what_prefix=u"{color=#FF8D7C}{b}"+displayStrings.name_rin+"{/b}{/color}\n" + displayStrings.quote_outer_open,
#                                     what_suffix=displayStrings.quote_outer_close)
#
#
#
#
#
#
#         store.narrator = Character(NARRATOR_NAME, what_prefix="", what_suffix="", show_function=say_wrapper)
#         store.centered = Character(None, what_style="centered_text", window_style="centered_window", what_prefix="", what_suffix="", show_function=say_wrapper)
#         store.centered_b = Character(None, kind=centered, what_color="#666666", what_drop_shadow=None, what_style="alive_text")
#         store.centered_alive = Character(None, kind=centered_b, window_xpos=130, window_xanchor=0, window_xpadding=0)
#         store.centered_alive_j = Character(None, kind=centered_b, window_xpos=70, window_xanchor=0, window_xpadding=0)
#
#
#
#

    dotwipe_down = ImageDissolve(im.Tile(hb_rescale_image("ui/tr-dots_col.png")), 0.5, 32, ramptype="mcube")
    dotwipe_up = ImageDissolve(im.Tile(hb_rescale_image("ui/tr-dots_col.png")), 0.5, 32, ramptype="mcube", reverse = True)
#     slowfade = Fade(1.0, 0.5, 1.0)
#
#
#     openeye = ImageDissolve("vfx/tr_eyes.png", 2.0, 64, ramptype="cube")
#     shuteye = ImageDissolve("vfx/tr_eyes.png", 2.0, 64, ramptype="mcube", reverse=True)
#
#     openeyefast = ImageDissolve("vfx/tr_eyes.png", 0.5, 64, ramptype="cube")
#     shuteyefast = ImageDissolve("vfx/tr_eyes.png", 0.2, 64, ramptype="mcube", reverse=True)
#
#     openeye_shock = ImageDissolve("vfx/tr-openshock.png", 0.8, 64, ramptype="cube")
#
#     whip_right = ImageDissolve(im.Tile("vfx/tr-softwipe.png"), 0.3, 256, ramptype="mcube")
#     whip_left = ImageDissolve(im.Tile("vfx/tr-softwipe.png"), 0.3, 256, ramptype="mcube", reverse = True)
#
#
#     define.move_transitions("charamove", 1.0, _ease_time_warp, _ease_in_time_warp, _ease_out_time_warp)
#     define.move_transitions("charamove_slow", 2.0, _ease_time_warp, _ease_in_time_warp, _ease_out_time_warp)
#     define.move_transitions("charamove_old", 1.0, _ease_time_warp, _ease_in_time_warp, _ease_out_time_warp, old=True)
#
#     define.move_transitions("ease_accel", 0.5, _ease_out_time_warp)
#     define.move_transitions("ease_decel", 0.5, _ease_in_time_warp)
#
#     define.move_transitions("slice_in", 0.2, _ease_in_time_warp)
#
#     define.move_transitions("charamove_accel", 1.0, _ease_out_time_warp)
#     define.move_transitions("charamove_decel", 1.0, _ease_in_time_warp)
#
#     define.move_transitions("charafast", 0.2, _ease_time_warp, _ease_in_time_warp, _ease_out_time_warp)
#
#     def Dissolvemove(time, time_warp=_ease_time_warp):
#         top = Dissolve(time)
#         before = MoveTransition(time, factory=MoveFactory(time_warp=time_warp), old=True)
#         after = MoveTransition(time, factory=MoveFactory(time_warp=time_warp))
#         return ComposeTransition(top, before=before, after=after)
#
#     dissolvecharamove = Dissolvemove(1.0)
#
#     delayed_dissolve = MultipleTransition([False,Dissolve(0.5),False,Dissolve(0.5),True])
#     shownote = ComposeTransition(delayed_dissolve, None, charamoveinbottom)
#     hidenote = ComposeTransition(Dissolve(0.5, alpha=True), charamoveoutbottom, None)
#
#     delayblinds = ImageDissolve("vfx/tr-delayblinds.png", 1.0)
#
#     delayblindsfade = MultipleTransition([False,SoundTransition("sfx/time.ogg"),False,delayblinds,Solid("#000"),delayblinds,True])
#     delayblindsfadesilent = MultipleTransition([False,delayblinds,Solid("#000"),delayblinds,True])
#
#
#
#     menueffect = None
#     charaenter = dissolve
#     charaexit = dissolve
#     charachange = dissolve
#     characlose = dissolve
#     charadistant = dissolve
#     locationchange = dissolve
#     locationskip = Fade(0.5, 0, 0.5)
#     locationskip_in = Fade(0.5, 0, 0.0)
#     locationskip_out = Dissolve(0.5)
#
#     shorttimeskip = delayblindsfade
#     shorttimeskipsilent = delayblindsfadesilent
#
#     ex_m_tracks = []
#     def ks_music(filename, alias):
#         fullpath = "bgm/" + filename + ".ogg"
#         setattr(store, "music_" + alias, fullpath)
#         store.ex_m_tracks.append((filename.replace("_", " "), fullpath))
#
#
#
#
#
#     ks_music("Afternoon", "tranquil")
#     ks_music("Ah_Eh_I_Oh_You", "nurse")
#     ks_music("Air_Guitar", "soothing")
#     ks_music("Aria_de_l'Etoile", "twinkle")
#     ks_music("Breathlessly", "moonlight")
#     ks_music("Caged_Heart", "rain")
#     ks_music("Cold_Iron", "tragic")
#     ks_music("Comfort", "comfort")
#     ks_music("Concord", "lilly")
#     ks_music("Daylight", "daily")
#     ks_music("Ease", "ease")
#     ks_music("Everyday_Fantasy", "another")
#     ks_music("Friendship", "friendship")
#     ks_music("Fripperies", "happiness")
#     ks_music("Generic_Happy_Music", "comedy")
#     ks_music("High_Tension", "tension")
#     ks_music("Hokabi", "running")
#     ks_music("Innocence", "innocence")
#     ks_music("Letting_my_Heart_Speak", "heart")
#     ks_music("Lullaby_of_Open_Eyes", "serene")
#     ks_music("Moment_of_Decision", "drama")
#     ks_music("Nocturne", "night")
#     ks_music("Out_of_the_Loop", "kenji")
#     ks_music("Painful_History", "hanako")
#     ks_music("Parity", "rin")
#     ks_music("Passing_of_Time", "timeskip")
#     ks_music("Raindrops_and_Puddles", "dreamy")
#     ks_music("Red_Velvet", "jazz")
#     ks_music("Romance_in_Andante_II", "romance")
#     ks_music("Romance_in_Andante", "credits")
#     ks_music("Sarabande_from_BWV1010", "musicbox")
#     ks_music("School_Days", "normal")
#     ks_music("Shadow_of_the_Truth", "sadness")
#     ks_music("Standing_Tall", "emi")
#     ks_music("Stride", "pearly")
#     ks_music("The_Student_Council", "shizune")
#     ks_music("To_Become_One", "one")
#     ks_music("Wiosna", "menus")
#
#
#
#
#     config.main_menu_music = None
#     config.enter_sound = None
#
#
#
#
#
#
#     sfx_tcard = "sfx/tcard.ogg"
#     sfx_4lslogo = "sfx/4lsaudiologo.ogg"
#     sfx_alarmclock = "sfx/alarm.ogg"
#     sfx_normalbell = "sfx/carillon.ogg"
#     sfx_warningbell = "sfx/chaimu.ogg"
#     sfx_crunchydeath = "sfx/crunch.ogg"
#     sfx_fireworks = "sfx/fireworks.ogg"
#     sfx_rain = "sfx/rain.ogg"
#     sfx_rustling = "sfx/rustling.ogg"
#     sfx_impact = "sfx/wumph.ogg"
#     sfx_impact2 = "sfx/wumph_2.ogg"
#     sfx_heartfast = "sfx/heart_single_fast.ogg"
#     sfx_heartslow = "sfx/heart_single_slow.ogg"
#     sfx_heartstop = "sfx/heart_stop.ogg"
#
#     sfx_emijogging = "sfx/emijogging.ogg"
#     sfx_emirunning = "sfx/emirunning.ogg"
#     sfx_emipacing = "sfx/emipacing.ogg"
#     sfx_emisprinting = "sfx/emisprinting.ogg"
#
#     sfx_startpistol = "sfx/startpistol.ogg"
#     sfx_crowd_indoors = "sfx/crowd_indoors.ogg"
#     sfx_crowd_outdoors = "sfx/crowd_outdoors.ogg"
#     sfx_crowd_cheer = "sfx/crowd_cheer.ogg"
#     sfx_doorknock = "sfx/doorknock.ogg"
#     sfx_doorknock2 = "sfx/doorknock2.ogg"
#     sfx_doorslam = "sfx/doorslam.ogg"
#     sfx_doorclose = "sfx/doorclose.ogg"
#
#     sfx_cicadas = "sfx/cicadas.ogg"
#     sfx_scratch = "sfx/scratch.ogg"
#     sfx_traffic = "sfx/traffic.ogg"
#     sfx_rumble = "sfx/rumble.ogg"
#     sfx_skid = "sfx/skid2.ogg"
#     sfx_gymbounce = "sfx/emibounce.ogg"
#     sfx_hammer = "sfx/hammer.ogg"
#     sfx_paper = "sfx/paperruffling.ogg"
#     sfx_birdstakeoff = "sfx/birdstakeoff.ogg"
#     sfx_storebell = "sfx/storebell.ogg"
#     sfx_thunder = "sfx/thunder.ogg"
#     sfx_slide = "sfx/slide.ogg"
#     sfx_slide2 = "sfx/slide2.ogg"
#     sfx_draw = "sfx/sword_draw.ogg"
#     sfx_shower = "sfx/shower.ogg"
#     sfx_switch = "sfx/switch.ogg"
#     sfx_pillow = "sfx/pillow.ogg"
#     sfx_cellphone = "sfx/cellphone.ogg"
#     sfx_door_creak = "sfx/door_creak.ogg"
#     sfx_dooropen = "sfx/dooropen.ogg"
#     sfx_dropglasses = "sfx/dropglasses.ogg"
#     sfx_can = "sfx/can.ogg"
#     sfx_stallbuilding = "sfx/stallbuilding.ogg"
#     sfx_parkambience = "sfx/parkambience.ogg"
#     sfx_trainint = "sfx/trainint.ogg"
#
#     sfx_footsteps_hard = "sfx/footsteps_hard.ogg"
#     sfx_footsteps_soft = "sfx/footsteps_soft.ogg"
#     sfx_paper = "sfx/paper.ogg"
#     sfx_paperruffling = "sfx/paperruffling.ogg"
#     sfx_rooftop = "sfx/rooftop.ogg"
#     sfx_lighter = "sfx/lighter.ogg"
#     sfx_phone = "sfx/phone.ogg"
#     sfx_hollowclick = "sfx/hollowclick.ogg"
#     sfx_businterior = "sfx/businterior.ogg"
#     sfx_teacup = "sfx/teacup.ogg"
#     sfx_can_clatter = "sfx/can_clatter.ogg"
#     sfx_snap = "sfx/snap.ogg"
#
#     sfx_billiards_break = "sfx/billiards_break.ogg"
#     sfx_billiards = "sfx/billiards.ogg"
#     sfx_lock = "sfx/lock.ogg"
#     sfx_dropstuff = "sfx/dropstuff.ogg"
#     sfx_camera = "sfx/camera.ogg"
#
#
#
#     sfx_time = "sfx/time.ogg"
#     sfx_flash = "sfx/flash.ogg"
#     sfx_whiteout = "sfx/whiteout.ogg"
#
#
#     vid_op = "op_1.mkv"
#     vid_4ls = "video/4ls.mkv"

#     config.screen_width = 1200
#     config.screen_height = 900

#     config.default_afm_enable = None
#
#     _preferences.afm_time = 0
#     if not persistent.afm_time:
#         persistent.afm_time = 5
#
#     config.default_fullscreen = False
#     config.default_text_cps = 70
#     config.has_sound = True
#     config.has_music = True
#     config.has_voice = False
#
#     config.skip_indicator = False
#
#     config.window_icon = "ui/icon.png"
#
    config.enter_transition = dotwipe_down
    config.exit_transition = dotwipe_up
    config.intra_transition = dissolve
    config.main_game_transition = dotwipe_down
    config.game_main_transition = dotwipe_up
    config.end_splash_transition = None
    config.end_game_transition = dotwipe_up
    config.after_load_transition = dotwipe_up
#
#     config.window_show_transition = Dissolve(0.2)
#     config.window_hide_transition = Dissolve(0.2)
#
#     def wdt_hide(trans=dissolve):
#         wdt_off()
#         narrator("", interact=False)
#         renpy.with_statement(None)
#         renpy.with_statement(trans)
#
#     def wdt_show(trans=dissolve):
#         narrator("", interact=False)
#         renpy.with_statement(trans)
#         wdt_on()
#
#
#
#
#
#
#     config.label_overrides = {"_noisy_return": "custom_noisy_return",
#                               "_return": "custom_return",
#                               "_confirm_quit": "quit_from_os",
#                               "_game_menu": "_custom_game_menu"}
#
#
#
#
#     def ib_base(image, **options):
#         return im.MatrixColor(image, im.matrix.opacity(0.4), **options)
#     def ib_disabled(image, **options):
#         return im.MatrixColor(image, im.matrix.opacity(0.1), **options)
#
#
#     style.default.font = mainfont
#
#
    def main_menu_composer(is_animated):

        if is_animated and store.animate_mm:
            is_animated = True
        else:
            is_animated = False


        is_animated = False

        widget_map = [('tc_act4_shizune', hb_rescale_xy(677, 308), '16_tc4-shizune.png'),
                      ('tc_act3_shizune', hb_rescale_xy(681, 383), '15_tc3-shizune.png'),
                      ('tc_act2_shizune', hb_rescale_xy(620, 476), '14_tc2-shizune.png'),
                      ('tc_act4_rin', hb_rescale_xy(526, 190), '13_tc4-rin.png'),
                      ('tc_act3_rin', hb_rescale_xy(590, 232), '12_tc3-rin-rin.png'),
                      ('tc_act3_rin', hb_rescale_xy(637, 295), '11_tc3-rin-hisao.png'),
                      ('tc_act2_rin', hb_rescale_xy(629, 402), '10_tc2-rin.png'),
                      ('tc_act4_lilly', hb_rescale_xy(464, 201), '09_tc4-lilly.png'),
                      ('tc_act3_lilly', hb_rescale_xy(529, 279), '08_tc3-lilly.png'),
                      ('tc_act2_lilly', hb_rescale_xy(582, 375), '07_tc2-lilly.png'),
                      ('tc_act4_hanako', hb_rescale_xy(382, 279), '06_tc4-hanako.png'),
                      ('tc_act4_emi', hb_rescale_xy(375, 370), '05_tc4-emi.png'),
                      ('tc_act3_emi', hb_rescale_xy(429, 453), '04_tc3-emi.png'),
                      ('tc_act2_emi', hb_rescale_xy(463, 498), '03_tc2-emi.png'),
                      ('tc_act3_hanako', hb_rescale_xy(469, 312), '02_tc3-hanako.png'),
                      ('tc_act2_hanako', hb_rescale_xy(493, 380), '01_tc2-hanako.png'),
                      ('tc_act1', hb_rescale_xy(552, 473), '00_tc1-hisao.png')]

        basedelay = 0.07
        delay = basedelay * len (widget_map)
        widget_list = [(config.screen_width,config.screen_height), (0,0), hb_rescale_image("ui/main/bg-main.png"), hb_rescale_xy(720,568),Text(game_version+"\n"+renpy.version(),color="#00000080", size=hb_rescale_y(9), font="font/playtime.ttf")]
        for trigger, offset, widget in widget_map:
            if persistent_seen(trigger) or has_devlvl():
                widget_list.append(offset)
                if is_animated:
                    widget_list.append(Fixed(mm_widget_in(hb_rescale_image("ui/main/"+widget),delay), xmaximum=hb_rescale_x(220), ymaximum=hb_rescale_y(200)))
                else:
                    widget_list.append(hb_rescale_image("ui/main/"+widget))
                delay -= basedelay
        return LiveComposite(*widget_list), None
#
#     def mm_live_bg(st,at):
#         return main_menu_composer(True)
#
#     def mm_static_bg(st,at):
#         return main_menu_composer(False)
#
#     renpy.image("mmcache",DynamicDisplayable(mm_static_bg))

    style.mm_root.background = DynamicDisplayable(mm_live_bg)
    style.create("mm_static", "mm_root")
    style.mm_static.background = DynamicDisplayable(mm_static_bg)

    style.mm_menu_frame.background = None

    style.mm_menu_frame.xanchor = 0.0
    style.mm_menu_frame.yanchor = 1.0
    style.mm_menu_frame.xpos = hb_rescale_x(65)

    style.mm_menu_frame.ypos = hb_rescale_y(540)
    style.mm_button.background = None
    style.mm_button.xminimum = 0
    style.mm_button_text.xalign = 0

    style.mm_button_text.color = "#00000066"
    style.mm_button_text.insensitive_color = "#00000019"
    style.mm_button_text.hover_color = "#000000"

    style.prompt_text.color = "#00000066"


    style.create("rpa_button", "button")
    style.rpa_button.background = None
    style.create("rpa_button_text", "mm_button_text")

    style.create("rpa_active_button", "rpa_button")
    style.create("rpa_active_button_text", "rpa_button_text")
    style.rpa_active_button_text.insensitive_color = "#000000"

    style.say_window.background = hb_rescale_image("ui/bg-say.png")
    style.say_window.left_padding = hb_rescale_x(14)
    style.say_window.right_padding = hb_rescale_x(30)

    style.say_window.top_padding = hb_rescale_y(10)
    style.say_window.xmargin = 0
    style.say_window.ymargin = 0
    style.say_window.yminimum = hb_rescale_y(160)

    style.say_vbox.spacing = hb_rescale_y(15)

    style.say_dialogue.first_indent = hb_rescale_x(14)
    style.say_dialogue.rest_indent = hb_rescale_x(14)
    style.say_dialogue.size = hb_rescale_y(22)

    style.say_dialogue['rollback'].color="#CCCCCC"

    style.centered_text.size = hb_rescale_y(26)
    style.centered_text.outlines = [ (1,"#000C") ]

    style.create("alive_text", "centered_text")
    style.alive_text.outlines = []

    style.create("note_window", "centered_window")
    style.note_window.background=Frame(hb_rescale_image("ui/bg-note.png"), 0, hb_rescale_y(16), tile=True)
    style.note_window.xalign=0.5
    style.note_window.yalign=0.5
    style.note_window.top_padding=hb_rescale_y(14)
    style.note_window.left_padding=hb_rescale_x(35)
    style.note_window.right_padding=hb_rescale_x(25)
    style.note_window.xmaximum=hb_rescale_x(402)
    style.note_window.yfill = False
    style.note_window.xminimum=hb_rescale_x(402)

    style.create("note_text", "centered_text")
    style.note_text.size = hb_rescale_y(25)
    style.note_text.outlines = []
    style.note_text.color = "#000244"
    style.note_text.text_align=0.0
    style.note_text.xalign=0.0
    style.note_text.yalign=0.0
    style.note_text.slow_cps = False
    style.note_text.layout = "greedy"

    style.nvl_window.background = hb_rescale_image("ui/bg-nvl.png")
    style.nvl_window.top_padding = hb_rescale_y(25)

    style.say_window.left_padding = hb_rescale_x(14)
    style.say_window.right_padding = hb_rescale_x(30)

    style.create("b_nvl_window", "nvl_window")
    style.b_nvl_window.background = None
    style.b_nvl_window.top_padding = hb_rescale_y(140)
    style.b_nvl_window.left_padding = hb_rescale_x(80)

    style.menu_choice_button.background = hb_rescale_image("ui/bg-choice.png")
    style.menu_choice.take(style.mm_button_text)
    style.menu_choice.xalign = 0.5
    style.menu_choice.size = hb_rescale_y(20)
    style.menu_choice_button.top_padding = hb_rescale_y(5)

    style.hyperlink_text.color = "#F99"
    style.hyperlink_text.underline = False

    style.create("readback_text", "say_dialogue")
    style.readback_text.size= hb_rescale_y(16)
    style.readback_text.color = "#00000066"
    style.create("readback_label", "readback_text")
    style.readback_label.bold = True

    style.create("comment_frame", "default")
    style.comment_frame.background = Frame(hb_rescale_image("ui/bg-comment.png"), hb_rescale_x(8),hb_rescale_y(8),tile=True)
    style.comment_frame.xmargin = hb_rescale_x(4)
    style.comment_frame.ymargin = hb_rescale_y(4)
    style.comment_frame.xpadding = hb_rescale_x(16)
    style.comment_frame.top_padding = hb_rescale_y(12)
    style.comment_frame.bottom_padding = hb_rescale_y(18)
    style.comment_frame.xminimum = hb_rescale_x(800)

    style.say_label.size = hb_rescale_y(22)

    style.create("comment_label", "say_label")

    style.create("comment_text", "say_dialogue")
    style.comment_text.first_indent = 0
    style.comment_text.rest_indent = 0

    style.gm_root.background = Solid("#0000007F")
    style.gm_root[None].background = None
    style.gm_nav_frame.xalign = 0.5
    style.gm_nav_frame.yalign = 0.4
    style.gm_nav_frame.xminimum= hb_rescale_x(200)
    style.gm_nav_frame.yminimum = hb_rescale_y(306)
    style.gm_nav_frame.background = hb_rescale_image("ui/bg-gm.png")
    style.gm_nav_frame.top_padding = hb_rescale_y(62)
    style.gm_nav_box.xalign = 0.5
    style.gm_nav_box.yalign = 0.5
    if is_glrenpy():
        style.gm_nav_box.spacing = -1
    else:
        style.gm_nav_box.spacing = 0
    style.gm_nav_button.take(style.mm_button)
    style.gm_nav_button_text.take(style.mm_button_text)
    style.gm_nav_button.xalign = 0.5
    style.gm_nav_button_text.xalign = 0.5

    style.yesno_frame = Style(style.menu_frame, help="frame containing a yes/no prompt and yes/no buttons")
    style.yesno_frame_vbox = Style(style.vbox, help="box separating yes/no prompt from yes/no buttons")

    style.yesno_prompt = Style(style.prompt, help="a yes/no prompt")
    style.yesno_prompt_text = Style(style.prompt_text, help="a yes/no prompt (text)")

    style.yesno_button_hbox = Style(style.hbox, help="the box separating yes and no buttons")
    style.yesno_button = Style(style.button, help="a yes or no button")
    style.yesno_button_text = Style(style.button_text, help="a yes or no button (text)")

    style.yesno_frame.xfill = True
    style.yesno_frame.ypadding = .05

    style.yesno_frame_vbox.xalign = 0.5
    style.yesno_frame_vbox.yalign = 0.5
    style.yesno_frame_vbox.box_spacing = hb_rescale_y(30)

    style.yesno_button_hbox.xalign = 0.5
    style.yesno_button_hbox.spacing = hb_rescale_x(100)

    style.yesno_button.size_group = "yesno"

    style.yesno_frame.xalign = 0.5
    style.yesno_frame.yalign = 0.5
    style.yesno_frame.xmaximum = hb_rescale_x(331)

    style.yesno_frame.xmargin = 0
    style.yesno_frame.background = hb_rescale_image("ui/bg-popup.png")
    style.yesno_prompt.yminimum = hb_rescale_y(60)
    style.yesno_prompt.color = "#00000066"
    style.yesno_button_hbox.box_spacing = hb_rescale_x(20)
    style.yesno_button.background = None
    style.yesno_button_text.take(style.mm_button_text)
    style.yesno_button_text.xalign = 0.5


    style.file_picker_entry = Style(style.large_button, help="used to select a file to load or save")
    style.file_picker_entry_box = Style(style.hbox, help="the box inside a file picker entry")

    style.file_picker_text = Style(style.large_button_text, help="text inside a file picker entry")
    style.file_picker_empty_slot = Style(style.file_picker_text, help="text inside an empty file picker entry slot")
    style.file_picker_extra_info = Style(style.file_picker_text)
    style.file_picker_new = Style(style.file_picker_text)
    style.file_picker_old = Style(style.file_picker_text)

    style.file_picker_frame = Style(style.menu_frame, help="the frame enclosing the entire file picker")
    style.file_picker_frame_box = Style(style.vbox, help="the box containing the navbox and the grid of file picker entries")

    style.file_picker_navbox = Style(style.hbox, help="the box containing navigation buttons")

    style.file_picker_nav_button = Style(style.small_button, help="a file picker navigation button")
    style.file_picker_nav_button_text = Style(style.small_button_text, help="file picker navigation button text")

    style.file_picker_grid = Style(style.default, help="a grid containing file picker navigation buttons")

    style.file_picker_entry.xminimum = hb_rescale_x(0.5)

    style.file_picker_frame.xmargin = hb_rescale_x(6)
    style.file_picker_frame.ymargin = hb_rescale_y(6)

    style.file_picker_frame_box.box_spacing = hb_rescale_y(4)
    style.file_picker_entry_box.box_spacing = hb_rescale_y(4)

    style.file_picker_frame.background = None
    style.file_picker_frame.xalign = 0.5
    style.file_picker_frame.yalign = 0.42
    style.file_picker_frame.xmaximum = hb_rescale_x(480)
    style.file_picker_navbox.xalign = 0.5
    style.file_picker_new.bold = True
    style.file_picker_new.color = "#00000066"
    style.file_picker_new.hover_color = "#000000"
    style.file_picker_old.color = "#00000066"
    style.file_picker_old.hover_color = "#000000"
    style.file_picker_extra_info.color = "#00000066"
    style.file_picker_extra_info.line_spacing = hb_rescale_y(5)
    style.file_picker_extra_info.hover_color = "#000000"
    style.file_picker_empty_slot.color = "#00000066"
    style.file_picker_empty_slot.hover_color = "#000000"
    style.file_picker_entry.xfill=False
    style.file_picker_entry.xminimum=hb_rescale_x(380)
    style.file_picker_entry.background = ib_base(hb_rescale_image("ui/bt-scribble.png"))
    style.file_picker_entry.hover_background = hb_rescale_image("ui/bt-scribble.png")
    style.file_picker_entry.top_padding = hb_rescale_y(3)

    style.vscrollbar.thumb_offset = hb_rescale_y(-10)
    style.vscrollbar.ymaximum = hb_rescale_y(250)
    style.vscrollbar.xmaximum = hb_rescale_x(24)
    style.vscrollbar.top_gutter = hb_rescale_y(15)
    style.vscrollbar.bottom_gutter = hb_rescale_y(14)
    style.vscrollbar.top_bar = ib_base(hb_rescale_image("ui/bt-vscrollbar.png"))
    style.vscrollbar.hover_top_bar = hb_rescale_image("ui/bt-vscrollbar.png")
    style.vscrollbar.bottom_bar = ib_base(hb_rescale_image("ui/bt-vscrollbar.png"))
    style.vscrollbar.hover_bottom_bar = hb_rescale_image("ui/bt-vscrollbar.png")
    style.vscrollbar.thumb = ib_base(hb_rescale_image("ui/bt-vscrollthumb.png"))
    style.vscrollbar.hover_thumb = hb_rescale_image("ui/bt-vscrollthumb.png")

    style.create("vscrollbar_disabled", "vscrollbar")
    style.vscrollbar_disabled.top_gutter = 0
    style.vscrollbar_disabled.top_bar = ib_disabled(hb_rescale_image("ui/bt-vscrollbar.png"))
    style.vscrollbar_disabled.hover_top_bar = ib_disabled(hb_rescale_image("ui/bt-vscrollbar.png"))
    style.vscrollbar_disabled.bottom_bar = ib_disabled(hb_rescale_image("ui/bt-vscrollbar.png"))
    style.vscrollbar_disabled.hover_bottom_bar = ib_disabled(hb_rescale_image("ui/bt-vscrollbar.png"))
    style.vscrollbar_disabled.thumb = None
    style.vscrollbar_disabled.hover_thumb = None

    style.create("vscrollbar2", "vscrollbar")
    style.vscrollbar2.ymaximum = hb_rescale_y(224)
    style.vscrollbar2.top_bar = ib_base(hb_rescale_image("ui/bt-vscrollbar2.png"))
    style.vscrollbar2.hover_top_bar = hb_rescale_image("ui/bt-vscrollbar2.png")
    style.vscrollbar2.bottom_bar = ib_base(hb_rescale_image("ui/bt-vscrollbar2.png"))
    style.vscrollbar2.hover_bottom_bar = hb_rescale_image("ui/bt-vscrollbar2.png")

    style.create("vscrollbar2_disabled", "vscrollbar2")
    style.vscrollbar2_disabled.top_gutter = 0
    style.vscrollbar2_disabled.top_bar = ib_disabled(hb_rescale_image("ui/bt-vscrollbar2.png"))
    style.vscrollbar2_disabled.hover_top_bar = ib_disabled(hb_rescale_image("ui/bt-vscrollbar2.png"))
    style.vscrollbar2_disabled.bottom_bar = ib_disabled(hb_rescale_image("ui/bt-vscrollbar2.png"))
    style.vscrollbar2_disabled.hover_bottom_bar = ib_disabled(hb_rescale_image("ui/bt-vscrollbar2.png"))
    style.vscrollbar2_disabled.thumb = None
    style.vscrollbar2_disabled.hover_thumb = None

    style.prefs_frame = Style(style.default, help="the frame containing all of the preferences")
    style.prefs_frame_box = Style(style.default, help="the box inside the frame containing all of the preferences")

    style.prefs_pref_frame = Style(style.menu_frame, help="a frame containing a single preference")
    style.prefs_pref_box = Style(style.vbox, help="the box separating the preference label from the preference choices")
    style.prefs_pref_choicebox = Style(style.vbox, help="the box containing the preference choices")

    style.prefs_label = Style(style.label, help="a preference label (window)")
    style.prefs_label_text = Style(style.label_text, help="a preference label (text)")

    style.prefs_button = Style(style.radio_button, help="preference value button")
    style.prefs_button_text = Style(style.radio_button_text, help="preference value button (text)")

    style.prefs_slider = Style(style.slider, help="preference value slider bar")

    style.prefs_volume_box = Style(style.vbox, help="box containing a volume slider and soundtest button")
    style.prefs_volume_slider = Style(style.prefs_slider, help="volume slider bar")

    style.soundtest_button = Style(style.small_button, help="soundtest button")
    style.soundtest_button_text = Style(style.small_button_text, help="soundtest button (text)")

    style.prefs_jump = Style(style.prefs_pref_frame, help="jump preference frame")
    style.prefs_jump_button = Style(style.button, help="jump preference button")
    style.prefs_jump_button_text = Style(style.button_text, help="jump preference button (text)")

    style.create('gallery_nav_vbox', 'vbox')
    style.create('gallery_nav_button', 'mm_button')
    style.create('gallery_nav_button_text', 'mm_button_text')

    style.gallery_nav_button_text.selected_color = "#000"
    style.gallery_nav_button.xmargin = hb_rescale_x(2)
    style.gallery_nav_button.xpadding = hb_rescale_x(4)

    style.create('gallery_pager_desc', 'gallery_nav_button_text')
    style.gallery_pager_desc.color = "#00000066"

    style.prefs_column = Style(style.vbox, help="preference columns")
    style.prefs_left = Style(style.prefs_column, help="the left preference column")
    style.prefs_center = Style(style.prefs_column, help="the center preference column")
    style.prefs_right = Style(style.prefs_column, help="the right preference column")

    style.prefs_pref_frame.xfill = True
    style.prefs_column.xanchor = 0.5

    style.prefs_pref_box.box_spacing = hb_rescale_y(12)

    style.prefs_column.xmaximum = hb_rescale_x(250)
    style.prefs_column.box_spacing = hb_rescale_y(10)
    style.prefs_frame.top_margin = hb_rescale_y(10)

    style.prefs_left.xpos = hb_rescale_x(137)
    style.prefs_center.xpos = hb_rescale_x(400)
    style.prefs_right.xpos = hb_rescale_x(663)

    style.prefs_slider.xmaximum = hb_rescale_x(200)

    style.prefs_pref_box.xfill = True
    style.prefs_volume_box.xfill = True
    style.prefs_pref_choicebox.xfill = True
    style.prefs_button.xalign = 1.0
    style.prefs_jump_button.xalign = 1.0
    style.prefs_slider.xalign = 1.0
    style.soundtest_button.xalign = 1.0

    style.prefs_button.size_group = "prefs"
    style.prefs_jump_button.size_group = "prefs"

    style.prefs_pref_frame.background = None
    style.prefs_pref_frame.bottom_margin = 0
    style.prefs_button.take(style.mm_button)
    style.prefs_button_text.take(style.mm_button_text)
    style.prefs_label.take(style.yesno_prompt)
    style.prefs_label.yminimum = None
    style.prefs_label.xalign = 0.0
    style.prefs_label_text.take(style.prompt_text)

    style.prefs_slider.clear()
    style.prefs_slider.left_bar = ib_base(hb_rescale_image("ui/bt-cf-bar-left.png"))
    style.prefs_slider.hover_left_bar = hb_rescale_image("ui/bt-cf-bar-left.png")
    style.prefs_slider.right_bar = ib_base(hb_rescale_image("ui/bt-cf-bar-right.png"))
    style.prefs_slider.hover_right_bar = hb_rescale_image("ui/bt-cf-bar-right.png")
    style.prefs_slider.thumb = ib_base(hb_rescale_image("ui/bt-cf-thumb.png"))
    style.prefs_slider.hover_thumb = hb_rescale_image("ui/bt-cf-thumb.png")
    style.prefs_slider.left_gutter = hb_rescale_x(12)
    style.prefs_slider.right_gutter = hb_rescale_x(12)
    style.prefs_slider.xmaximum = hb_rescale_x(200)
    # TODO
    style.prefs_slider.thumb_offset = -3

    style.create("page_caption", "prefs_label")
    style.page_caption.bold = True

#     if not persistent.hdisabled:
#         persistent.hdisabled = False
#
#     if not persistent.emi:
#         persistent.emi = 0
#     if not persistent.emibad:
#         persistent.emibad = 0
#
#     if not persistent.hanako:
#         persistent.hanako = 0
#     if not persistent.hanakosad:
#         persistent.hanakosad = 0
#     if not persistent.hanakorage:
#         persistent.hanakorage = 0
#
#     if not persistent.lilly:
#         persistent.lilly = 0
#     if not persistent.lillybad:
#         persistent.lillybad = 0
#
#     if not persistent.shizune:
#         persistent.shizune = 0
#     if not persistent.shizunebad:
#         persistent.shizunebad = 0
#
#     if not persistent.rin:
#         persistent.rin = 0
#     if not persistent.rinbad:
#         persistent.rinbad = 0
#     if not persistent.rintrue:
#         persistent.rintrue = 0
#
#     if not persistent.bad:
#         persistent.bad = 0
#
#     if not persistent.seen_videos or not 'video/4ls.mkv' in persistent.seen_videos:
#         persistent.seen_videos = ['video/4ls.mkv']
#
#
#
#     def init_vars():
#
#         store.attraction_fail = 0
#         store.attraction_sc = 0
#         store.attraction_lilly = 0
#         store.attraction_hanako = 0
#         store.attraction_rin = 0
#         store.attraction_emi = 0
#         store.attraction_shizune = 0
#         store.attraction_kenji = 0
#         store.seen_labels = []
#         store.current_line = None
#         store.breadcrumbs = []
#         persistent.breadcrumbs = []
#
#     init_vars()
#     NARRATOR_NAME = "{color=#0000}#{/color} "
#     _game_menu_screen = "gm_bare"
#     suppress_window_after_timeskip = False
#     suppress_window_before_timeskip = False
#     prefs_looped = False
#     playthroughflag = True
#     devlvl = 0
#     entered_from_game = False
#     coming_from_prefs_sub = False
#     gm_active = False
#     quit_from_os_flag = False
#     ask_to_quit = False
#     last_visited_label = None
#     last_scene_label = None
#     gm_exit_to = None
#     previous_language = None
#     np_current_language = None
#     may_afm = True
#     animate_mm = True
#     statechangesincesave = True
#     from_splash = False
